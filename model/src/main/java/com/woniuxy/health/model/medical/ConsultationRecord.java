package com.woniuxy.health.model.medical;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.sql.Timestamp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * <p>
 *
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Getter
@Setter
@ToString
@TableName("t_consultation_record")
@ApiModel(value = "ConsultationRecord对象", description = "")
public class ConsultationRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("问诊id(主键)")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("问题描述")
    private String describe;

    @ApiModelProperty("问诊时间")
    private Timestamp consultationTime;

    @ApiModelProperty("解答时间")
    private Timestamp answerTime;

    @ApiModelProperty("问诊方式")
    private Byte consultationMethod;

    @ApiModelProperty("问诊类型")
    private Byte consultationType;

    @ApiModelProperty("问诊状态	图文问诊 待支付 0 未解答(未支付)1 已解答2 已评价3	电话问诊 待支付 0 已支付1 待回电4 已回电5")
    private Byte consultationStatus;

    @ApiModelProperty("医生id(外键)")
    private Long doctorId;

    @ApiModelProperty("用户id(外键)")
    private Long patientId;

    @ApiModelProperty("订单id(外键)")
    private Long orderId;

    @ApiModelProperty("患者满意")
    private Byte patientSatisfaction;

    @ApiModelProperty("患者评价")
    private String patientEvaluation;

    @ApiModelProperty("创建时间")
    private Timestamp createTime;

    @ApiModelProperty("更新时间")
    private Timestamp updateTime;


    @ApiModelProperty("逻辑删除 0 正常 1 删除")
    @TableLogic
    private Byte deleteFlag;


}
