package com.woniuxy.health.model.medical;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.sql.Timestamp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * <p>
 *
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Getter
@Setter
@ToString
@TableName("t_doctor")
@ApiModel(value = "Doctor对象", description = "")
public class Doctor implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键医生id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("医生名称")
    private String name;

    @ApiModelProperty("医生头像")
    private String doctorImgUrl;

    @ApiModelProperty("医生职称")
    private Long titleId;

    @ApiModelProperty("医生电话")
    private String phoneNumber;

    @ApiModelProperty("擅长领域")
    private String field;

    @ApiModelProperty("医生详情")
    private String detail;

    private Integer state;

    @ApiModelProperty("所属科室id")
    private Long departmentId;

    @ApiModelProperty("所属医院id")
    private Long hospitalId;

    @ApiModelProperty("问诊费用")
    private Double receptionFee;

    @ApiModelProperty("创建时间")
    private Timestamp createTime;

    @ApiModelProperty("更新时间")
    private Timestamp updateTime;

    @ApiModelProperty("逻辑删除 0 正常 1 删除")
    @TableLogic
    private Byte deleteFlag;


}
