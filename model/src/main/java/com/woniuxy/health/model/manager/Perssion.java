package com.woniuxy.health.model.manager;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.sql.Timestamp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * <p>
 *
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Getter
@Setter
@ToString
@TableName("sys_perssion")
@ApiModel(value = "Perssion对象", description = "")
public class Perssion implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("自增主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("权限名称")
    private String perssionName;

    @ApiModelProperty("权限对应的url")
    private String url;

    @ApiModelProperty("上级菜单id")
    private Long parentId;

    @ApiModelProperty("菜单权限还是接口权限：1-一级菜单权限，2-二级菜单权限，3-接口权限")
    private Byte urlType;

    @ApiModelProperty("创建时间")
    private Timestamp createTime;

    @ApiModelProperty("更新时间")
    private Timestamp updateTime;

    @ApiModelProperty("逻辑删除，默认0")
    @TableLogic
    private Byte deleteFlag;


}
