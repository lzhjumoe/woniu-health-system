package com.woniuxy.health.model.customer;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * <p>
 * 睡眠表
 * </p>
 *
 * @author godric
 * @since 2023-09-08
 */
@Getter
@Setter
@ToString
  @TableName("t_user_sleep")
@ApiModel(value = "UserSleep对象", description = "睡眠表")
public class UserSleep implements Serializable {

    private static final long serialVersionUID = 1L;

      @ApiModelProperty("睡眠表主键")
        private Integer id;

      @ApiModelProperty("用户id")
      private Integer userId;

      @ApiModelProperty("累计睡眠时长")
      @JsonFormat(pattern = "HH:mm:ss")
      private Time totalSleepTime;

      @ApiModelProperty("累计深睡时长")
      @JsonFormat(pattern = "HH:mm:ss")
      private Time totalDeepSleepTime;

  @ApiModelProperty("累计浅睡时长")
  @JsonFormat(pattern = "HH:mm:ss")
      private Time totalLightSleepTime;

      @ApiModelProperty("累计清醒时长")
      @JsonFormat(pattern = "HH:mm:ss")
      private Time totalAwakeTime;

      @ApiModelProperty("入睡时间")
      @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
      private Time onsetOfSleep;

      @ApiModelProperty("结束睡眠时间")
      @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
      private Time fallAsleepEndTime;

      @ApiModelProperty("创建时间")
      @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
      private Timestamp createTime;

      @ApiModelProperty("更新时间")
      @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
      private Timestamp updateTime;

      @ApiModelProperty("逻辑删除")
      private Integer deleteFlag;


}
