package com.woniuxy.health.model.customer;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Getter
@Setter
@ToString
@TableName("t_user")
@ApiModel(value = "User对象", description = "用户表")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("用户id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("用户名（账号）")
    private String username;

    @ApiModelProperty("登陆密码")
    private String loginPwd;

    @ApiModelProperty("支付密码")
    private String payPwd;

    @ApiModelProperty("昵称")
    private String nickname;

    @ApiModelProperty("真实姓名")
    private String realname;

    @ApiModelProperty("用户头像")
    private String userPhoto;

    @ApiModelProperty("电话号码")
    private String userTel;

    @ApiModelProperty("出生日期")
    private Date birth;
    @ApiModelProperty("身份证号")
    private String idCard;

    @ApiModelProperty("用户性别")
    private Integer userGender;

    @ApiModelProperty("用户年龄")
    private String userAge;

    @ApiModelProperty("用户身高")
    private Double userHeight;

    @ApiModelProperty("用户体重")
    private Double userWeight;

    @ApiModelProperty("创建时间")
    private Timestamp createTime;

    @ApiModelProperty("更新时间")
    private Timestamp updateTime;

    @ApiModelProperty("逻辑删除")
    @TableLogic
    private Byte deleteFlag;

    @ApiModelProperty("用户是否会员,0:不是,1:是")
    private String isMember;

    @ApiModelProperty("用户地区")
    private String userLocation;
}
