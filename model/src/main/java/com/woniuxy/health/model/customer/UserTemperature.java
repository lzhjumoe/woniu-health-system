package com.woniuxy.health.model.customer;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * <p>
 * 体温表
 * </p>
 *
 * @author godric
 * @since 2023-09-08
 */
@Getter
@Setter
@ToString
@TableName("t_user_temperature")
@ApiModel(value = "UserTemperature对象", description = "体温表")
public class UserTemperature implements Serializable {

    private static final long serialVersionUID = 1L;

      @ApiModelProperty("主键")
        private Long id;

      @ApiModelProperty("用户id")
      private Long userId;

      @ApiModelProperty("体温/摄氏度")
      private Double temperature;

      @ApiModelProperty("测量时间")
      @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
      private Date measurementTime;

      @ApiModelProperty("状态 五种状态【正常（36-37），低热（37.2-38），中热（38.1-39），高热（39.1-40），超高热（40以上）】")
      private Integer state;

      @ApiModelProperty("创建时间")
      private Timestamp createTime;

      @ApiModelProperty("更新时间")
      private Timestamp updateTime;

      @ApiModelProperty("逻辑删除")
      private Integer deleteFlag;


}
