package com.woniuxy.health.model.manager;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.sql.Timestamp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Getter
@Setter
@ToString
@TableName("sys_operation_log")
@Accessors(chain=true)
@ApiModel(value = "OperationLog对象", description = "")
public class OperationLog implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("自增主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("操作账号")
    private String operAccount;

    @ApiModelProperty("用户名")
    private String nickName;

    @ApiModelProperty("操作时间")
    private Timestamp operTime;

    @ApiModelProperty("操作人IP地址")
    private String operIp;

    @ApiModelProperty("请求URL")
    private String operUri;

    @ApiModelProperty("操作方法名称")
    private String operMethodName;

    @ApiModelProperty("请求参数")
    private String operRequParam;

    @ApiModelProperty("操作模块")
    private String operModul;

    @ApiModelProperty("操作类型")
    private String operType;

    @ApiModelProperty("操作描述")
    private String operDesc;

    @ApiModelProperty("操作结果")
    private Byte state;

    @ApiModelProperty("创建时间")
    private Timestamp createTime;

    @ApiModelProperty("更新时间")
    private Timestamp updateTime;

    @ApiModelProperty("逻辑删除，默认0")
    @TableLogic
    private Byte deleteFlag;


}
