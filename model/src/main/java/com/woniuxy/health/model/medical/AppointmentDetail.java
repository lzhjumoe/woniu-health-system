package com.woniuxy.health.model.medical;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.sql.Timestamp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * <p>
 *
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Getter
@Setter
@ToString
@TableName("t_appointment_detail")
@ApiModel(value = "AppointmentDetail对象", description = "")
public class AppointmentDetail implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("预约明细表主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("预约、就诊时间")
    private Timestamp appointmentTime;

    @ApiModelProperty("就诊状态，默认未就诊(0)")
    private Byte appointmentState;

    @ApiModelProperty("订单的id")
    private Long orderId;

    private Timestamp createTime;

    private Timestamp updateTime;

    @TableLogic
    private Byte deleteFlag;


}
