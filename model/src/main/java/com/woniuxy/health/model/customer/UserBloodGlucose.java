package com.woniuxy.health.model.customer;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * <p>
 * 血糖表
 * </p>
 *
 * @author godric
 * @since 2023-09-08
 */
@Getter
@Setter
@ToString
  @TableName("t_user_blood_glucose")
@ApiModel(value = "UserBloodGlucose对象", description = "血糖表")
public class UserBloodGlucose implements Serializable {

    private static final long serialVersionUID = 1L;

      @ApiModelProperty("血糖表主键")
        private Long id;

      @ApiModelProperty("用户id")
      private Long userId;

      @ApiModelProperty("血糖 当空腹血糖大于等于6.1mmol/L或餐后血糖大于等于7.8mmol/L时应注意及时去医院就诊 " +
                        "（血糖=(糖化血红蛋白-6)×2+7.5）")
      private Integer bloodGlucose;

      @ApiModelProperty("测量时间")
      @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
      private Date measurementTime;

      @ApiModelProperty("hemoglobin A1C（糖化血红蛋白）正常值为低于5.7%，糖尿病前期介于5.7%-6.4%之间，6.5或之上为糖尿病")
      private Integer hbA1c;

      private Integer state;

      @ApiModelProperty("创建时间")
      private Timestamp createTime;

      @ApiModelProperty("更新时间")
      private Timestamp updateTime;

      @ApiModelProperty("逻辑删除")
      private Integer deleteFlag;


}
