package com.woniuxy.health.model.medical;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * @Date 2023/9/9 12:08
 * @Author LZH
 * Description:
 */
@Getter
@Setter
@ToString
@TableName("t_doctor_title")
@ApiModel(value = "DoctorTitle对象", description = "")
public class DoctorTitle implements Serializable {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty("医生职称id")
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("医生职称名称")
    private String name;
}
