package com.woniuxy.health.model.medical;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author usercheng
 * @since 2023-09-07
 */
@Getter
@Setter
@ToString
  @TableName("t_inquiry_appointment")
@ApiModel(value = "InquiryAppointment对象", description = "")
public class InquiryAppointment implements Serializable {

    private static final long serialVersionUID = 1L;

      @ApiModelProperty("问诊预约表(与订单表同时生成数据)")
        @TableId(value = "id", type = IdType.AUTO)
      private Long id;

      @ApiModelProperty("回答时间(医生回复的时候再赋值)")
      private Date receptionTime;

      @ApiModelProperty("订单id(外键)")
      private Long orderId;

      @ApiModelProperty("患者id(外键)")
      private Long userId;


}
