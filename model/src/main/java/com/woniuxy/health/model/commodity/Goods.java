package com.woniuxy.health.model.commodity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.elasticsearch.client.ml.job.config.DataDescription;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * <p>
 * 商品表
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Getter
@Setter
@ToString
@TableName("t_goods")
@ApiModel(value = "Goods对象", description = "")
public class Goods implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("商品名字")
    private String goodsName;

    @ApiModelProperty("商品类型")
    private Integer goodsTypeId;

    @ApiModelProperty("商品编号")
    private String goodsCode;

    @ApiModelProperty("缩略图")
    private String goodsPhoto;

    @ApiModelProperty("标题")
    private String goodsHead;

    @ApiModelProperty("副标题")
    private String goodsSubhead;

    @ApiModelProperty("备注")
    private String goodsDetail;

    @ApiModelProperty("价格")
    private BigDecimal goodsPrice;

    @ApiModelProperty("库存")
    private Integer goodsStorecount;

    @ApiModelProperty("购买量")
    private Integer goodsBuycount;

    @ApiModelProperty("浏览量")
    private Integer goodsReadcount;

    @ApiModelProperty("商品状态")
    private Byte goodsState;

    @ApiModelProperty("相册")
    private String bigPhoto;

    @ApiModelProperty("排序")
    private Integer goodsOrder;

    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;


    @ApiModelProperty("更新时间")
    private LocalDateTime updateTime;

    @ApiModelProperty("逻辑删除")
    @TableLogic
    private Byte deleteFlag;

}
