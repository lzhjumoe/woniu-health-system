package com.woniuxy.health.model.medical;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.sql.Timestamp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * <p>
 *
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Getter
@Setter
@ToString
@TableName("t_order_detail")
@ApiModel(value = "OrderDetail对象", description = "")
public class OrderDetail implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("订单详情表订单id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("订单类型")
    private Byte orderType;

    @ApiModelProperty("订单金额")
    private Long orderPrice;

    @ApiModelProperty("下单时间")
    private Timestamp orderTime;

    @ApiModelProperty("支付时间")
    private Timestamp payTime;

    @ApiModelProperty("支付方式")
    private Byte paymentType;

    @ApiModelProperty("订单状态")
    private Byte orderState;

    @ApiModelProperty("医生id(外键)")
    private Long doctorId;

    @ApiModelProperty("用户id(外键)")
    private Long userId;

    private Timestamp createTime;

    private Timestamp updateTime;

    @TableLogic
    private Byte deleteFlag;


}
