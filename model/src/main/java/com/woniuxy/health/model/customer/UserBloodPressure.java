package com.woniuxy.health.model.customer;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * <p>
 * 血压表
 * </p>
 *
 * @author godric
 * @since 2023-09-08
 */
@Getter
@Setter
@ToString
  @TableName("t_user_blood_pressure")
@ApiModel(value = "UserBloodPressure对象", description = "血压表")
public class UserBloodPressure implements Serializable {

    private static final long serialVersionUID = 1L;

      @ApiModelProperty("主键")
        private Long id;

      @ApiModelProperty("用户id")
      private Long userId;

      @ApiModelProperty("收缩压")
      private Integer systolicPressure;

      @ApiModelProperty("舒张压")
      private Integer diastolicPressure;

      @ApiModelProperty("平均收缩压")
      private Integer meanSp;

      @ApiModelProperty("平均舒张压")
      private Integer meanDp;

      @ApiModelProperty("状态 六种 【低血压（收缩压小于90或舒张压小于60）正常（收缩压90-119或舒张压69-79）高血压（收缩压120-129或舒张压60-79）高血压I阶段（收缩压130-139或舒张压80-89）高血压II阶段（收缩压140-180或舒张压90-120）高血压危象（收缩压大于180或舒张压大于120）】")
      private Integer state;

      @ApiModelProperty("创建时间")
      @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
      private Timestamp createTime;

      @ApiModelProperty("修改时间")
      @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
      private Timestamp updateTime;

      @ApiModelProperty("逻辑删除")
      private Integer deleteFlag;


}
