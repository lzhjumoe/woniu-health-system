package com.woniuxy.health.model.customer;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * <p>
 * 心率表
 * </p>
 *
 * @author godric
 * @since 2023-09-08
 */
@Getter
@Setter
@ToString
  @TableName("t_user_heart_rate")
@ApiModel(value = "UserHeartRate对象", description = "心率表")
public class UserHeartRate implements Serializable {

    private static final long serialVersionUID = 1L;

      @ApiModelProperty("心率表主键")
        private Long id;

      @ApiModelProperty("用户id")
      private Long userId;

      @ApiModelProperty("心率")
      private Integer heartRate;

      @ApiModelProperty("最高心率")
      private Integer maxHr;

      @ApiModelProperty("最低心率")
      private Integer minHr;

      @ApiModelProperty("平均心率")
      private Integer meanHr;

      @ApiModelProperty("状态（低心率<60，静息心率60-80，常规心率80-100，高心率>100）")
      private Integer state;

      @ApiModelProperty("创建时间")
      private Timestamp createTime;

      @ApiModelProperty("更新时间")
      private Timestamp updateTime;

      @ApiModelProperty("逻辑删除")
      private Integer deleteFlag;


}
