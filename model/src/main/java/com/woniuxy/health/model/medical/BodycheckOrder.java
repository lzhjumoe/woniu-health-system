package com.woniuxy.health.model.medical;

import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * <p>
 *
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-11
 */
@Getter
@Setter
@ToString
@TableName("t_bodycheck_order")
@ApiModel(value = "BodycheckOrder对象", description = "")
public class BodycheckOrder implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键id")
    private Long id;

    @ApiModelProperty("体检订单编码")
    private String orderCode;

    @ApiModelProperty("体检订单套餐id")
    private Long mealId;

    @ApiModelProperty("体检机构id")
    private Long instId;

    @ApiModelProperty("用户id")
    private Long userId;

    @ApiModelProperty("体检日期")
    private Date checkTime;

    @ApiModelProperty("报告文件路径")
    private String reportUrl;

    @ApiModelProperty("是否上传")
    private Integer state;

    @ApiModelProperty("创建日期")
    private Timestamp createTime;

    @ApiModelProperty("修改日期")
    private Timestamp updateTime;

    @ApiModelProperty("逻辑删除")
    @TableLogic
    private Byte deleteFlag;


}
