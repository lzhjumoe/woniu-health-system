package com.woniuxy.health.model.medical;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.sql.Timestamp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * <p>
 *
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-11
 */
@Getter
@Setter
@ToString
@TableName("t_bodycheck_meal")
@ApiModel(value = "BodycheckMeal对象", description = "")
public class BodycheckMeal implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("体检套餐名字")
    private String mealName;

    @ApiModelProperty("体检套餐图片")
    private String mealImgUrl;

    @ApiModelProperty("关联体检机构id")
    private Long checkInstId;

    @ApiModelProperty("套餐价格")
    private Object mealPrice;

    @ApiModelProperty("套餐详情")
    private String mealDetail;

    @ApiModelProperty("套餐项目")
    private String mealItem;

    @ApiModelProperty("套餐上架下架")
    private Integer mealState;

    @ApiModelProperty("创建时间")
    private Timestamp createTime;

    @ApiModelProperty("修改时间")
    private Timestamp updateTime;

    @ApiModelProperty("逻辑删除")
    @TableLogic
    private Byte deleteFlag;


}
