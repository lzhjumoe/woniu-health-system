package com.woniuxy.health.model.commodity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.sql.Timestamp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * <p>
 *  订单记录表
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Getter
@Setter
@ToString
@TableName("t_goods_order")
@ApiModel(value = "GoodsOrder对象", description = "")
public class GoodsOrder implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("订单编号")
    private String orderCode;

    @ApiModelProperty("用户")
    private Long userId;

    @ApiModelProperty("商品编号")
    private Long goodsId;

    @ApiModelProperty("订单地址")
    private Long addrId;

    @ApiModelProperty("购买商品数量")
    private Integer goodsNumber;

    @ApiModelProperty("支付方式")
    private Byte orderPayMethod;

    @ApiModelProperty("订单状态")
    private Byte state;

    @ApiModelProperty("创建时间")
    private Timestamp createTime;

    @ApiModelProperty("更新时间")
    private Timestamp updateTime;

    @ApiModelProperty("逻辑删除")
    @TableLogic
    private Byte deleteFlag;


}
