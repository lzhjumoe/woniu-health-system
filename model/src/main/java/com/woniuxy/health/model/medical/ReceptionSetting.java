package com.woniuxy.health.model.medical;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.sql.Time;
import java.sql.Timestamp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * <p>
 *
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Getter
@Setter
@ToString
@TableName("t_reception_setting")
@ApiModel(value = "ReceptionSetting对象", description = "")
public class ReceptionSetting implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("接诊设置表主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("接诊开始时间")
    private Time receptionBegin;
    @ApiModelProperty("接诊结束时间")
    private Time receptionEnd;
    @ApiModelProperty("接诊数量")
    private Integer receptionCount;

    @ApiModelProperty("接诊费用")
    private Double receptionFee;

    @ApiModelProperty("医生id(外键)")
    private Long doctorId;

    private Timestamp createTime;

    private Timestamp updateTime;

    @TableLogic
    private Byte deleteFlag;


}
