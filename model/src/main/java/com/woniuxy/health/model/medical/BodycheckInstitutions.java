package com.woniuxy.health.model.medical;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author usercheng
 * @since 2023-09-09
 */
@Getter
@Setter
@ToString
  @TableName("t_bodycheck_institutions")
@ApiModel(value = "BodycheckInstitutions对象", description = "")
public class BodycheckInstitutions implements Serializable {

    private static final long serialVersionUID = 1L;

      @ApiModelProperty("体检机构表的id")
        @TableId(value = "id", type = IdType.AUTO)
      private Long id;

      @ApiModelProperty("机构名称")
      private String institutionsName;

      @ApiModelProperty("机构头像")
      private String institutionsImgUrl;

      @ApiModelProperty("机构类型0公立1私立")
      private Integer institutionsType;

      @ApiModelProperty("机构地址")
      private String institutionsAddress;

      @ApiModelProperty("机构状态(上0、1下架)")
      private Integer institutionsState;

      @ApiModelProperty("机构层级关系(总店，分店)")
      private Long pid;

    private Date createTime;

    private Date updateTime;

      @ApiModelProperty("逻辑删除，默认0")
      @TableLogic
      private Integer deleteFlag;


}
