package com.woniuxy.health.model.information;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.sql.Timestamp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * <p>
 *
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Getter
@Setter
@ToString
@TableName("t_info")
@ApiModel(value = "Info对象", description = "")
public class Info implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("资讯id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("资讯名称")
    private String infoName;

    @ApiModelProperty("资讯图片")
    private String infoPhoto;

    @ApiModelProperty("资讯来源（0：自制 1：转载）")
    private Byte infoSource;

    @ApiModelProperty("资讯详情")
    private String infoDetail;

    @ApiModelProperty("作者名称")
    private String authorName;

    @ApiModelProperty("栏目外键")
    private Long infoColumnId;

    @ApiModelProperty("网络地址")
    private String infoIp;

    @ApiModelProperty("是否置顶（0：不置顶 1：置顶）")
    private Byte infoTop;

    @ApiModelProperty("是否上架（0：下架 1：上架）")
    private Byte state;

    @ApiModelProperty("审核状态（0：待审核，1：审核中，2：已审核）")
    private Byte auditType;

    @ApiModelProperty("创建时间")
    private Timestamp createTime;

    @ApiModelProperty("更新时间")
    private Timestamp updateTime;

    @TableLogic
    @ApiModelProperty("逻辑删除")
    private Byte deleteFlag;


}
