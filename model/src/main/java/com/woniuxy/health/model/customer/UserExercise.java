package com.woniuxy.health.model.customer;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * <p>
 * 运动表
 * </p>
 *
 * @author godric
 * @since 2023-09-08
 */
@Getter
@Setter
@ToString
  @TableName("t_user_exercise")
@ApiModel(value = "UserExercise对象", description = "运动表")
public class UserExercise implements Serializable {

    private static final long serialVersionUID = 1L;

      @ApiModelProperty("主键")
        private Long id;

      @ApiModelProperty("用户id")
      private Long userId;

  @ApiModelProperty("跑步距离")
  private Integer runMileage;

  @ApiModelProperty("深蹲数量")
  private Integer fullSquat;

  @ApiModelProperty("仰卧起做数量")
  private Integer sitUp;

  @ApiModelProperty("俯卧撑数量")
  private Integer pressUp;

  @ApiModelProperty("跑步时长")
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private Time runDuration;
  @ApiModelProperty("深蹲时长")
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private Time fullSquatDuration;
  @ApiModelProperty("仰卧起坐时长")
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private Time sitUpDuration;
  @ApiModelProperty("俯卧撑时长")
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private Time pressUpDuration;

      @ApiModelProperty("创建时间")
      @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
      private Timestamp createTime;

      @ApiModelProperty("更新时间")
      @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
      private Timestamp updateTime;

      @ApiModelProperty("逻辑删除")
      private Integer deleteFlag;


}
