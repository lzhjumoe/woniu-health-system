package com.woniuxy.health.model.information;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.sql.Timestamp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * <p>
 *
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Getter
@Setter
@ToString
@TableName("t_info_column")
@ApiModel(value = "InfoColumn对象", description = "")
public class InfoColumn implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("栏目id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("栏目名")
    private String columnName;

    @ApiModelProperty("栏目详情")
    private String columnDetail;

    @ApiModelProperty("栏目状态")
    private Byte state;

    @ApiModelProperty("创建时间")
    private Timestamp createTime;

    @ApiModelProperty("更新时间")
    private Timestamp updateTime;

    @TableLogic
    @ApiModelProperty("逻辑删除")
    private Byte deleteFlag;


}
