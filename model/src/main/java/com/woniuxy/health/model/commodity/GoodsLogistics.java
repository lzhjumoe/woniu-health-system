package com.woniuxy.health.model.commodity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.sql.Timestamp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * <p>
 *  商品订单表
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Getter
@Setter
@ToString
@TableName("t_goods_logistics")
@ApiModel(value = "GoodsLogistics对象", description = "")
public class GoodsLogistics implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("订单id")
    private Long orderId;

    @ApiModelProperty("发货方式")
    private Byte logisType;

    @ApiModelProperty("物流单号")
    private String logisNumber;

    private Timestamp createTime;

    @ApiModelProperty("更新时间")
    private Timestamp updateTime;

    @TableLogic
    private Byte deleteFlag;


}
