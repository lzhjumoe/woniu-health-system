package com.woniuxy.health.model.medical;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.sql.Time;
import java.sql.Timestamp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * <p>
 *
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-11
 */
@Getter
@Setter
@ToString
@TableName("t_bodycheck_time")
@ApiModel(value = "BodycheckTime对象", description = "")
public class BodycheckTime implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("体检套餐id外键")
    private Long mealId;

    @ApiModelProperty("体检套餐标识")
    private Integer state;

    @ApiModelProperty("体检开始时间")
    private Time beginTime;

    @ApiModelProperty("体检结束时间")
    private Time endTime;

    @ApiModelProperty("创建时间")
    private Timestamp createTime;

    @ApiModelProperty("修改时间")
    private Timestamp updateTime;

    @ApiModelProperty("逻辑删除")
    @TableLogic
    private Long deleteFlag;


}
