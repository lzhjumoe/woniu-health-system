package com.woniuxy.health.model.medical;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author usercheng
 * @since 2023-09-09
 */
@Getter
@Setter
@ToString
  @TableName("t_china")
@ApiModel(value = "China对象", description = "")
public class China implements Serializable {

    private static final long serialVersionUID = 1L;

      private Integer id;

    private String name;

    private Integer pid;


}
