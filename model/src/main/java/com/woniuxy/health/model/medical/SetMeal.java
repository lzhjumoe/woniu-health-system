package com.woniuxy.health.model.medical;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.sql.Timestamp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * <p>
 *
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Getter
@Setter
@ToString
@TableName("t_set_meal")
@ApiModel(value = "SetMeal对象", description = "")
public class SetMeal implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("机构套餐表")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("套餐名字")
    private String setMealName;

    @ApiModelProperty("套餐图片")
    private String setMealImgUrl;

    @ApiModelProperty("套餐价格")
    private Long setMealPrice;

    @ApiModelProperty("套餐描述")
    private String setMealDescribe;

    @ApiModelProperty("套餐项目")
    private String setMealItem;

    @ApiModelProperty("套餐上架状态")
    private Byte setMealState;

    @ApiModelProperty("医院id(外键)")
    private Long hospitalId;

    private Timestamp createTime;

    private Timestamp updateTime;

    @TableLogic
    private Byte deleteFlag;


}
