package com.woniuxy.health.model.medical;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.sql.Timestamp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * <p>
 *
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Getter
@Setter
@ToString
@TableName("t_inquiry_setting")
@ApiModel(value = "InquirySetting对象", description = "")
public class InquirySetting implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("问诊设置表主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("问诊类型")
    private Byte inquiryType;

    @ApiModelProperty("问诊价格")
    private Double inquiryPrice;

    @ApiModelProperty("医生id(外键)")
    private Long doctorId;

    private Timestamp createTime;

    private Timestamp updateTime;

    @TableLogic
    private Byte deleteFlag;


}
