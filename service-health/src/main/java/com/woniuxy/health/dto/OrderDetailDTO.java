package com.woniuxy.health.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDateTime;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderDetailDTO {

    private Long id;//订单编号(订单表)
    private Integer orderType;//提问方式(问诊设置表)
    private String userName;//患者名字
    private LocalDateTime orderTime;//问诊时间(与下单时间一样)
    private LocalDateTime receptionTime;//接诊时间(一开始为空，医生回答问题后再修改)
    private Integer orderState;//订单状态(数据库中的状态是数字1,0但是我们返回给前端的是枚举类中的字符串"待支付")

}
