package com.woniuxy.health.dto;

import lombok.Data;

@Data
public class HospitalDto {
    private Long id;
    private String hospitalName;
    private Byte hospitalType;
    private Byte hospitalGrade;

}
