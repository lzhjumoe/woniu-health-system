package com.woniuxy.health.utils;


import com.woniuxy.health.medical.handler.HospitalGradeEnum;
import com.woniuxy.health.vo.HospitalVO;

import java.util.List;
import java.util.ArrayList;


public class HospitalGradeUtil {
    public static String getHospitalGrade(Integer code){
        for(HospitalGradeEnum e : HospitalGradeEnum.values()){
            if(e.getCode() == code){
                return e.getDescribe();
            }
        }
        return null;
    }

    public static List getHospitalGrade(){

        List list = new ArrayList<>();



        for(HospitalGradeEnum e : HospitalGradeEnum.values()){
            HospitalVO hospitalVO = new HospitalVO();
            hospitalVO.setHospitalGradeCode(e.getCode());
            hospitalVO.setHospitalGrade(e.getDescribe());
            list.add(hospitalVO);
        }
        return list;

    }
}
