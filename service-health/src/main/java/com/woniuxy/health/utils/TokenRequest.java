package com.woniuxy.health.utils;


import lombok.Data;

@Data
public class TokenRequest {
    private String refreshToken;//长令牌
}
