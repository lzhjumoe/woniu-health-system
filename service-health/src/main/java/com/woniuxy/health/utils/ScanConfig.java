package com.woniuxy.health.utils;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"com.woniuxy.utils"})
public class ScanConfig {
}
