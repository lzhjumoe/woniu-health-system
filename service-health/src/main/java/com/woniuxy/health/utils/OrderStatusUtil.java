package com.woniuxy.health.utils;

//这个工具类是用于判断订单状态的，因为在数据库中保存的订单状态是数字1、2、3等
//但是返回给前端要返回文字"待付款"等，所以我们就在这里判断数字状态，并返回对应的文字状态
public class OrderStatusUtil {
    public static String getOrderStatus(Integer code){
        //遍历OrderStatusEnum枚举类，并把传过来的code和枚举类中的code做对比
        for(OrderStatusEnum e : OrderStatusEnum.values()){
            if(e.getCode() == code){
                return e.getDescribe();
            }
        }
        return null;
    }


    public static String getOrderType(Integer code){
        //遍历InquiryTypeEnum枚举类，并把传过来的code和枚举类中的code做对比
        for(OrderTypeEnum e : OrderTypeEnum.values()){
            if(e.getCode() == code){
                return e.getDescribe();
            }
        }
        return null;
    }
}
