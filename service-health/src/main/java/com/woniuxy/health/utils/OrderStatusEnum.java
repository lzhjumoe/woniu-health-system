package com.woniuxy.health.utils;

import lombok.Getter;

@Getter
public enum OrderStatusEnum {
    NO_PAY(1,"待付款"),
    NO_SEND(0,"已付款")
    ;

    private Integer code;
    private String describe;
    private OrderStatusEnum(Integer code, String describe) {
        this.code = code;
        this.describe = describe;
    }
}