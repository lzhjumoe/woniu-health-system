package com.woniuxy.health.utils;

import lombok.Getter;

@Getter
public enum OrderTypeEnum {

    INQUIRIES_WITH_IMAGES(0,"图文问诊"),
    INQUIRIES_WITH_TEL(1,"电话问诊"),
    Physical_Examination(2,"体检订单");

    private Integer code;
    private String describe;

    private OrderTypeEnum(Integer code, String describe) {
        this.code = code;
        this.describe = describe;
    }

}