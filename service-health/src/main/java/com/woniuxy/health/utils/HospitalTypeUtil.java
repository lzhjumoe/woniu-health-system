package com.woniuxy.health.utils;



import com.woniuxy.health.medical.handler.HospitalTypeEnum;
import com.woniuxy.health.vo.HospitalVO;

import java.util.ArrayList;
import java.util.List;

public class HospitalTypeUtil {
    public static String getHospitalType(Integer code){
        for(HospitalTypeEnum e : HospitalTypeEnum.values()){
            if(e.getCode() == code){
                return e.getDescribe();
            }
        }
        return null;
    }

    public static List getHospitalType(){

        List list = new ArrayList<>();

        for(HospitalTypeEnum e : HospitalTypeEnum.values()){
            HospitalVO hospitalVO = new HospitalVO();
            hospitalVO.setHospitalTypeCode(e.getCode());
            hospitalVO.setHospitalType(e.getDescribe());
            list.add(hospitalVO);
        }

        return list;

    }
}
