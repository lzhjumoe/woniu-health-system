package com.woniuxy.health.commodity.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.commodity.constant.Constant;
import com.woniuxy.health.commodity.dto.GoodsOrderDTO;
import com.woniuxy.health.commodity.mapper.GoodsOrderMapper;
import com.woniuxy.health.utils.OrderStatusEnum;
import com.woniuxy.health.vo.GoodsOrderVO;
import com.woniuxy.health.model.commodity.GoodsOrder;
import com.woniuxy.health.commodity.service.IGoodsOrderService;
import com.woniuxy.health.vo.PageVO;
import com.woniuxy.utils.result.Asserts;
import com.woniuxy.utils.result.ResultCodeEnum;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Service
public class GoodsOrderServiceImpl extends ServiceImpl<GoodsOrderMapper, GoodsOrder> implements IGoodsOrderService {

    private final GoodsOrderMapper goodsOrderMapper;
    private final RedisTemplate redisTemplate;
    private final RedissonClient redissonClient;

    public GoodsOrderServiceImpl(GoodsOrderMapper goodsOrderMapper, StringRedisTemplate redisTemplate, RedissonClient redissonClient) {
        this.goodsOrderMapper = goodsOrderMapper;
        this.redisTemplate = redisTemplate;
        this.redissonClient = redissonClient;
    }

    @Override
    public Page<GoodsOrderVO> getPageInfo(Integer pageSize, Integer pageNum, Integer state) {
        Page<GoodsOrderVO> page = Page.of(pageNum, pageSize);
        Page<GoodsOrderVO> pageInfo = goodsOrderMapper.getPageInfo(page, state);

        pageInfo.getRecords().stream().forEach(e -> {
            e.setPrice(e.getPrice().multiply(new BigDecimal(e.getGoodsNumber())));
        });
        System.out.println("pageInfo = " + pageInfo);
        return pageInfo;
    }

    @Transactional
    @Override
    public GoodsOrderVO selectById(Integer orderId) {
        GoodsOrderVO goodsOrderVO = goodsOrderMapper.getById(orderId);
        if (goodsOrderVO.getProvince().equals("中国")) {
            goodsOrderVO.setProvince("");
        }
        goodsOrderVO.setPrice(goodsOrderVO.getPrice().multiply(new BigDecimal(goodsOrderVO.getGoodsNumber())));
        return goodsOrderVO;
    }

    @Override
    public List<GoodsOrderDTO> getByIds(List<Integer> ids) {
        Asserts.fail(ids.size() == 0, ResultCodeEnum.NO_DATA);
        List<GoodsOrderDTO> goodsOrderDTOS = goodsOrderMapper.getByIds(ids);
        return goodsOrderDTOS;
    }

}
