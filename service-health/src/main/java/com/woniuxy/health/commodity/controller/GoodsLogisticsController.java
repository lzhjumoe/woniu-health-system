package com.woniuxy.health.commodity.controller;

import com.woniuxy.health.aspect.OperationLogAnnotation;
import com.woniuxy.health.model.commodity.GoodsLogistics;
import com.woniuxy.utils.result.Result;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.woniuxy.health.commodity.service.IGoodsLogisticsService;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@RestController
@RequestMapping("/goodsLogistics")
public class GoodsLogisticsController {

    private final IGoodsLogisticsService goodsLogisticsServiceImpl;

    public GoodsLogisticsController(IGoodsLogisticsService goodsLogisticsServiceImpl){
        this.goodsLogisticsServiceImpl = goodsLogisticsServiceImpl;
    }
    @OperationLogAnnotation(module = "商品物流", type = "新增", description = "新增物流信息")
    @PostMapping("/add")
    public Result add(@RequestBody GoodsLogistics goodsLogistics){
        goodsLogisticsServiceImpl.save(goodsLogistics);
        return Result.ok();
    }


    @GetMapping("/getAll/{id}")
    public Result getAll(@PathVariable("id") Integer id){
        GoodsLogistics byId = goodsLogisticsServiceImpl.getById(id);
        return Result.ok(byId);
    }
}
