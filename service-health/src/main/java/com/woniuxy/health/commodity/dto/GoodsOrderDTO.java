package com.woniuxy.health.commodity.dto;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.woniuxy.health.commodity.converter.TimeConverter;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class GoodsOrderDTO {

    @ExcelProperty("订单编号")
    @ColumnWidth(10)
    private String orderCode;

    @ExcelProperty("用户账号")
    @ColumnWidth(20)
    private String userName;

    @ExcelProperty("订单商品")
    @ColumnWidth(20)
    private String goodsName;

    @ExcelProperty(value = "下单时间", converter = TimeConverter.class)
    @ColumnWidth(20)
    private LocalDateTime createTime;

    @ExcelProperty("订单状态")
    @ColumnWidth(10)
    private String state;

    @ExcelProperty("支付方式")
    @ColumnWidth(10)
    private String payMethod;

    @ExcelProperty("订单金额")
    @ColumnWidth(15)
    private BigDecimal price;
}
