package com.woniuxy.health.commodity.dto;

import lombok.Data;
import org.springframework.lang.Nullable;

import java.util.Date;

/**
 * @author huang
 * @date 2023/9/13 11:15
 */
@Data
public class MessageDTO {

    private String from;
    private String replyTo;
    private String[] to;
    private String[] cc;
    private String[] bcc;
    private Date sentDate;
    private String subject;
    private String text;
}
