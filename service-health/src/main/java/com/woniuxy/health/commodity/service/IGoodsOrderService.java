package com.woniuxy.health.commodity.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.woniuxy.health.commodity.dto.GoodsOrderDTO;
import com.woniuxy.health.vo.GoodsOrderVO;
import com.woniuxy.health.model.commodity.GoodsOrder;
import com.woniuxy.health.vo.PageVO;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
public interface IGoodsOrderService extends IService<GoodsOrder> {

    Page<GoodsOrderVO> getPageInfo(Integer pageSize, Integer pageNum, Integer state);

    GoodsOrderVO selectById(Integer orderId);

    List<GoodsOrderDTO> getByIds(List<Integer> ids);

}
