package com.woniuxy.health.commodity.mapper;
import com.woniuxy.health.commodity.dto.GoodsTypeDTO;
import com.woniuxy.health.vo.GoodsTypeVO;
import org.apache.ibatis.annotations.Mapper;
import com.woniuxy.health.model.commodity.GoodsType;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Mapper
public interface GoodsTypeMapper extends BaseMapper<GoodsType> {

    List<GoodsTypeVO> selectByCondition(@Param("condition") GoodsTypeDTO goodsTypeDTO);

    void deleteSubType(Integer pid);
}
