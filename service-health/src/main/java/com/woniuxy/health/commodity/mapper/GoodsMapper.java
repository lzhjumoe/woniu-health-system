package com.woniuxy.health.commodity.mapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniuxy.health.commodity.dto.GoodsDTO;
import com.woniuxy.health.vo.GoodsVO;
import org.apache.ibatis.annotations.Mapper;
import com.woniuxy.health.model.commodity.Goods;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Mapper
public interface GoodsMapper extends BaseMapper<Goods> {

    Page selectGoods(Page<Object> page,
                     @Param("go") GoodsDTO goodsDTO);
}
