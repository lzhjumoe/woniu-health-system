package com.woniuxy.health.commodity.controller;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.support.ExcelTypeEnum;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniuxy.health.aspect.OperationLogAnnotation;
import com.woniuxy.health.commodity.annotation.Idempotent;
import com.woniuxy.health.commodity.constant.Constant;
import com.woniuxy.health.commodity.dto.GoodsOrderDTO;
import com.woniuxy.health.model.commodity.Goods;
import com.woniuxy.health.model.commodity.GoodsOrder;
import com.woniuxy.health.utils.BatchDownload;
import com.woniuxy.health.vo.GoodsOrderVO;
import com.woniuxy.health.vo.PageVO;
import com.woniuxy.utils.result.Result;
import com.woniuxy.utils.result.ResultCodeEnum;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.*;
import com.woniuxy.health.commodity.service.IGoodsOrderService;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@RestController
@RequestMapping("/goodsOrder")
public class GoodsOrderController {

    private final IGoodsOrderService goodsOrderServiceImpl;
    private final RedisTemplate redisTemplate;

    public GoodsOrderController(IGoodsOrderService goodsOrderServiceImpl, RedisTemplate redisTemplate){
        this.goodsOrderServiceImpl = goodsOrderServiceImpl;
        this.redisTemplate = redisTemplate;
    }

    @OperationLogAnnotation(module = "商品订单", type = "导出excel", description = "导出商品订单")
    @Idempotent(expireTime = 30, timeunit = TimeUnit.SECONDS)
    @PostMapping("/download")
    public Result download(@RequestBody List<Integer> ids,
                         HttpServletResponse resp) throws IOException {

        // rawFileName 设置excel文件名
        BatchDownload.setExcelRespProp(resp, "订单信息");

        List<GoodsOrderDTO> goodsOrderDTO = goodsOrderServiceImpl.getByIds(ids);

        EasyExcel.write(resp.getOutputStream())
                .head(GoodsOrderDTO.class)
                .excelType(ExcelTypeEnum.XLSX) // 设置格式为xlsx
                .sheet("订单信息")  // 设置工作簿名
                .doWrite(goodsOrderDTO);

        return Result.ok();
    }

    // 查询全部订单
    @GetMapping("/getPageInfo")
    public Result getAllOrders(Integer pageSize, Integer pageNum, Integer state){
        Page<GoodsOrderVO> pageInfo = goodsOrderServiceImpl.getPageInfo(pageSize, pageNum, state);
        return Result.ok(pageInfo);
    }

    @OperationLogAnnotation(module = "商品订单", type = "关闭订单", description = "关闭一条订单")
    // 关闭订单
    @Idempotent(expireTime = 30, timeunit = TimeUnit.SECONDS)
    @DeleteMapping("/close/{id}")
    public Result close(@PathVariable("id") Integer id){

        // 接口幂等性
        if (redisTemplate.hasKey(Constant.INTERFACE_IDEA_PREFIX + id)) {
            return Result.fail(ResultCodeEnum.HTTP_ALREADY_HANDLE);
        }
        redisTemplate.opsForValue()
                .setIfAbsent(Constant.INTERFACE_IDEA_PREFIX + id,
                        "", 30, TimeUnit.SECONDS);
        goodsOrderServiceImpl.removeById(id);
        return Result.ok();
    }

    @GetMapping("/getById/{id}")
    public Result getById(@PathVariable("id") Integer orderId){
        GoodsOrderVO goodsOrderVO = goodsOrderServiceImpl.selectById(orderId);
        return Result.ok(goodsOrderVO);
    }
}
