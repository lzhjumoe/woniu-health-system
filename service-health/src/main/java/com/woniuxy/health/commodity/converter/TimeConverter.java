package com.woniuxy.health.commodity.converter;

import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.converters.ReadConverterContext;
import com.alibaba.excel.converters.WriteConverterContext;
import com.alibaba.excel.metadata.data.WriteCellData;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class TimeConverter implements Converter<LocalDateTime> {

    // 转换为java数据格式
    @Override
    public LocalDateTime convertToJavaData(ReadConverterContext<?> context) throws Exception {
        String strDate = context.getReadCellData().getStringValue();

        LocalDateTime date = LocalDateTime.parse(strDate);
        return date;
    }

    // 转换为excel数据格式
    @Override
    public WriteCellData<?> convertToExcelData(WriteConverterContext<LocalDateTime> context) throws Exception {
        LocalDateTime date = context.getValue();

        if (date == null) {
            return new WriteCellData<>("");
        }

        String strDate = date.toString();

        // 使用WriteCellData,把数据写入到excel单元格中
        return new WriteCellData<>(strDate);
    }

}
