package com.woniuxy.health.commodity.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
public class GoodsDTO {
    private Integer pageSize;
    private Integer pageNum;
    private Long id;
    private String goodsName;
    private Integer goodsTypeId;
    private String goodsCode;
    private Byte goodsState;
}
