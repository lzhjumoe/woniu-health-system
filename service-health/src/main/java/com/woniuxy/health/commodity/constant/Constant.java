package com.woniuxy.health.commodity.constant;

public class Constant {

    public static final String INTERFACE_IDEA_PREFIX = "Interface_idea_prefix"; // 幂等性前缀
    public static final String ALL_PTYPE = "ALL_PTYPE";                         // redis中全部一级品类前缀
    public static final Object ORDER_STATUS = "ORDER_STATUS";                   // 订单状态
}
