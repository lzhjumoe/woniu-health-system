package com.woniuxy.health.commodity.dto;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigDecimal;
import java.util.List;
// 添加商品dto
@Data
public class Goods4AddDTO {
    private String goodsName;
    private String goodsHead;
    private String goodsSubhead;
    private Integer goodsTypeId;
    private Integer goodsOrder;
    private String goodsCode;
    private BigDecimal goodsPrice;
    private Integer goodsStorecount;
    private String goodsDetail;
}
