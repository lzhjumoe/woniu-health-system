package com.woniuxy.health.commodity.annotation;

import java.lang.annotation.*;

// 更新缓存注解，标注在方法后
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface UpdateGoodsTypeCacheAnnotation {
}
