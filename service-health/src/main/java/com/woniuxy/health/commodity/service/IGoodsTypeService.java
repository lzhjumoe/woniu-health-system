package com.woniuxy.health.commodity.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.woniuxy.health.commodity.dto.GoodsTypeDTO;
import com.woniuxy.health.model.commodity.GoodsType;
import com.woniuxy.health.vo.PageVO;

import java.util.HashMap;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
public interface IGoodsTypeService extends IService<GoodsType> {

    PageVO getPageInfo(GoodsTypeDTO goodsTypeDTO);

    List<GoodsType> getAllLevelOne();

    void addEntity(GoodsTypeDTO goodsTypeDTO);

    void upgradeById(GoodsTypeDTO goodsTypeDTO);

    List<GoodsType> getAllSubTypes(Integer id);
}
