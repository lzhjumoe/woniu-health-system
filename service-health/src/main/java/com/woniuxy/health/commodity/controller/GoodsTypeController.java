package com.woniuxy.health.commodity.controller;

import com.woniuxy.health.aspect.OperationLogAnnotation;
import com.woniuxy.health.commodity.annotation.Idempotent;
import com.woniuxy.health.commodity.annotation.UpdateGoodsTypeCacheAnnotation;
import com.woniuxy.health.commodity.dto.GoodsTypeDTO;
import com.woniuxy.health.model.commodity.GoodsType;
import com.woniuxy.health.vo.PageVO;
import com.woniuxy.utils.result.Result;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import com.woniuxy.health.commodity.service.IGoodsTypeService;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@RestController
@RequestMapping("/goodsType")
public class GoodsTypeController {

    private final IGoodsTypeService goodsTypeServiceImpl;

    public GoodsTypeController(IGoodsTypeService goodsTypeServiceImpl){
        this.goodsTypeServiceImpl = goodsTypeServiceImpl;
    }

    // 根据父id找子品类
    @GetMapping("/getAllSubTypes/{id}")
    @ApiOperation("根据父品类找子品类")
    @ApiImplicitParam(name = "id", value = "父品类id")
    public Result getAllSubTypes(@PathVariable("id") Integer id){
        List<GoodsType> subTypes
                = goodsTypeServiceImpl.getAllSubTypes(id);
        return Result.ok(subTypes);
    }

    @OperationLogAnnotation(module = "商品类型", type = "批量删除", description = "批量删除商品类型")
    // 批量删除
    @UpdateGoodsTypeCacheAnnotation
    @PostMapping("/batchDelete")
    @ApiOperation("批量删除")
    @ApiImplicitParam(name = "list", value = "批量删除id集合")
    public Result batchDelete(@RequestBody List<Integer> list){
        goodsTypeServiceImpl.removeBatchByIds(list);
        return Result.ok();
    }

    // 查询全部一级品类
    @GetMapping("/getAllLevelOne")
    @ApiOperation("查询所有一级品类")
    public Result getAllLevelOne(){
        List<GoodsType> levelOne = goodsTypeServiceImpl.getAllLevelOne();
        return Result.ok(levelOne.get(0));
    }

    @Idempotent(expireTime = 30, timeunit = TimeUnit.SECONDS)
    @UpdateGoodsTypeCacheAnnotation
    @PostMapping("/add")
    @ApiOperation("添加品类")
    @ApiImplicitParam(name = "goodsTypeDTO", value = "添加品类对象")
    public Result add(@RequestBody GoodsTypeDTO goodsTypeDTO){
        goodsTypeServiceImpl.addEntity(goodsTypeDTO);
        return Result.ok();
    }

    @PostMapping("/getPageInfo")
    @ApiOperation("查询分页")
    @ApiImplicitParam(name = "goodsTypeDTO", value = "查询品类封装对象")
    public Result getPageInfo(@RequestBody GoodsTypeDTO goodsTypeDTO) throws Exception{
        PageVO page = goodsTypeServiceImpl.getPageInfo(goodsTypeDTO);
        return Result.ok(page);
    }

    @OperationLogAnnotation(module = "商品类型", type = "删除", description = "删除一条记录")
    @UpdateGoodsTypeCacheAnnotation
    @DeleteMapping("/delete/{id}")
    @ApiOperation("根据id删除")
    @ApiImplicitParam(name = "id", value = "目标品类id")
    public Result delete(@PathVariable("id") Integer id){
        goodsTypeServiceImpl.removeById(id);
        return Result.ok();
    }

    @OperationLogAnnotation(module = "商品类型", type = "更新", description = "更新品类")
    @UpdateGoodsTypeCacheAnnotation
    @PutMapping("/update")
    @ApiOperation("更新品类")
    @ApiImplicitParam(name = "goodsTypeDTO", value = "更新品类对象")
    public Result update(@RequestBody GoodsTypeDTO goodsTypeDTO){
        goodsTypeServiceImpl.upgradeById(goodsTypeDTO);
        return Result.ok();
    }
}
