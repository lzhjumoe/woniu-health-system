package com.woniuxy.health.commodity.interceptor;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniuxy.health.commodity.annotation.UpdateBookESAnnotation;
import com.woniuxy.health.commodity.annotation.UpdateGoodsTypeCacheAnnotation;
import com.woniuxy.health.commodity.constant.Constant;
import com.woniuxy.health.commodity.dto.GoodsDTO;
import com.woniuxy.health.commodity.mapper.GoodsTypeMapper;
import com.woniuxy.health.commodity.service.impl.GoodsServiceImpl;
import com.woniuxy.health.model.commodity.GoodsType;
import com.woniuxy.health.vo.GoodsVO;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;



// 更新缓存拦截器
public class UpdateCacheInterceptor implements HandlerInterceptor {

    @Resource
    private RedisTemplate redisTemplate;

    @Resource
    private GoodsTypeMapper goodsTypeMapper;

    @Autowired
    private GoodsServiceImpl goodsService;

    @Autowired
    private ElasticsearchRestTemplate esTemplate;



    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        HandlerMethod hmd
                = (HandlerMethod) handler;

        // 检查方法上是否有自定义的更新缓存注解，有则更新redis缓存
        if (hmd.getMethod().isAnnotationPresent(UpdateGoodsTypeCacheAnnotation.class)){
            redisTemplate.delete(Constant.ALL_PTYPE);
            List<GoodsType> list = goodsTypeMapper.selectList(Wrappers.lambdaQuery(GoodsType.class)
                    .eq(GoodsType::getParentId, 0));
            // 存入redis
            redisTemplate.opsForList().leftPush(Constant.ALL_PTYPE, list);
        }

        // 检查方法上是否有自定义的更新缓存注解, 有则更新es缓存
        if (hmd.getMethod().isAnnotationPresent(UpdateBookESAnnotation.class)){

            // 根据删除指定类型索引
            esTemplate.indexOps(GoodsVO.class).delete();

//            Runnable runnable = new Runnable() {
//                @Override
//                public void run() {
//                    try {
//                        Thread.sleep(3000);
//                    } catch (InterruptedException e) {
//                        throw new RuntimeException(e);
//                    }
//                    GoodsDTO goodsDTO = new GoodsDTO();
//                    goodsDTO.setPageNum(1);
//                    goodsDTO.setPageSize(Integer.MAX_VALUE);
//                    Page pageInfo = goodsService.getPageInfo(goodsDTO);
//                    List<GoodsVO> records = pageInfo.getRecords();
//
//                    esTemplate.save(records);
//                }
//            };
//            Thread thread = new Thread(runnable);
//            thread.start();

            GoodsDTO goodsDTO = new GoodsDTO();
            goodsDTO.setPageNum(1);
            goodsDTO.setPageSize(Integer.MAX_VALUE);
            Page pageInfo = goodsService.getPageInfo(goodsDTO);
            List<GoodsVO> records = pageInfo.getRecords();

            esTemplate.save(records);

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }


    }
}
