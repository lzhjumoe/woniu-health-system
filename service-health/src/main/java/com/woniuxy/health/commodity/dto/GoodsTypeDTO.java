package com.woniuxy.health.commodity.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.woniuxy.health.model.commodity.GoodsType;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.List;

@Data
public class GoodsTypeDTO {

    private Integer id;
    private Integer pageNum;
    private Integer pageSize;
    private Integer typeId;
    private String subTypesName;
    private List<String> subTypes;  // 子品类集合
    private String typeName;        // 品类名称
    private String typeDetail;      // 品类描述

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate[] dateRange;

}
