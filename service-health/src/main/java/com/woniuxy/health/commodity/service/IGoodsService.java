package com.woniuxy.health.commodity.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.woniuxy.health.commodity.dto.GoodsDTO;
import com.woniuxy.health.model.commodity.Goods;
import com.woniuxy.health.vo.PageVO;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
public interface IGoodsService extends IService<Goods> {

    Page getPageInfo(GoodsDTO goodsDTO);

    void batchOperate(List<Integer> ids, Byte b);

    public PageVO listByElasticSearch(Pageable page, GoodsDTO goodsDTO);
}
