package com.woniuxy.health.commodity.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.commodity.mapper.GoodsLogisticsMapper;

import com.woniuxy.health.commodity.service.IGoodsLogisticsService;
import com.woniuxy.health.model.commodity.GoodsLogistics;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Service
public class GoodsLogisticsServiceImpl extends ServiceImpl<GoodsLogisticsMapper, GoodsLogistics> implements IGoodsLogisticsService {

    private final GoodsLogisticsMapper goodsLogisticsMapper;

    @Autowired
    public GoodsLogisticsServiceImpl(GoodsLogisticsMapper goodsLogisticsMapper){
        this.goodsLogisticsMapper = goodsLogisticsMapper;
    }

}
