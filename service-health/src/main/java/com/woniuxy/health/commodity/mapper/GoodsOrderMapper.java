package com.woniuxy.health.commodity.mapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniuxy.health.commodity.dto.GoodsOrderDTO;
import com.woniuxy.health.vo.GoodsOrderVO;
import org.apache.ibatis.annotations.Mapper;
import com.woniuxy.health.model.commodity.GoodsOrder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Mapper
public interface GoodsOrderMapper extends BaseMapper<GoodsOrder> {

    Page<GoodsOrderVO> getPageInfo(@Param("pagInfo") Page pagInfo, @Param("state") Integer state);

    GoodsOrderVO getById(Integer orderId);

    List<GoodsOrderDTO> getByIds(List<Integer> ids);

}
