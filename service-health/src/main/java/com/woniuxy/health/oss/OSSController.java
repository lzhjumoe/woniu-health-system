package com.woniuxy.health.oss;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

/**
 * @Date 2023/9/11 1:15
 * @Author LZH
 * Description:
 */

@RestController
@CrossOrigin
public class OSSController {


    @Resource
    private OSSUtil ossUtil;

    /**
     * 文件上传
     *
     * @param file
     * @return
     */
    @PostMapping("/oss/upload")
    public String uploadFile(@RequestParam("file") MultipartFile file) {
        try {
            String fileName = file.getOriginalFilename();
            String uuidFileName = ossUtil.generateUUIDFileName(fileName);
            String key = "folder/" + uuidFileName; // 设置文件在OSS中的存储路径
            String fileUrl = ossUtil.getFileUrl(key);
            System.out.println("fileUrl = " + fileUrl);
            ossUtil.uploadFile(key, file.getInputStream());
            return fileUrl; // 返回文件在OSS中的存储路径
        } catch (Exception e) {
            e.printStackTrace();
            return "上传失败";
        }
    }



}
