package com.woniuxy.health.manager.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.woniuxy.health.aspect.OperationLogAnnotation;
import com.woniuxy.health.manager.dto.AuthorizationDto;
import com.woniuxy.health.manager.dto.RolePermissionDto;
import com.woniuxy.health.manager.service.IPerssionService;
import com.woniuxy.health.model.manager.Perssion;
import com.woniuxy.health.model.manager.RolePermission;
import com.woniuxy.utils.result.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.woniuxy.health.manager.service.IRolePermissionService;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@RestController
@RequestMapping("/rolePermission")
public class RolePermissionController {

    private final IRolePermissionService rolePermissionServiceImpl;
    private final IPerssionService perssionService;

    @Autowired
    public RolePermissionController(IRolePermissionService rolePermissionServiceImpl, IPerssionService perssionService){
        this.rolePermissionServiceImpl = rolePermissionServiceImpl;
        this.perssionService = perssionService;
    }

    @GetMapping("/default/{id}")
    public Result searchDefaultPermissionCheck(@PathVariable("id") Long id){
        List<RolePermission> list = rolePermissionServiceImpl.
                list(Wrappers.lambdaQuery(RolePermission.class)
                        .eq(RolePermission::getRoleId, id));
        //使用Stream的map方法，完成数据映射
        List<Long> ids = list.stream().map(e -> e.getPerssionId()).collect(Collectors.toList());
        return Result.ok(ids);
    }

    @OperationLogAnnotation(module = "角色管理-角色权限更新", type = "更新", description = "更新角色权限")
    @PostMapping("/roleAuthorization")
    public Result roleAuthorization(@RequestBody RolePermissionDto rolePermissionDto){
        rolePermissionServiceImpl.roleAuthorization2(rolePermissionDto);
        return Result.ok();
    }

    @OperationLogAnnotation(module = "权限管理-超级管理员权限更新", type = "更新", description = "更新管理员权限")
    @PostMapping("/adminInitializePermissions")
    public Result adminInitializePermissions(){
        List<Perssion> list = perssionService.list();
        List<Long> collect = list.stream().map(e -> e.getId()).collect(Collectors.toList());
        RolePermissionDto rolePermission = new RolePermissionDto();
        rolePermission.setPermissionData(collect);
        rolePermission.setId(1l);
        rolePermission.setNickName("admin");
        rolePermissionServiceImpl.roleAuthorization2(rolePermission);
        return Result.ok();
    }
}
