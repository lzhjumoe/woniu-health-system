package com.woniuxy.health.manager.controller;

import com.woniuxy.health.aspect.OperationLogAnnotation;
import com.woniuxy.health.model.manager.Department;
import com.woniuxy.utils.result.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.woniuxy.health.medical.service.IDepartmentService;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@RestController
@RequestMapping("/department")
public class DepartmentController {

    private final IDepartmentService departmentServiceImpl;

    @Autowired
    public DepartmentController(IDepartmentService departmentServiceImpl){
        this.departmentServiceImpl = departmentServiceImpl;
    }

    @GetMapping("/departmentList")
    public Result findByDepartmentList(){
        List<Department> list = departmentServiceImpl.list();
        return Result.ok(list);
    }

}
