package com.woniuxy.health.manager.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.woniuxy.health.manager.dto.AuthorizationDto;
import com.woniuxy.health.manager.dto.RolePermissionDto;
import com.woniuxy.health.model.manager.RolePermission;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
public interface IRolePermissionService extends IService<RolePermission> {

    void roleAuthorization(RolePermissionDto rolePermissionDto);
    void roleAuthorization2(RolePermissionDto rolePermissionDto);
}
