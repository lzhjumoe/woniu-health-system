package com.woniuxy.health.manager.dto;

import lombok.Data;

@Data
public class ManagerDto {
    private Long id;
    private String account;
    private String nickName;

    private String password;
    private Long roleId;
}
