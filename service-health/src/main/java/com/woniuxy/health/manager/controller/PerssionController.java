package com.woniuxy.health.manager.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniuxy.health.aspect.OperationLogAnnotation;
import com.woniuxy.health.manager.dto.PermissionDto;
import com.woniuxy.health.model.manager.Perssion;
import com.woniuxy.health.manager.vo.PageVo;
import com.woniuxy.health.manager.vo.PermissionMenuVo;
import com.woniuxy.utils.result.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.woniuxy.health.manager.service.IPerssionService;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Timestamp;
import java.util.List;

import static com.baomidou.mybatisplus.core.toolkit.SystemClock.now;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@RestController
@CrossOrigin
@RequestMapping("/permission")
public class PerssionController {

    private final IPerssionService perssionServiceImpl;

    @Autowired
    public PerssionController(IPerssionService perssionServiceImpl) {
        this.perssionServiceImpl = perssionServiceImpl;
    }


    @GetMapping("/findPermission")
    public Result findPermission() {
        List<PermissionMenuVo> list = perssionServiceImpl.findPermission();
        return Result.ok(list);
    }

    @GetMapping("/findPermissionName")
    public Result findPermissionName(String permissionName) {
        Perssion perssion = perssionServiceImpl.findPermissionByName(permissionName);
        return Result.ok(perssion);
    }

    @GetMapping("/findRolePermission/{id}")
    public Result findRolePermission(@PathVariable("id") Long managedId) {
        List<PermissionMenuVo> list = perssionServiceImpl.findRolePermission(managedId);
        return Result.ok(list);
    }


    @PostMapping("/listAll/{pageSize}/{pageNum}")
    public Result listAll(@PathVariable("pageSize") Integer pageSize, @PathVariable("pageNum") Integer pageNum, @RequestBody PermissionDto permissionDto) {
        Page page = Page.of(pageNum, pageSize);
        PageVo pv = perssionServiceImpl.listAll(page, permissionDto);
        return Result.ok(pv);
    }

    @GetMapping("/findParentPermission")
    public Result findParentPermission(Byte urlType) {
        List<Perssion> list = perssionServiceImpl.list(Wrappers.lambdaQuery(Perssion.class).eq(Perssion::getUrlType, urlType));
        return Result.ok(list);
    }

    @GetMapping("/findAllParentPermission")
    public Result findAllParentPermission() {
        List<Perssion> list = perssionServiceImpl.list(Wrappers.lambdaQuery(Perssion.class)
                .eq(Perssion::getUrlType, 0)
                .or()
                .eq(Perssion::getUrlType, 1)
                .or()
                .eq(Perssion::getUrlType, 2));
        return Result.ok(list);
    }

    @OperationLogAnnotation(module = "权限管理-权限新增", type = "新增", description = "新增权限")
    @PostMapping("/add")
    public Result addUrlPermission(@RequestBody Perssion permission) {
        perssionServiceImpl.save(permission);
        return Result.ok();
    }

    @OperationLogAnnotation(module = "权限管理-权限更新", type = "更新", description = "更新权限")
    @PostMapping("/update")
    public Result updateUrlPermission(@RequestBody Perssion permission) {
        permission.setUpdateTime(new Timestamp(now()));
        perssionServiceImpl.updateById(permission);
        return Result.ok();
    }

    @OperationLogAnnotation(module = "权限管理-权限删除", type = "删除", description = "删除权限")
    @DeleteMapping("/delete/{id}")
    public Result deleteUrlPermission(@PathVariable("id") Long id) {
        perssionServiceImpl.deletePermission(id);
        return Result.ok();
    }

    @GetMapping("/all")
    public Result findAll() {
        List<PermissionMenuVo> list = perssionServiceImpl.findAll();
        return Result.ok(list);
    }
}
