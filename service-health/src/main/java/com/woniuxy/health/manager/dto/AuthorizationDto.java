package com.woniuxy.health.manager.dto;

import lombok.Data;

import java.util.List;

@Data
public class AuthorizationDto {

    private Long id;
    private String roleName;
}
