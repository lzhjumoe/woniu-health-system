package com.woniuxy.health.manager.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.woniuxy.health.manager.dto.RoleDto;
import com.woniuxy.health.model.manager.Role;
import com.woniuxy.health.manager.vo.PageVo;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
public interface IRoleService extends IService<Role> {

    PageVo findAllPage(Page<Role> page, RoleDto roleDto);

    void deleteRole(Long id);

    Role findByRoleName(String roleInfo);

    Role findByRoleCode(String roleInfo);

    void updateRoleState(Role role);
}
