package com.woniuxy.health.manager.vo;

import lombok.Data;

import java.util.List;

@Data
public class PermissionMenuVo {
    private Long id;
    private String perssionName;
    private String url;
    private List<PermissionMenuVo> children;
}