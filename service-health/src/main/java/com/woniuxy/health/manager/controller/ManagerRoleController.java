package com.woniuxy.health.manager.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniuxy.health.aspect.OperationLogAnnotation;
import com.woniuxy.health.manager.vo.PageVo;
import com.woniuxy.health.model.manager.ManagerRole;
import com.woniuxy.utils.result.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.woniuxy.health.manager.service.IManagerRoleService;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@RestController
@RequestMapping("/managerRole")
public class ManagerRoleController {

    private final IManagerRoleService managerRoleServiceImpl;

    @Autowired
    public ManagerRoleController(IManagerRoleService managerRoleServiceImpl){
        this.managerRoleServiceImpl = managerRoleServiceImpl;
    }

    @GetMapping("/findManagerByRole/{pageSize}/{pageNum}")
    public Result findManagerByRole(@PathVariable("pageSize") Integer pageSize,@PathVariable("pageNum") Integer pageNum,Long id){
        Page page = Page.of(pageNum,pageSize);
        PageVo vo =  managerRoleServiceImpl.findAllPage(page,id);
        return Result.ok(vo);
    }

    @OperationLogAnnotation(module = "角色管理-角色信息更新", type = "更新", description = "更新角色信息")
    @PutMapping("/updateRolesByUid")
    public Result updateRolesByUid(@RequestBody Map<String,Object> uidAndRids){
        managerRoleServiceImpl.updateRolesByUid(uidAndRids);
        return Result.ok();
    }

}
