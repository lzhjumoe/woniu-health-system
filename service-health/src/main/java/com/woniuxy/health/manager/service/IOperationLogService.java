package com.woniuxy.health.manager.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.woniuxy.health.manager.dto.OperationLogDto;
import com.woniuxy.health.manager.vo.PageVo;
import com.woniuxy.health.model.manager.OperationLog;
import com.woniuxy.health.model.manager.OperationLog;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
public interface IOperationLogService extends IService<OperationLog> {

    PageVo<OperationLog> findPage(Page<OperationLog> page, OperationLogDto operationLogDto);
}
