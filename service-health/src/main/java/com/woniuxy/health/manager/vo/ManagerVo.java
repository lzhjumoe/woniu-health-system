package com.woniuxy.health.manager.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.sql.Timestamp;

@Data
public class ManagerVo {
    private Long id;

    private String account;

    private String nickName;

    private String realName;

    private String roleName;

    private Long departmentId;

    private String deptName;

    private Byte state;

    private Timestamp createTime;

    private Timestamp updateTime;

}
