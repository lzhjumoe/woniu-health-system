package com.woniuxy.health.manager.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.woniuxy.health.manager.dto.PermissionDto;
import com.woniuxy.health.model.manager.Perssion;
import com.woniuxy.health.manager.vo.PageVo;
import com.woniuxy.health.manager.vo.PermissionMenuVo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
public interface IPerssionService extends IService<Perssion> {

    List<PermissionMenuVo> findPermission();

    List<PermissionMenuVo> findRolePermission(Long managedId);

    PageVo listAll(Page<Perssion> page, PermissionDto permissionDto);

    void deletePermission(Long id);

    List<PermissionMenuVo> findAll();

    Perssion findPermissionByName(String permissionName);
}
