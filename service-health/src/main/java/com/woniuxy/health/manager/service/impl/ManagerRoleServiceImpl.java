package com.woniuxy.health.manager.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.manager.mapper.ManagerMapper;
import com.woniuxy.health.manager.mapper.ManagerRoleMapper;
import com.woniuxy.health.manager.vo.ManagerVo;
import com.woniuxy.health.manager.vo.PageVo;
import com.woniuxy.health.medical.mapper.DepartmentMapper;
import com.woniuxy.health.model.manager.Department;
import com.woniuxy.health.model.manager.Manager;
import com.woniuxy.health.model.manager.ManagerRole;
import com.woniuxy.health.manager.service.IManagerRoleService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Service
public class ManagerRoleServiceImpl extends ServiceImpl<ManagerRoleMapper, ManagerRole> implements IManagerRoleService {

    private final ManagerRoleMapper managerRoleMapper;
    private final ManagerMapper managerMapper;
    private final DepartmentMapper departmentMapper;

    @Autowired
    public ManagerRoleServiceImpl(ManagerRoleMapper managerRoleMapper, ManagerMapper managerMapper, DepartmentMapper departmentMapper) {
        this.managerRoleMapper = managerRoleMapper;
        this.managerMapper = managerMapper;
        this.departmentMapper = departmentMapper;
    }

    @Override
    public PageVo findAllPage(Page page, Long id) {
        List<ManagerRole> managerRoles = managerRoleMapper.selectList(Wrappers.lambdaQuery(ManagerRole.class)
                .eq(ManagerRole::getRoleId, id));
        List<Long> managerIds = managerRoles.stream().map(ManagerRole::getManagerId).collect(Collectors.toList());
        if (managerIds.isEmpty()) {
            // managerIds为空，直接返回一个空的PageVo对象
            PageVo pageVo = new PageVo<>();
            pageVo.setTotal(0L);
            pageVo.setRecords(Collections.emptyList());
            return pageVo;
        }
        Page managerPage = managerMapper.selectPage(page, Wrappers.lambdaQuery(Manager.class)
                .in(Manager::getId, managerIds));
        List<Manager> managers = managerPage.getRecords();
        List<ManagerVo> managerVos = new ArrayList<>();
        for (Manager manager : managers) {
            String deptName ="";
            if (Objects.nonNull(manager.getDepartmentId())) {
                deptName = departmentMapper.selectOne(Wrappers.lambdaQuery(Department.class)
                        .eq(Department::getId, manager.getDepartmentId())).getDeptName();
            }
            ManagerVo managerVo = new ManagerVo();
            BeanUtils.copyProperties(manager, managerVo);
            managerVo.setDeptName(deptName);
            managerVos.add(managerVo);
        }
        PageVo<ManagerVo> pageVo = new PageVo<>(managerPage.getTotal(), managerVos);
        return pageVo;
    }


    @Override
    @Transactional
    public void updateRolesByUid(Map<String, Object> uidAndRids) {
        Integer uid = (Integer) uidAndRids.get("uid");
        Long longUid = Long.valueOf(uid);
        //删除原有的记录
        int delete = managerRoleMapper.delete(Wrappers.lambdaQuery(ManagerRole.class)
                .eq(ManagerRole::getManagerId, uid));
        //插入新记录
        List<Integer> rids = (List<Integer>) uidAndRids.get("rids");
        rids.stream().forEach(e ->{
            Long aLong = Long.valueOf(e);
            ManagerRole managerRole = new ManagerRole();
            managerRole.setRoleId(aLong);
            managerRole.setManagerId(longUid);
            int insert = managerRoleMapper.insert(managerRole);
        });
    }
}
