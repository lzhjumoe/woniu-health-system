package com.woniuxy.health.manager.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.woniuxy.health.manager.vo.PageVo;
import com.woniuxy.health.model.manager.ManagerRole;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
public interface IManagerRoleService extends IService<ManagerRole> {

    PageVo findAllPage(Page page, Long id);

    void updateRolesByUid(Map<String, Object> uidAndRids);
}
