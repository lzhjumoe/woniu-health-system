package com.woniuxy.health.manager.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniuxy.health.aspect.OperationLogAnnotation;
import com.woniuxy.health.manager.dto.RoleDto;
import com.woniuxy.health.manager.service.IManagerRoleService;
import com.woniuxy.health.model.manager.ManagerRole;
import com.woniuxy.health.model.manager.Role;
import com.woniuxy.health.manager.vo.PageVo;
import com.woniuxy.utils.result.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.woniuxy.health.manager.service.IRoleService;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import static com.baomidou.mybatisplus.core.toolkit.SystemClock.now;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@RestController
@RequestMapping("/role")
public class RoleController {

    private final IRoleService roleServiceImpl;
    private final IManagerRoleService managerRoleServiceImpl;

    @Autowired
    public RoleController(IRoleService roleServiceImpl, IManagerRoleService managerRoleServiceImpl){
        this.roleServiceImpl = roleServiceImpl;
        this.managerRoleServiceImpl = managerRoleServiceImpl;
    }

    @PostMapping("/findAllPage/{pageSize}/{pageNum}")
    public Result findAllPage(@PathVariable("pageSize") Integer pageSize,@PathVariable("pageNum") Integer pageNum,@RequestBody RoleDto roleDto){
        Page page = Page.of(pageNum,pageSize);
        PageVo vo =  roleServiceImpl.findAllPage(page,roleDto);
        return Result.ok(vo);
    }

    @GetMapping("/findByRoleName")
    public Result findByRoleName(String roleInfo) {
        Role role=roleServiceImpl.findByRoleName(roleInfo);
        if (role != null) {
            return Result.ok(role);
        }
        return Result.fail();
    }

    @GetMapping("/findByRoleCode")
    public Result findByRoleCode(String roleInfo) {
        Role role=roleServiceImpl.findByRoleCode(roleInfo);
        if (role != null) {
            return Result.ok(role);
        }
        return Result.fail();
    }

    @GetMapping("/findAllRole")
    public Result findAllRole() {
        List<Role> roles=roleServiceImpl.list();
        return Result.ok(roles);
    }

    @GetMapping("/findRoleIdsByUserId")
    public Result findRoleIdsByUserId(Long uid){
        QueryWrapper<ManagerRole> wrapper = new QueryWrapper<>();
        wrapper.eq("manager_id", uid)
                .select("role_id");
        List<Long> roleIds = managerRoleServiceImpl.listObjs(wrapper, obj -> (Long) obj);
        System.out.println("roleIds = " + roleIds);
        return Result.ok(roleIds);
    }

    @OperationLogAnnotation(module = "角色管理-角色新增", type = "新增", description = "新增角色")
    @PostMapping("/add")
    public Result addRole(@RequestBody Role role){
        roleServiceImpl.save(role);
        return Result.ok();
    }

    @OperationLogAnnotation(module = "角色管理-角色信息更新", type = "更新", description = "更新角色信息")
    @PostMapping("/update")
    public Result updateRole(@RequestBody Role role){
        role.setUpdateTime(new Timestamp(now()));
        roleServiceImpl.updateById(role);
        return Result.ok();
    }

    @OperationLogAnnotation(module = "角色管理-角色状态更新", type = "更新", description = "更新角色状态")
    @PostMapping("/updateRoleState")
    public Result updateRoleState(@RequestBody Role role){
        roleServiceImpl.updateRoleState(role);
        return Result.ok();
    }

    @OperationLogAnnotation(module = "角色管理-角色删除", type = "删除", description = "删除角色")
    @DeleteMapping("/delete/{id}")
    public Result deleteRole(@PathVariable("id") Long id){
        roleServiceImpl.deleteRole(id);
        return Result.ok();
    }
}
