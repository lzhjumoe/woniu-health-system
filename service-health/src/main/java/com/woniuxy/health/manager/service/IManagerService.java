package com.woniuxy.health.manager.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.woniuxy.health.manager.dto.AccountDto;
import com.woniuxy.health.manager.dto.ManagerDto;
import com.woniuxy.health.manager.vo.ManagerVo;
import com.woniuxy.health.manager.vo.PageVo;
import com.woniuxy.health.model.manager.Manager;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
public interface IManagerService extends IService<Manager> {

    Manager login(ManagerDto managerDto);

    PageVo<ManagerVo> findAllPage(Page<Manager> page, AccountDto accountDto);


    Manager findByAccount(String managerInfo);

    Manager findByNickName(String managerInfo);

    void deleteManager(Long id);
}
