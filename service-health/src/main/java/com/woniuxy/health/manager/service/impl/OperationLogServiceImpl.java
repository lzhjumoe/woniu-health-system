package com.woniuxy.health.manager.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.manager.dto.OperationLogDto;
import com.woniuxy.health.manager.mapper.OperationLogMapper;
import com.woniuxy.health.manager.vo.PageVo;
import com.woniuxy.health.model.manager.OperationLog;
import com.woniuxy.health.model.manager.OperationLog;
import com.woniuxy.health.model.manager.OperationLog;
import com.woniuxy.health.manager.service.IOperationLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Service
public class OperationLogServiceImpl extends ServiceImpl<OperationLogMapper, OperationLog> implements IOperationLogService {

    private final OperationLogMapper operationLogMapper;

    @Autowired
    public OperationLogServiceImpl(OperationLogMapper operationLogMapper){
        this.operationLogMapper = operationLogMapper;
    }

    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    @Override
    public PageVo<OperationLog> findPage(Page<OperationLog> page, OperationLogDto operationLogDto) {
        Page<OperationLog> rolePage = operationLogMapper.selectPage(page, Wrappers.lambdaQuery(OperationLog.class)
                .like(StringUtils.hasLength(operationLogDto.getOperModul()), OperationLog::getOperModul, operationLogDto.getOperModul())
                .likeRight(StringUtils.hasLength(operationLogDto.getOperType()), OperationLog::getOperType, operationLogDto.getOperType())
                .like(StringUtils.hasLength(operationLogDto.getNickName()), OperationLog::getNickName, operationLogDto.getNickName())
                .eq(operationLogDto.getState() != null, OperationLog::getState, operationLogDto.getState())
                .ge(operationLogDto.getStartDateTime() != null, OperationLog::getCreateTime, operationLogDto.getStartDateTime())
                .lt(operationLogDto.getEndDateTime() != null, OperationLog::getCreateTime, operationLogDto.getEndDateTime())
                .orderByDesc(OperationLog::getCreateTime));
        return new PageVo<>(rolePage.getTotal(),rolePage.getRecords());
    }
}
