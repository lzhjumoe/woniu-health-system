package com.woniuxy.health.manager.dto;

import lombok.Data;

import java.util.List;


@Data
public class RolePermissionDto {

    private Long id;//角色ID
    private String nickName;//角色的名称
    private List<Long> permissionData;
}
