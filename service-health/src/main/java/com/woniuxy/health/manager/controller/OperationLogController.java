package com.woniuxy.health.manager.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniuxy.health.aspect.OperationLogAnnotation;
import com.woniuxy.health.manager.dto.OperationLogDto;
import com.woniuxy.health.manager.vo.PageVo;
import com.woniuxy.health.model.manager.OperationLog;
import com.woniuxy.utils.result.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.woniuxy.health.manager.service.IOperationLogService;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@RestController
@RequestMapping("/operationLog")
public class OperationLogController {

    private final IOperationLogService operationLogServiceImpl;

    @Autowired
    public OperationLogController(IOperationLogService operationLogServiceImpl){
        this.operationLogServiceImpl = operationLogServiceImpl;
    }

    @PostMapping("/findAllPage/{pageSize}/{pageNum}")
    public Result findAllPage(@PathVariable("pageSize") Integer pageSize, @PathVariable("pageNum") Integer pageNum, OperationLogDto operationLogDto){
        Page<OperationLog> page = Page.of(pageNum,pageSize);
        PageVo<OperationLog> vo =  operationLogServiceImpl.findPage(page,operationLogDto);
        return Result.ok(page);
    }

    @OperationLogAnnotation(module = "日志管理-日志记录删除", type = "批量删除", description = "批量删除日志记录")
    @DeleteMapping("/deleteByIds")
    public Result deleteByIds(@RequestBody List<Long> ids) {
        operationLogServiceImpl.removeBatchByIds(ids);
        return Result.ok();
    }

    
}
