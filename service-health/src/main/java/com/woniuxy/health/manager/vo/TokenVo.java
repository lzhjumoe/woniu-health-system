package com.woniuxy.health.manager.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TokenVo {
    private String accessToken;//短令牌
    private String refreshToken;//长令牌
    private Long id;//登录者的ID
    private String account; //登录账号
    private String currentUser;//登录者的名称
}
