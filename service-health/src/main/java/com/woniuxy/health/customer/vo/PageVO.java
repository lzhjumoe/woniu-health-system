package com.woniuxy.health.customer.vo;

import com.woniuxy.health.model.customer.UserWalk;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageVO<T> {
    private Long total;
    private List<T> records;

}
