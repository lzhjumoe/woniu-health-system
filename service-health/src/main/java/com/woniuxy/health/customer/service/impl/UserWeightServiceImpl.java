package com.woniuxy.health.customer.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.customer.mapper.UserWeightMapper;
import com.woniuxy.health.customer.vo.PageVO;
import com.woniuxy.health.customer.vo.UserWeightVO;
import com.woniuxy.health.model.customer.User;
import com.woniuxy.health.model.customer.UserWeight;
import com.woniuxy.health.customer.service.IUserWeightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * <p>
 * 体重表 服务实现类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Service
public class UserWeightServiceImpl extends ServiceImpl<UserWeightMapper, UserWeight> implements IUserWeightService {

    private final UserWeightMapper userWeightMapper;

    @Autowired
    public UserWeightServiceImpl(UserWeightMapper userWeightMapper) {
        this.userWeightMapper = userWeightMapper;
    }

    //分页查询
    @Override
    public PageVO<UserWeightVO> getPage(Page<UserWeightVO> page, String tel, Integer state) {

        /*userWeightMapper.selectPage(page,Wrappers.lambdaQuery(UserWeightVO.class)
                .eq(StringUtils.hasLength(tel),User::getUserTel,tel))*/

        Page<UserWeightVO> weightVOPage = userWeightMapper.selectPageAll(page,tel,state);
        return new PageVO<>(weightVOPage.getTotal(),weightVOPage.getRecords());
    }
}
