package com.woniuxy.health.customer.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.customer.mapper.UserMapper;
import com.woniuxy.health.customer.mapper.UserWalkMapper;
import com.woniuxy.health.model.customer.User;
import com.woniuxy.health.model.customer.UserWalk;
import com.woniuxy.health.customer.service.IUserWalkService;
import com.woniuxy.health.customer.vo.PageVO;
import com.woniuxy.health.customer.vo.UserWalkVO;
import jodd.time.TimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 走步表 服务实现类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Service
public class UserWalkServiceImpl extends ServiceImpl<UserWalkMapper, UserWalk> implements IUserWalkService {

    private final UserWalkMapper userWalkMapper;
    private final UserMapper userMapper;
    private final RedisTemplate redisTemplate;


    @Autowired
    public UserWalkServiceImpl(UserWalkMapper userWalkMapper, UserMapper userMapper, RedisTemplate redisTemplate) {
        this.userWalkMapper = userWalkMapper;
        this.userMapper = userMapper;
        this.redisTemplate = redisTemplate;
    }



    //分页查询
    @Override
    public PageVO<UserWalkVO> getPage(Page<UserWalkVO> page, String tel, Integer walkStep) {
        Page<UserWalkVO> userWalkVOPage = userWalkMapper.selectUserWalkPage(page, tel, walkStep);

        return new PageVO<>(userWalkVOPage.getTotal(),userWalkVOPage.getRecords());
    }

}
