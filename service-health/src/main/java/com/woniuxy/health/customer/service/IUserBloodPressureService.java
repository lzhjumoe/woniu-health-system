package com.woniuxy.health.customer.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniuxy.health.customer.vo.PageVO;
import com.woniuxy.health.customer.vo.UserBloodGlucoseVO;
import com.woniuxy.health.customer.vo.UserBloodPressureVO;
import com.woniuxy.health.model.customer.UserBloodPressure;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 血压表 服务类
 * </p>
 *
 * @author godric
 * @since 2023-09-08
 */
public interface IUserBloodPressureService extends IService<UserBloodPressure> {
    PageVO<UserBloodPressureVO> getPage(Page<UserBloodPressureVO> page, String tel, Integer state);
}
