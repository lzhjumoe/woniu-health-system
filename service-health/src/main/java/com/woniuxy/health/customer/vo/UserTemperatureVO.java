package com.woniuxy.health.customer.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

@Getter
@Setter
@ToString
public class UserTemperatureVO {
    @ApiModelProperty("主键")
    private Long id;

    @ApiModelProperty("电话号码")
    @ExcelProperty(value="电话号码",index = 0)
    private String userTel;

    @ApiModelProperty("昵称")
    @ExcelProperty(value="昵称",index = 1)
    private String nickname;
    @ApiModelProperty("体温")
    @ExcelProperty(value="体温/摄氏度",index = 2)
    private Double temperature;

    @ApiModelProperty("测量时间")
    @ExcelProperty(value="测量时间",index = 3)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date measurementTime;

    @ApiModelProperty("状态 五种【正常（36-37），低热（37.2-38），中热（38.1-39），高热（39.1-40），超高热（40以上）】")
    @ExcelProperty(value="状态 五种：0正常（36-37），1低热（37.2-38），2中热（38.1-39），3高热（39.1-40），4超高热（40以上）",index = 4)
    private Integer state;

}
