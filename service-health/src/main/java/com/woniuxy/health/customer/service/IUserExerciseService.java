package com.woniuxy.health.customer.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniuxy.health.customer.vo.PageVO;
import com.woniuxy.health.customer.vo.UserExerciseVO;
import com.woniuxy.health.model.customer.UserExercise;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 运动表 服务类
 * </p>
 *
 * @author godric
 * @since 2023-09-08
 */
public interface IUserExerciseService extends IService<UserExercise> {

    PageVO<UserExerciseVO> getPage(Page<UserExerciseVO> page,String tel);
}
