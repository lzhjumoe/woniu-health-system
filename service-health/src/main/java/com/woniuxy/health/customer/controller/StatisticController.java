package com.woniuxy.health.customer.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.woniuxy.health.customer.service.IUserService;
import com.woniuxy.health.model.customer.User;
import com.woniuxy.utils.result.Result;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;


import java.util.List;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author xhr
 * @since 2023-09-13
 */
@RestController
@RequestMapping("/statistic")
@CrossOrigin
public class StatisticController {

    private final IUserService userServiceImpl;

    public StatisticController(IUserService userServiceImpl){
        this.userServiceImpl = userServiceImpl;
    }

    //统计会员/非会员数量
    @GetMapping("/countUser/{isMember}")
    public Result<Long> countUser(@PathVariable Integer isMember){
        long count = userServiceImpl.count(Wrappers.lambdaQuery(User.class).eq(User::getIsMember, isMember));
        return Result.ok(count);
    }



}
