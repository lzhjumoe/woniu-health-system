package com.woniuxy.health.customer.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.woniuxy.health.model.customer.User;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
public interface IUserService extends IService<User> {


    List<Map<String, Integer>> getAgeCountList();

}
