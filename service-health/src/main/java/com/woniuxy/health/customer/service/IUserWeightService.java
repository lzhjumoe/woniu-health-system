package com.woniuxy.health.customer.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.woniuxy.health.customer.vo.PageVO;
import com.woniuxy.health.customer.vo.UserWeightVO;
import com.woniuxy.health.model.customer.UserWeight;

/**
 * <p>
 * 体重表 服务类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
public interface IUserWeightService extends IService<UserWeight> {

    PageVO<UserWeightVO> getPage(Page<UserWeightVO> page, String tel, Integer state);
}
