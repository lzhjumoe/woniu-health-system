package com.woniuxy.health.customer.util;


import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class SchedulerForRedis {

    @Value("${job.corn}")
    private String jobCorn;

    @Autowired
    private Scheduler scheduler;

    @PostConstruct
    //在属性注入后调用该方法
    public void startScheduler() throws SchedulerException {
        JobDetail jobDetail = JobBuilder
                .newJob(Myjob.class)
                .withIdentity("jobForRedis","group1")
                .build();

        CronTrigger trigger = TriggerBuilder.newTrigger()
                .withIdentity("triggerForRedis", "group1")
                .withSchedule(CronScheduleBuilder.cronSchedule("*/5 * * * * ? *"))  //0 0 * * * ?
                .build();

        scheduler.scheduleJob(jobDetail,trigger);
        scheduler.start();
    }

//    //更新停止规则
//    public void stopScheduler() throws SchedulerException {
//        scheduler.shutdown(true);
//    }
//
//    //更新指定触发器规则
//    public void updateScheduler(String cronExpression) throws SchedulerException {
//        Trigger oldTrigger = scheduler.getTrigger(new TriggerKey("triggerForRedis", "group1"));
//
//        Trigger newTrigger = TriggerBuilder.newTrigger()
//                .withIdentity("triggerForRedis", "group1")
//                .withSchedule(CronScheduleBuilder.cronSchedule(cronExpression))
//                .build();
//
//        scheduler.rescheduleJob(oldTrigger.getKey(), newTrigger);
//    }
}
