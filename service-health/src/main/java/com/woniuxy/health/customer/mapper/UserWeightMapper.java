package com.woniuxy.health.customer.mapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniuxy.health.customer.vo.UserWeightVO;
import org.apache.ibatis.annotations.Mapper;
import com.woniuxy.health.model.customer.UserWeight;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 体重表 Mapper 接口
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Mapper
public interface UserWeightMapper extends BaseMapper<UserWeight> {

    Page<UserWeightVO> selectPageAll(@Param("page") Page<UserWeightVO> page, @Param("tel") String tel,@Param("state") Integer state);

}
