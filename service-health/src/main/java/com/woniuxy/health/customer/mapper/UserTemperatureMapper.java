package com.woniuxy.health.customer.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniuxy.health.customer.vo.UserTemperatureVO;
import com.woniuxy.health.model.customer.UserTemperature;
import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 体温表 Mapper 接口
 * </p>
 *
 * @author godric
 * @since 2023-09-08
 */
@Mapper
public interface UserTemperatureMapper extends BaseMapper<UserTemperature> {
    Page<UserTemperatureVO> selectTemperaturePage(@Param("page") Page<UserTemperatureVO> page, @Param("tel") String tel, @Param("state") Integer state);

}
