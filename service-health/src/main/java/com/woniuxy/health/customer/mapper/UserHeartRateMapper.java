package com.woniuxy.health.customer.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniuxy.health.customer.vo.UserHeartRateVO;
import com.woniuxy.health.customer.vo.UserWeightVO;
import org.apache.ibatis.annotations.Mapper;
import com.woniuxy.health.model.customer.UserHeartRate;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 心率表 Mapper 接口
 * </p>
 *
 * @author godric
 * @since 2023-09-08
 */
@Mapper
public interface UserHeartRateMapper extends BaseMapper<UserHeartRate> {

    Page<UserHeartRateVO> selectHRPage(@Param("page") Page<UserHeartRateVO> page, @Param("tel") String tel, @Param("state") Integer state);

}
