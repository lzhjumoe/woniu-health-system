package com.woniuxy.health.customer.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.models.auth.In;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Time;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserWeightVO {
    private Long id;

    @ApiModelProperty("手机号")
    @ExcelProperty(value="手机号",index = 0)
    private String userTel;

    @ApiModelProperty("昵称")
    @ExcelProperty(value="昵称",index = 1)
    private String nickname;
    @ApiModelProperty("体重")
    @ExcelProperty(value="体重/KG",index = 2)
    private Double userWeight;
    @ApiModelProperty("BMI")
    @ExcelProperty(value="BMI",index = 3)
    private Double userBmi;

    @ApiModelProperty("状态 四种：0偏瘦 1标准 2偏胖 3肥胖")
    @ExcelProperty(value="状态",index = 4)
    private Integer state;

}
