package com.woniuxy.health.customer.controller;

import com.alibaba.excel.EasyExcel;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniuxy.health.aspect.OperationLogAnnotation;
import com.woniuxy.health.customer.service.IUserBloodPressureService;
import com.woniuxy.health.customer.vo.PageVO;
import com.woniuxy.health.customer.vo.UserBloodGlucoseVO;
import com.woniuxy.health.customer.vo.UserBloodPressureVO;
import com.woniuxy.utils.result.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 * 血压表 前端控制器
 * </p>
 *
 * @author godric
 * @since 2023-09-08
 */
@RestController
@CrossOrigin
@RequestMapping("/userBloodPressure")
public class UserBloodPressureController {

    private final IUserBloodPressureService userBloodPressureServiceImpl;
    public UserBloodPressureController(IUserBloodPressureService userBloodPressureServiceImpl){
        this.userBloodPressureServiceImpl = userBloodPressureServiceImpl;
    }

    //分页查询
    @GetMapping("/page")
    public Result getPage(Integer pageSize,
                          Integer pageNum,
                          String tel,
                          Integer state){

        Page<UserBloodPressureVO> page = Page.of(pageNum,pageSize);

        PageVO<UserBloodPressureVO> pageVO = userBloodPressureServiceImpl.getPage(page,tel,state);
        return Result.ok(pageVO);

    }

    //导出
    @OperationLogAnnotation(module = "用户血压记录", type = "导出excel", description = "导出用户血压记录")
    @PostMapping("/import")
    public void ExcelImport(HttpServletResponse response, Integer pageSize,
                            Integer pageNum,
                            String tel,
                            Integer state) throws IOException {

        Page<UserBloodPressureVO> page = Page.of(pageNum,pageSize);
        //查询数据方法
        PageVO<UserBloodPressureVO> allPage = userBloodPressureServiceImpl.getPage(page, tel,state);

        //设置请求类型
        response.setContentType("application/vnd.ms-excel");
        response.setCharacterEncoding("utf-8");

        // 设置请求头,这里URLEncoder.encode可以防止中文乱码,当然和easyexcel没有关系
        String fileName = URLEncoder.encode("血压表", "UTF-8");
        response.setHeader("Content-disposition", "attachment;filename=" + fileName + ".xlsx");
        System.out.println(allPage.getRecords());
        // write()
        //1.参数一:输出流
        //2.参数二:导出所参考的实体类
        //sheet()
        //1.参数一:表格名称
        //dowrite()
        //1.参数一:传输数据
        EasyExcel.write(response.getOutputStream(), UserBloodPressureVO.class)
                .sheet("sheet0")
                .doWrite(allPage.getRecords());

    }
}
