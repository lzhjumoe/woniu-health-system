package com.woniuxy.health.customer.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniuxy.health.customer.vo.PageVO;
import com.woniuxy.health.customer.vo.UserBloodPressureVO;
import com.woniuxy.health.customer.vo.UserTemperatureVO;
import com.woniuxy.health.customer.vo.UserWeightVO;
import org.apache.ibatis.annotations.Mapper;
import com.woniuxy.health.model.customer.UserBloodPressure;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 血压表 Mapper 接口
 * </p>
 *
 * @author godric
 * @since 2023-09-08
 */
@Mapper
public interface UserBloodPressureMapper extends BaseMapper<UserBloodPressure> {
    Page<UserBloodPressureVO> selectBPPage(@Param("page")Page<UserBloodPressureVO> page,@Param("tel") String tel,@Param("state") Integer state);

}
