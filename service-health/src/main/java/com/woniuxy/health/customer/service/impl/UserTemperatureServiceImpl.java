package com.woniuxy.health.customer.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.customer.mapper.UserTemperatureMapper;
import com.woniuxy.health.customer.service.IUserTemperatureService;
import com.woniuxy.health.customer.vo.PageVO;
import com.woniuxy.health.customer.vo.UserTemperatureVO;
import com.woniuxy.health.model.customer.UserTemperature;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 体温表 服务实现类
 * </p>
 *
 * @author godric
 * @since 2023-09-08
 */
@Service
public class UserTemperatureServiceImpl extends ServiceImpl<UserTemperatureMapper, UserTemperature> implements IUserTemperatureService {

    private final UserTemperatureMapper userTemperatureMapper;
    public UserTemperatureServiceImpl(UserTemperatureMapper userTemperatureMapper){
        this.userTemperatureMapper = userTemperatureMapper;
    }

    //分页查询
    @Override
    public PageVO<UserTemperatureVO> getPage(Page<UserTemperatureVO> page, String tel,Integer state) {
        Page<UserTemperatureVO> userTemperatureVOPage = userTemperatureMapper.selectTemperaturePage(page,tel,state);

        return new PageVO<>(userTemperatureVOPage.getTotal(),userTemperatureVOPage.getRecords());
    }
}
