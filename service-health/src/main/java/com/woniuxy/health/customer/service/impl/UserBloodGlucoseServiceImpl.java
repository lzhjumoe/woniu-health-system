package com.woniuxy.health.customer.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.customer.mapper.UserBloodGlucoseMapper;
import com.woniuxy.health.customer.service.IUserBloodGlucoseService;
import com.woniuxy.health.customer.vo.PageVO;
import com.woniuxy.health.customer.vo.UserBloodGlucoseVO;
import com.woniuxy.health.model.customer.UserBloodGlucose;
import org.springframework.stereotype.Service;
/**
 * <p>
 * 血糖表 服务实现类
 * </p>
 *
 * @author godric
 * @since 2023-09-08
 */
@Service
public class UserBloodGlucoseServiceImpl extends ServiceImpl<UserBloodGlucoseMapper, UserBloodGlucose> implements IUserBloodGlucoseService {

    private final UserBloodGlucoseMapper userBloodGlucoseMapper;
    public UserBloodGlucoseServiceImpl(UserBloodGlucoseMapper userBloodGlucoseMapper){
        this.userBloodGlucoseMapper = userBloodGlucoseMapper;
    }

    //分页查询
    @Override
    public PageVO<UserBloodGlucoseVO> getPage(Page<UserBloodGlucoseVO> page, String tel,Integer state) {
        Page<UserBloodGlucoseVO> userBloodGlucoseVOPage = userBloodGlucoseMapper.selectBGPage(page,tel,state);
        System.out.println("userBloodGlucoseVOPage = " + userBloodGlucoseVOPage);
        return new PageVO<>(userBloodGlucoseVOPage.getTotal(),userBloodGlucoseVOPage.getRecords());
    }
}
