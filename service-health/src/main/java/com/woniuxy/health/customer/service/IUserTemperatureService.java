package com.woniuxy.health.customer.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniuxy.health.customer.vo.PageVO;
import com.woniuxy.health.customer.vo.UserTemperatureVO;
import com.woniuxy.health.model.customer.UserTemperature;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 体温表 服务类
 * </p>
 *
 * @author godric
 * @since 2023-09-08
 */
public interface IUserTemperatureService extends IService<UserTemperature> {

    PageVO<UserTemperatureVO> getPage(Page<UserTemperatureVO> page, String tel,Integer state);
}
