package com.woniuxy.health.customer.vo;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@ContentRowHeight(10)
@HeadRowHeight(20)
@ColumnWidth(25)
public class UserBloodPressureVO {
    @ExcelIgnore
    private Long id;

    @ApiModelProperty("电话号码")
    @ExcelProperty(value="电话号码",index = 0)
    private String userTel;

    @ApiModelProperty("昵称")
    @ExcelProperty(value="id",index = 1)
    private String nickname;

    @ApiModelProperty("收缩压")
    @ExcelProperty(value="收缩压/mmHg",index = 2)
    private Integer systolicPressure;

    @ApiModelProperty("舒张压")
    @ExcelProperty(value="舒张压/mmHg",index = 3)
    private Integer diastolicPressure;

    @ApiModelProperty("平均收缩压")
    @ExcelProperty(value="平均收缩压/mmHg",index = 4)
    private Integer meanSp;

    @ApiModelProperty("平均舒张压")
    @ExcelProperty(value="平均舒张压/mmHg",index = 5)
    private Integer meanDp;

    @ApiModelProperty("状态 六种 【低血压（收缩压小于90或舒张压小于60）正常（收缩压90-119或舒张压69-79）高血压（收缩压120-129或舒张压60-79）高血压I阶段（收缩压130-139或舒张压80-89）高血压II阶段（收缩压140-180或舒张压90-120）高血压危象（收缩压大于180或舒张压大于120）】")
    @ExcelProperty(value="状态： 0低血压 1正常 2高血压 3高血压I阶段 4高血压II阶段 5高血压危象",index = 6)
    private Integer state;

}
