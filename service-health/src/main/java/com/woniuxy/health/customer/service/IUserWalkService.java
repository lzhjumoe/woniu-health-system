package com.woniuxy.health.customer.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.woniuxy.health.model.customer.UserWalk;
import com.woniuxy.health.customer.vo.PageVO;
import com.woniuxy.health.customer.vo.UserWalkVO;

/**
 * <p>
 * 走步表 服务类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
public interface IUserWalkService extends IService<UserWalk> {

    PageVO<UserWalkVO> getPage(Page<UserWalkVO> page, String tel, Integer walkStep);


}
