package com.woniuxy.health.customer.dto;

import lombok.Data;

@Data
public class UserDTO {

    //@ApiModelProperty("用户名（账号）")
    private String username;

    //@ApiModelProperty("登陆密码")
    private String loginPwd;
    private String validateCode;

}
