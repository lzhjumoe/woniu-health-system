package com.woniuxy.health.customer.vo;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

@Getter
@Setter
@ToString
@ContentRowHeight(10)
@HeadRowHeight(20)
@ColumnWidth(25)
public class UserBloodGlucoseVO {
    @ApiModelProperty("血糖表主键")
    @ExcelIgnore
    private Long id;

    @ApiModelProperty("电话号码")
    @ExcelProperty(value="电话号码",index = 0)
    private String userTel;

    @ApiModelProperty("昵称")
    @ExcelProperty(value="昵称",index = 1)
    private String nickname;

    @ApiModelProperty("血糖 当空腹血糖大于等于6.1mmol/L或餐后血糖大于等于7.8mmol/L时应注意及时去医院就诊")
    @ExcelProperty(value="血糖 mmol/L",index = 2)
    private Integer bloodGlucose;

    @ApiModelProperty("测量时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ExcelProperty(value="测量时间",index = 3)
    private Date measurementTime;

    @ApiModelProperty("hemoglobin A1C（糖化血红蛋白）正常值为低于5.7%，糖尿病前期介于5.7%-6.4%之间，6.5或之上为糖尿病")
    @ExcelProperty(value="糖化血红蛋白 %",index = 4)
    private Integer hbA1c;

    @ExcelProperty(value="状态",index = 5)
    private Integer state;


}
