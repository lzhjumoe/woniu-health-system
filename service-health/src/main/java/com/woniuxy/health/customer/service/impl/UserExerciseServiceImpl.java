package com.woniuxy.health.customer.service.impl;



import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.customer.mapper.UserExerciseMapper;
import com.woniuxy.health.customer.service.IUserExerciseService;
import com.woniuxy.health.customer.vo.PageVO;
import com.woniuxy.health.customer.vo.UserExerciseVO;
import com.woniuxy.health.model.customer.UserExercise;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 运动表 服务实现类
 * </p>
 *
 * @author godric
 * @since 2023-09-08
 */
@Service
public class UserExerciseServiceImpl extends ServiceImpl<UserExerciseMapper, UserExercise> implements IUserExerciseService {

    private final UserExerciseMapper userExerciseMapper;
    public UserExerciseServiceImpl(UserExerciseMapper userExerciseMapper){
        this.userExerciseMapper = userExerciseMapper;
    }

    //分页查询
    @Override
    public PageVO<UserExerciseVO> getPage(Page<UserExerciseVO> page, String tel) {
        Page<UserExerciseVO> userExerciseVOPage = userExerciseMapper.selectExercisePage(page,tel);

        return new PageVO<>(userExerciseVOPage.getTotal(),userExerciseVOPage.getRecords());
    }
}
