package com.woniuxy.health.customer.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.customer.mapper.UserAddrMapper;
import com.woniuxy.health.model.customer.UserAddr;
import com.woniuxy.health.customer.service.IUserAddrService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Service
public class UserAddrServiceImpl extends ServiceImpl<UserAddrMapper, UserAddr> implements IUserAddrService {

    private final UserAddrMapper userAddrMapper;

    @Autowired
    public UserAddrServiceImpl(UserAddrMapper userAddrMapper){
        this.userAddrMapper = userAddrMapper;
    }

}
