package com.woniuxy.health.customer.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.sql.Time;
import java.util.Date;

@Getter
@Setter
@ToString
@ContentRowHeight(10)
@HeadRowHeight(20)
@ColumnWidth(25)
public class UserExerciseVO {
    @ApiModelProperty("主键")
    private Long id;

    @ApiModelProperty("电话号码")
    @ExcelProperty(value="电话号码",index = 0)
    private String userTel;

    @ApiModelProperty("昵称")
    @ExcelProperty(value="昵称",index = 1)
    private String nickname;

    @ApiModelProperty("跑步距离")
    @ExcelProperty(value="跑步距离",index = 2)
    private Integer runMileage;

    @ApiModelProperty("深蹲数量")
    @ExcelProperty(value="深蹲数量",index = 3)
    private Integer fullSquat;

    @ApiModelProperty("仰卧起做数量")
    @ExcelProperty(value="仰卧起做数量",index = 4)
    private Integer sitUp;

    @ApiModelProperty("俯卧撑数量")
    @ExcelProperty(value="俯卧撑数量",index = 5)
    private Integer pressUp;

    @ApiModelProperty("跑步时长")
    @ExcelProperty(value="跑步时长",index = 6)
    private Time runDuration;
    @ApiModelProperty("深蹲时长")
    @ExcelProperty(value="深蹲时长",index = 7)
    private Time fullSquatDuration;
    @ApiModelProperty("仰卧起坐时长")
    @ExcelProperty(value="仰卧起坐时长",index = 8)
    private Time sitUpDuration;
    @ApiModelProperty("俯卧撑时长")
    @ExcelProperty(value="俯卧撑时长",index = 9)
    private Time pressUpDuration;
}
