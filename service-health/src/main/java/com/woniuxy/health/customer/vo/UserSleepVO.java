package com.woniuxy.health.customer.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Date;

@Getter
@Setter
@ToString
public class UserSleepVO {
    @ApiModelProperty("主键")
    private Long id;

    @ApiModelProperty("电话号码")
    @ExcelProperty(value="电话号码",index = 0)
    private String userTel;

    @ApiModelProperty("昵称")
    @ExcelProperty(value="昵称",index = 1)
    private String nickname;

    @ApiModelProperty("累计睡眠时长")
    @ExcelProperty(value="累计睡眠时长",index = 2)
    @JsonFormat(pattern = "HH:mm:ss")
    private Date totalSleepTime;

    @ApiModelProperty("累计深睡时长")
    @ExcelProperty(value="累计深睡时长",index = 3)
    @JsonFormat(pattern = "HH:mm:ss")
    private Date totalDeepSleepTime;

    @ApiModelProperty("累计浅睡时长")
    @ExcelProperty(value="累计浅睡时长",index = 4)
    @JsonFormat(pattern = "HH:mm:ss")
    private Time totalLightSleepTime;

    @ApiModelProperty("累计清醒时长")
    @ExcelProperty(value="累计清醒时长",index = 5)
    @JsonFormat(pattern = "HH:mm:ss")
    private Date totalAwakeTime;

    @ApiModelProperty("入睡时间")
    @ExcelProperty(value="入睡时间",index = 6)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date onsetOfSleep;

    @ApiModelProperty("结束睡眠时间")
    @ExcelProperty(value="昵称",index = 7)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date fallAsleepEndTime;

    // public String getTotalLightSleepTimeFormatted() {
    //     SimpleDateFormat formatter = new SimpleDateFormat("HH小时mm分钟");
    //     return formatter.format(totalLightSleepTime);
    // }
}
