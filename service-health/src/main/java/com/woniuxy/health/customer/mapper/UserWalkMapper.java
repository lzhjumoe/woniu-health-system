package com.woniuxy.health.customer.mapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniuxy.health.customer.vo.UserWalkVO;
import org.apache.ibatis.annotations.Mapper;
import com.woniuxy.health.model.customer.UserWalk;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 走步表 Mapper 接口
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Mapper
public interface UserWalkMapper extends BaseMapper<UserWalk> {

    Page<UserWalkVO> selectUserWalkPage(@Param("page") Page<UserWalkVO> page, @Param("tel") String tel, @Param("walkStep") Integer walkStep);
}
