package com.woniuxy.health.customer.service.impl;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.customer.mapper.UserBloodPressureMapper;
import com.woniuxy.health.customer.service.IUserBloodPressureService;
import com.woniuxy.health.customer.vo.PageVO;
import com.woniuxy.health.customer.vo.UserBloodPressureVO;
import com.woniuxy.health.model.customer.UserBloodPressure;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
/**
 * <p>
 * 血压表 服务实现类
 * </p>
 *
 * @author godric
 * @since 2023-09-08
 */
@Service
public class UserBloodPressureServiceImpl extends ServiceImpl<UserBloodPressureMapper, UserBloodPressure> implements IUserBloodPressureService {

    private final UserBloodPressureMapper userBloodPressureMapper;
    public UserBloodPressureServiceImpl(UserBloodPressureMapper userBloodPressureMapper){
        this.userBloodPressureMapper = userBloodPressureMapper;
    }

    //分页查询
    @Override
    public PageVO<UserBloodPressureVO> getPage(Page<UserBloodPressureVO> page, String tel, Integer state) {
        Page<UserBloodPressureVO> userBloodPressureVOPage = userBloodPressureMapper.selectBPPage(page,tel,state);

        return new PageVO<>(userBloodPressureVOPage.getTotal(),userBloodPressureVOPage.getRecords());
    }
}
