package com.woniuxy.health.customer.controller;

import com.alibaba.excel.EasyExcel;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniuxy.health.aspect.OperationLogAnnotation;
import com.woniuxy.health.customer.service.IUserExerciseService;
import com.woniuxy.health.customer.vo.PageVO;
import com.woniuxy.health.customer.vo.UserBloodGlucoseVO;
import com.woniuxy.health.customer.vo.UserExerciseVO;
import com.woniuxy.utils.result.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 * 运动表 前端控制器
 * </p>
 *
 * @author godric
 * @since 2023-09-08
 */
@RestController
@CrossOrigin
@RequestMapping("/userExercise")
public class UserExerciseController {

    private final IUserExerciseService userExerciseServiceImpl;
    public UserExerciseController(IUserExerciseService userExerciseServiceImpl){
        this.userExerciseServiceImpl = userExerciseServiceImpl;
    }

    //分页查询
    @GetMapping("/page")
    public Result getPage(Integer pageSize,
                          Integer pageNum,
                          String tel){

        Page<UserExerciseVO> page = Page.of(pageNum,pageSize);

        PageVO<UserExerciseVO> pageVO = userExerciseServiceImpl.getPage(page,tel);
        return Result.ok(pageVO);

    }

    //导出
    @OperationLogAnnotation(module = "用户运动记录", type = "导出excel", description = "导出用户运动记录")
    @PostMapping("/import")
    public void ExcelImport(HttpServletResponse response, Integer pageSize,
                            Integer pageNum,
                            String tel) throws IOException {

        Page<UserExerciseVO> page = Page.of(pageNum,pageSize);
        //查询数据方法
        PageVO<UserExerciseVO> allPage = userExerciseServiceImpl.getPage(page, tel);

        //设置请求类型
        response.setContentType("application/vnd.ms-excel");
        response.setCharacterEncoding("utf-8");

        // 设置请求头,这里URLEncoder.encode可以防止中文乱码,当然和easyexcel没有关系
        String fileName = URLEncoder.encode("运动表", "UTF-8");
        response.setHeader("Content-disposition", "attachment;filename=" + fileName + ".xlsx");


        System.out.println(allPage.getRecords());
        // write()
        //1.参数一:输出流
        //2.参数二:导出所参考的实体类
        //sheet()
        //1.参数一:表格名称
        //dowrite()
        //1.参数一:传输数据
        EasyExcel.write(response.getOutputStream(), UserExerciseVO.class)
                .sheet("sheet0")
                .doWrite(allPage.getRecords());

    }
}
