package com.woniuxy.health.customer.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniuxy.health.customer.vo.UserSleepVO;
import com.woniuxy.health.customer.vo.UserWeightVO;
import org.apache.ibatis.annotations.Mapper;
import com.woniuxy.health.model.customer.UserSleep;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 睡眠表 Mapper 接口
 * </p>
 *
 * @author godric
 * @since 2023-09-08
 */
@Mapper
public interface UserSleepMapper extends BaseMapper<UserSleep> {

    Page<UserSleepVO> selectSleepPage(@Param("page") Page<UserSleepVO> page, @Param("tel") String tel);

}
