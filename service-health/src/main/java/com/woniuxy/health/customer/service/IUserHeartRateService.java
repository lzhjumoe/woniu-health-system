package com.woniuxy.health.customer.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniuxy.health.customer.vo.PageVO;
import com.woniuxy.health.customer.vo.UserHeartRateVO;
import com.woniuxy.health.model.customer.UserHeartRate;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 心率表 服务类
 * </p>
 *
 * @author godric
 * @since 2023-09-08
 */
public interface IUserHeartRateService extends IService<UserHeartRate> {

    PageVO<UserHeartRateVO> getPage(Page<UserHeartRateVO> page, String tel,Integer state);
}
