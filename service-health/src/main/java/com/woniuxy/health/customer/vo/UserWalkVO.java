package com.woniuxy.health.customer.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.sql.Time;

@Getter
@Setter
@ToString
public class UserWalkVO {

    private Long id;

    @ApiModelProperty("手机号")
    @ExcelProperty(value="手机号",index = 0)
    private String userTel;

    @ApiModelProperty("昵称")
    @ExcelProperty(value="昵称",index = 1)
    private String nickname;
    @ApiModelProperty("步数")
    @ExcelProperty(value="步数",index = 2)
    private Integer walkStep;

    @ApiModelProperty("公里数")
    @ExcelProperty(value="公里数",index = 3)
    private Object walkKm;

    @ApiModelProperty("消耗热量")
    @ExcelProperty(value="消耗热量/cal",index = 4)
    private Object walkEnergy;

    @ApiModelProperty("运动开始时间")
    @ExcelProperty(value="测量时间",index = 5)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Time startTime;

    @ApiModelProperty("运动结束时间")
    @ExcelProperty(value="运动结束时间",index = 6)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Time endTime;

    @ApiModelProperty("运动总时长")
    @ExcelProperty(value="运动总时长",index = 7)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Time allTime;
}
