package com.woniuxy.health.customer.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class UserHeartRateVO {
    @ApiModelProperty("主键")
    private Long id;

    @ApiModelProperty("电话号码")
    @ExcelProperty(value="电话号码",index = 0)
    private String userTel;

    @ApiModelProperty("昵称")
    @ExcelProperty(value="昵称",index = 1)
    private String nickname;
    @ApiModelProperty("心率")
    @ExcelProperty(value="心率/分钟",index = 2)
    private Integer heartRate;

    @ApiModelProperty("最高心率")
    @ExcelProperty(value="最高心率/分钟",index = 3)
    private Integer maxHr;

    @ApiModelProperty("最低心率")
    @ExcelProperty(value="最低心率/分钟",index = 4)
    private Integer minHr;

    @ApiModelProperty("平均心率")
    @ExcelProperty(value="平均心率/分钟",index = 5)
    private Integer meanHr;

    @ApiModelProperty("状态（低心率<60，静息心率60-80，常规心率80-100，高心率>100）")
    @ExcelProperty(value="状态（低心率<60，静息心率60-80，常规心率80-100，高心率>100）",index = 6)
    private Integer state;

}
