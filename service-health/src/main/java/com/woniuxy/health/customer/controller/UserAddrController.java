package com.woniuxy.health.customer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.woniuxy.health.customer.service.IUserAddrService;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@RestController
@RequestMapping("/userAddr")
public class UserAddrController {

    private final IUserAddrService userAddrServiceImpl;

    @Autowired
    public UserAddrController(IUserAddrService userAddrServiceImpl){
        this.userAddrServiceImpl = userAddrServiceImpl;
    }

}
