package com.woniuxy.health.customer.mapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniuxy.health.customer.vo.UserWalkVO;
import org.apache.ibatis.annotations.Mapper;
import com.woniuxy.health.model.customer.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {

    Page<UserWalkVO> selectUserPage(@Param("page") Page<UserWalkVO> page, @Param("tel") String tel, @Param("walkStep") Integer walkStep);

    List<Map<String, Integer>> searchCountByAge();
}
