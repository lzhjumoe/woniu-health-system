package com.woniuxy.health.customer.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniuxy.health.customer.vo.UserExerciseVO;
import org.apache.ibatis.annotations.Mapper;
import com.woniuxy.health.model.customer.UserExercise;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 运动表 Mapper 接口
 * </p>
 *
 * @author godric
 * @since 2023-09-08
 */
@Mapper
public interface UserExerciseMapper extends BaseMapper<UserExercise> {
    Page<UserExerciseVO> selectExercisePage(@Param("page") Page<UserExerciseVO> page, @Param("tel") String tel);


}
