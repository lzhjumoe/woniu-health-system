package com.woniuxy.health.customer.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniuxy.health.customer.vo.PageVO;
import com.woniuxy.health.customer.vo.UserSleepVO;
import com.woniuxy.health.model.customer.UserSleep;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 睡眠表 服务类
 * </p>
 *
 * @author godric
 * @since 2023-09-08
 */
public interface IUserSleepService extends IService<UserSleep> {

    PageVO<UserSleepVO> getPage(Page<UserSleepVO> page, String tel);
}
