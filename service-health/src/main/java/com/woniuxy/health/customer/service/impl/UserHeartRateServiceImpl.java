package com.woniuxy.health.customer.service.impl;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.customer.mapper.UserHeartRateMapper;
import com.woniuxy.health.customer.service.IUserHeartRateService;
import com.woniuxy.health.customer.vo.PageVO;
import com.woniuxy.health.customer.vo.UserHeartRateVO;
import com.woniuxy.health.model.customer.UserHeartRate;
import com.woniuxy.utils.result.Result;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
/**
 * <p>
 * 心率表 服务实现类
 * </p>
 *
 * @author godric
 * @since 2023-09-08
 */
@Service
public class UserHeartRateServiceImpl extends ServiceImpl<UserHeartRateMapper, UserHeartRate> implements IUserHeartRateService {

    private final UserHeartRateMapper userHeartRateMapper;
    public UserHeartRateServiceImpl(UserHeartRateMapper userHeartRateMapper){
        this.userHeartRateMapper = userHeartRateMapper;
    }

    //分页查询
    @Override
    public PageVO<UserHeartRateVO> getPage(Page<UserHeartRateVO> page, String tel,Integer state) {
        Page<UserHeartRateVO> userHeartRateVOPage = userHeartRateMapper.selectHRPage(page,tel,state);

        return new PageVO<>(userHeartRateVOPage.getTotal(),userHeartRateVOPage.getRecords());
    }
}
