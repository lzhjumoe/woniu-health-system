package com.woniuxy.health.customer.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.woniuxy.health.customer.vo.PageVO;
import com.woniuxy.health.customer.vo.UserBloodGlucoseVO;
import com.woniuxy.health.model.customer.UserBloodGlucose;

/**
 * <p>
 * 血糖表 服务类
 * </p>
 *
 * @author godric
 * @since 2023-09-08
 */
public interface IUserBloodGlucoseService extends IService<UserBloodGlucose> {

    PageVO<UserBloodGlucoseVO> getPage(Page<UserBloodGlucoseVO> page, String tel,Integer state);
}
