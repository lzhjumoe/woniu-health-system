package com.woniuxy.health.customer.service.impl;



import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.customer.mapper.UserSleepMapper;
import com.woniuxy.health.customer.service.IUserSleepService;
import com.woniuxy.health.customer.vo.PageVO;
import com.woniuxy.health.customer.vo.UserSleepVO;
import com.woniuxy.health.model.customer.UserSleep;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
/**
 * <p>
 * 睡眠表 服务实现类
 * </p>
 *
 * @author godric
 * @since 2023-09-08
 */
@Service
public class UserSleepServiceImpl extends ServiceImpl<UserSleepMapper, UserSleep> implements IUserSleepService {

    private final UserSleepMapper userSleepMapper;
    public UserSleepServiceImpl(UserSleepMapper userSleepMapper){
        this.userSleepMapper = userSleepMapper;
    }

    //分页查询
    @Override
    public PageVO<UserSleepVO> getPage(Page<UserSleepVO> page, String tel) {
        Page<UserSleepVO> userSleepVOPage = userSleepMapper.selectSleepPage(page,tel);

        return new PageVO<>(userSleepVOPage.getTotal(),userSleepVOPage.getRecords());
    }
}
