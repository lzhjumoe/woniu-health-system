package com.woniuxy.health.customer.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniuxy.health.customer.vo.UserBloodGlucoseVO;
import com.woniuxy.health.customer.vo.UserWeightVO;
import org.apache.ibatis.annotations.Mapper;
import com.woniuxy.health.model.customer.UserBloodGlucose;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 血糖表 Mapper 接口
 * </p>
 *
 * @author godric
 * @since 2023-09-08
 */
@Mapper
public interface UserBloodGlucoseMapper extends BaseMapper<UserBloodGlucose> {

    Page<UserBloodGlucoseVO> selectBGPage(@Param("page") Page<UserBloodGlucoseVO> page, @Param("tel") String tel, @Param("state") Integer state);

}
