package com.woniuxy.health.customer.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import sun.rmi.runtime.Log;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserVO {

    //@ApiModelProperty("token")
    //private String token;

    //@ApiModelProperty("刷新token")
    //private String refreshToken;

    //@ApiModelProperty("用户id")
    private Long id;

    //@ApiModelProperty("昵称")
    private String nickname;
}
