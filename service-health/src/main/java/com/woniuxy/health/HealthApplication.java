package com.woniuxy.health;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.bind.annotation.CrossOrigin;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


/**
 * @Date 2023/9/4 21:10
 * @Author LZH
 * Description:
 */
@EnableRabbit
@EnableSwagger2
@EnableScheduling
@SpringBootApplication
@MapperScan(basePackages = {"com.woniuxy.health.commodity.mapper", "com.woniuxy.health.customer.mapper",
        "com.woniuxy.health.information.mapper", "com.woniuxy.health.manager.mapper", "com.woniuxy.health.medical.mapper"})
public class HealthApplication {

    public static void main(String[] args) {
        System.setProperty("spring.devtools.restart.enabled", "false");
        SpringApplication.run(HealthApplication.class, args);
    }
}
