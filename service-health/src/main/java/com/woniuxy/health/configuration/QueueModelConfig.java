package com.woniuxy.health.configuration;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * @author huang
 * @date 2023/9/13 9:55
 */
@Configuration
public class QueueModelConfig {

    // 广播交换机
    @Bean
    public FanoutExchange fanoutExchange(){
        return new FanoutExchange("fanoutExchange");
    }

    // 商品上架推送邮件队列
    @Bean
    public Queue GoodsUpLoadQueue(){
        return new Queue("GoodsUpLoadQueue");
    }

    // 队列绑定到交换机
    @Bean
    public Binding fanoutExchangeBinding01(){
        return BindingBuilder.bind(GoodsUpLoadQueue()).to(fanoutExchange());
    }
}
