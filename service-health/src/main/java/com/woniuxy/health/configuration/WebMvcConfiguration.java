package com.woniuxy.health.configuration;

import com.woniuxy.health.commodity.interceptor.UpdateCacheInterceptor;
//import com.woniuxy.operator.interceptor.LoginInterceptor;
import com.woniuxy.health.interceptor.LoginInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @Date 2023/8/10 19:47
 * @Author LZH
 * Description: 配置视图控制器
 */
@Configuration
public class WebMvcConfiguration implements WebMvcConfigurer {

    @Bean
    public ThreadLocal<String> threadLocal(){
        return new ThreadLocal<String>();
    }

    @Bean
    public LoginInterceptor loginInterceptor(){
        return new LoginInterceptor();
    }

    @Bean
    public UpdateCacheInterceptor updateCacheInterceptor(){
        return new UpdateCacheInterceptor();
    }



    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        List<String> whiteList = new ArrayList<>();
        //以后：swagger，druid 的一些请求，也需要放行
        Collections.addAll(whiteList, "/manager/login","/user/*" ,"/token/refresh", "/**/druid/**", "/oss/upload", "order-detail/download");
        registry.addInterceptor(loginInterceptor()).addPathPatterns("/**").excludePathPatterns(whiteList)
                .excludePathPatterns("/swagger-resources/**", "/webjars/**", "/v2/**", "/swagger-ui.html/**");

        registry.addInterceptor(updateCacheInterceptor()).addPathPatterns("/goodsType/**", "/goods/**");

    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
    }


}
