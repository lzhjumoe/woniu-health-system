package com.woniuxy.health.vo;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.models.auth.In;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

@Data
@Document(indexName = "goods")
public class GoodsVO {

    @Id
    private String id;

    @Field(analyzer = "smartcn", type = FieldType.Text)
    private String goodsName;

    @Field(type = FieldType.Keyword)
    private String goodsTypeName;

    @Field(type = FieldType.Text)
    private String goodsCode;

    @Field(type = FieldType.Text)
    private String goodsPhoto;

    @Field(analyzer = "smartcn", type = FieldType.Text)
    private String goodsHead;

    @Field(analyzer = "smartcn", type = FieldType.Text)
    private String goodsSubhead;

    @Field(analyzer = "smartcn", type = FieldType.Text)
    private String goodsDetail;

    @Field(type = FieldType.Double)
    private Long goodsPrice;

    @Field(type = FieldType.Integer)
    private Integer goodsStorecount;

    @Field(type = FieldType.Integer)
    private Integer goodsBuycount;

    @Field(type = FieldType.Integer)
    private Integer goodsReadcount;

    @Field(type = FieldType.Byte)
    private Byte goodsState;

    @Field(type = FieldType.Text)
    private String bigPhoto;

    @Field(type = FieldType.Integer)
    private Integer goodsOrder;

    @Field(type = FieldType.Integer)
    private Integer typeId;

    @Field(type = FieldType.Integer)
    private Integer parentId;
}
