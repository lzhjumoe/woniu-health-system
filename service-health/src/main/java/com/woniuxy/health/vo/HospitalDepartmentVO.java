package com.woniuxy.health.vo;

import com.woniuxy.health.model.medical.HospitalDepartments;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HospitalDepartmentVO {

    private Integer id;//1级科室
    private String departmentName;
    private String pName;
    private Integer pid;//1级科室对应的2级科室(多个)

}
