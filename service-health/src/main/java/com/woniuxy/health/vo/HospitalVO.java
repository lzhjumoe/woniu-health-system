package com.woniuxy.health.vo;

import com.woniuxy.health.model.medical.HospitalDepartments;
import lombok.Data;

import java.util.List;

@Data
public class HospitalVO {
    private Long id;

    private String hospitalName;

    private String hospitalImgUrl;

    private String hospitalAddress;

    private Integer hospitalTypeCode;

    private String hospitalType;

    private Integer hospitalGradeCode;

    private String hospitalGrade;

    private Integer hospitalState;
    //医院介绍
    private String hospitalIntroduction;

    private List<HospitalDepartments> department;

}
