package com.woniuxy.health.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.models.auth.In;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class GoodsOrderVO {
    private Integer id;
    private String orderCode;
    private String userName;
    private String goodsName;
    private String orderAddr;       // 收货地址
    private Integer goodsNumber;    // 购买数量
    private Integer payMethod;      // 支付方式
    private BigDecimal price;       // 价格
    @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private LocalDateTime createTime;
    @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private LocalDateTime updateTime;
    private Integer emailCode;      // 用户邮编
    private String phone;           // 用户手机
    private String logisNumber;     // 物流单号
    private Integer state;          // 订单状态
    private String province;
    private String city;
    private String area;
    private String detailAddr;
}
