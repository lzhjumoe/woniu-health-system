package com.woniuxy.health.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Time;

/**
 * @Date 2023/9/5 16:42
 * @Author LZH
 * Description:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReceptionVO {

    private Long doctorId;
    private Integer receptionCount0;
    private Integer receptionCount1;
}
