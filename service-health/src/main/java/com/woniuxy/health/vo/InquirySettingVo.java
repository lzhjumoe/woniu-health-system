package com.woniuxy.health.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InquirySettingVo {
    private Long id;//问诊设置表的主键id
    private String inquiryType; //dto转成vo，将问诊类型从数字转成文字
    private Double  inquiryPrice;
    private String doctorName;
    private Long doctorId;
}
