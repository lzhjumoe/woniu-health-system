package com.woniuxy.health.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ViewDetailsVO {
    private String doctorAnswer;
    private String diseaseDescription;
}
