package com.woniuxy.health.vo;

import com.woniuxy.health.model.medical.HospitalDepartments;
import lombok.Data;

import java.util.List;

@Data
public class HospitalDetailVO {

    //private Long id;

    private String hospitalName;

    private String hospitalImgUrl;

    private String hospitalAddress;

    private String hospitalType;

    private String hospitalGrade;

    private String hospitalIntroduction;

    private String departmentName;

}
