package com.woniuxy.health.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.woniuxy.health.utils.TimeConverter;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

@Data
@ExcelIgnoreUnannotated
public class OrderDetailVO2 {

    //tjy
    @ExcelProperty("订单编号") //在excel表中的列名
    @ColumnWidth(10) //在excel表中的列宽
    private Long id;//订单编号

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")

    @ExcelProperty(value ="订单时间", converter = TimeConverter.class) //在excel表中的列名
    @ColumnWidth(20) //在excel表中的列宽
    private LocalDateTime orderTime;//订单时间




    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime payTime;//支付时间

    private Integer paymentType;//支付类型

    @ExcelProperty("挂号费用") //在excel表中的列名
    @ColumnWidth(10) //在excel表中的列宽
    private Long orderPrice;//订单价格


    private Integer orderType;//订单价格

    @ExcelProperty("订单状态(1:付款,0:未付款)") //在excel表中的列名
    @ColumnWidth(10) //在excel表中的列宽
    private Integer orderState;//订单价格

    @ExcelProperty("医院") //在excel表中的列名
    @ColumnWidth(10) //在excel表中的列宽
    private String hospitalName;//医院名字


    private String hospitalAddress;//医院名字

    private String doctorName;//医生的名字

    @ExcelProperty("科室") //在excel表中的列名
    @ColumnWidth(10) //在excel表中的列宽
    private String departmentName;//科室的名字

    private String title;//医生职称

    @ExcelProperty("就诊人") //在excel表中的列名
    @ColumnWidth(10) //在excel表中的列宽
    private String realName;//患者姓名

    private String userTel;//患者电话

    private Integer userGender;//患者性别

    private String idCard;
}
