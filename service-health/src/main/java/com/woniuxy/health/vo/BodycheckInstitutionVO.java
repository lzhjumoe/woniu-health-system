package com.woniuxy.health.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BodycheckInstitutionVO {

    private String institutionsName;
    private Integer institutionsType;
    private String institutionsAddress;
    private Integer institutionsState;
    private Long provinceId;
    private Long cityId;
    private Long districtId;
    private String institutionImgUrl;//上传图片在oss中的地址

}
