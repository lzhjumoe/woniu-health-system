package com.woniuxy.health.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

//这个VO对象是用于客户端的问诊记录接口
@Data
@AllArgsConstructor
@NoArgsConstructor
public class InquiryRecordVO {
    private Integer orderType;
    private Integer orderState;
    private String name;//医生名
    private String doctorImgUrl;
    private String title;//医生职称
    private String diseaseDescription;//病情描述
}
