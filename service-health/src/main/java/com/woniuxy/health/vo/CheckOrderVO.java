package com.woniuxy.health.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @Date 2023/9/12 16:30
 * @Author LZH
 * Description:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CheckOrderVO {

    private Long id;

    @ApiModelProperty("体检订单编码")
    private String orderCode;

    @ApiModelProperty("体检套餐名字")
    private String mealName;

    @ApiModelProperty("机构名称")
    private String institutionsName;

    @ApiModelProperty("真实姓名")
    private String realname;

    @ApiModelProperty("电话号码")
    private String userTel;

    @ApiModelProperty("身份证号")
    private String idCard;

    @ApiModelProperty("体检日期")
    private Date checkTime;

    @ApiModelProperty("上传日期")
    private Date updateTime;

    @ApiModelProperty("报告文件路径")
    private String reportUrl;

    @ApiModelProperty("是否上传")
    private Integer state;


}
