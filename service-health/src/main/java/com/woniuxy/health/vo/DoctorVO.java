package com.woniuxy.health.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Date 2023/9/5 11:47
 * @Author LZH
 * Description:
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DoctorVO {

    @ApiModelProperty("医生id")
    private Long id;

    @ApiModelProperty("医生名称")
    private String name;

    @ApiModelProperty("医生头像")
    private String doctorImgUrl;

    @ApiModelProperty("医生职称")
    private String title;

    @ApiModelProperty("医生电话")
    private String phoneNumber;

    @ApiModelProperty("擅长领域")
    private String field;

    private Integer state;

    @ApiModelProperty("医生详情")
    private String detail;

    private String hospitalName;

    private String firstDept;

    private String departmentName;

    //接诊挂号费用
    private Double receptionFee;

    //问诊费用 图文问诊
    private Double inquiryPrice0;

    //问诊费用 电话问诊
    private Double inquiryPrice1;


}
