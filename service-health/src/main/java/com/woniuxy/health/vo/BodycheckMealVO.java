package com.woniuxy.health.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @Date 2023/9/11 14:22
 * @Author LZH
 * Description:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BodycheckMealVO {

    @ApiModelProperty("主键id")
    private Long id;

    @ApiModelProperty("体检套餐名字")
    private String mealName;

    @ApiModelProperty("体检套餐图片")
    private String mealImgUrl;

    @ApiModelProperty("套餐价格")
    private Object mealPrice;

    @ApiModelProperty("套餐详情")
    private String mealDetail;

    @ApiModelProperty("套餐项目")
    private String mealItem;

    @ApiModelProperty("机构名称")
    private String institutionsName;


    @ApiModelProperty("机构外键Id")
    private Long checkInstId;

    @ApiModelProperty("机构地址")
    private String institutionsAddress;

    @ApiModelProperty("套餐上架下架")
    private Integer mealState;

    @ApiModelProperty("添加时间的集合")
    private List<Integer> timeIds;
}
