package com.woniuxy.health.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ChangeInquiryPriceVO {
    private Long id;
    private Double inquiryPrice;
}
