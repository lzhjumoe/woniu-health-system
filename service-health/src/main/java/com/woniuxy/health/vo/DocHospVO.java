package com.woniuxy.health.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Date 2023/9/9 13:59
 * @Author LZH
 * Description: 医生添加页面的医院选择，需要返回医院id和医院名称
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DocHospVO {
    private Long id;
    private String name;
}
