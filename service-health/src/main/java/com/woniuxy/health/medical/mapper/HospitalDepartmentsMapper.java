package com.woniuxy.health.medical.mapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniuxy.health.medical.vo.DepartmentIncomeVO;
import com.woniuxy.health.vo.HospitalDepartmentVO;
import org.apache.ibatis.annotations.Mapper;
import com.woniuxy.health.model.medical.HospitalDepartments;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Mapper
public interface HospitalDepartmentsMapper extends BaseMapper<HospitalDepartments> {

    Page<HospitalDepartmentVO> showDepartment(@Param("page") Page page, @Param("id") Integer id);

    List<DepartmentIncomeVO> getDepartmentIncomeForMonth(@Param("firstDayOfMonth") Date firstDayOfMonth,
                                                         @Param("lastDayOfMonth") Date lastDayOfMonth);

    List<DepartmentIncomeVO> getDepartmentIncomeForYear(@Param("firstDayOfYear")Date firstDayOfYear,
                                                        @Param("lastDayOfYear")Date lastDayOfYear);
}
