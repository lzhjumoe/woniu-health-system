package com.woniuxy.health.medical.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniuxy.health.aspect.OperationLogAnnotation;
import com.woniuxy.health.medical.service.IBodycheckInstitutionsService;
import com.woniuxy.health.model.medical.BodycheckInstitutions;
import com.woniuxy.health.vo.BodycheckInstitutionVO;
import com.woniuxy.utils.result.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@RestController
@RequestMapping("/bodycheck-institutions")
public class BodycheckInstitutionsController {

    private final IBodycheckInstitutionsService bodycheckInstitutionsServiceImpl;
    private final RedisTemplate redisTemplate;

    @Autowired
    public BodycheckInstitutionsController(IBodycheckInstitutionsService bodycheckInstitutionsServiceImpl, RedisTemplate redisTemplate) {
        this.bodycheckInstitutionsServiceImpl = bodycheckInstitutionsServiceImpl;
        this.redisTemplate = redisTemplate;
    }


    //上传图片需要做两件事，1、把上传的图片保存到物理硬盘中  2、将数据库中的url图片字段，修改成上传图片的路径
    @OperationLogAnnotation(module = "体检机构", type = "上传图片", description = "上传体检机构图片")
    @PostMapping("/uploadPicture")
    public Result uploadPicture(Long id, @RequestParam("file") MultipartFile file) throws IOException {
        //1、将上传的图片保存在物理硬盘中
        //拿到具体文件 file
        System.out.println("打印查看file = " + file);
        System.out.println("打印查看id = " + id);

        // 获取文件的输入流和原始文件名
        InputStream inputStream = file.getInputStream();
        String originalFilename = file.getOriginalFilename();

        // 指定保存文件的目录
        String uploadDirectory = "D:\\imgs/";

        // 创建保存文件的路径
        String filePath = uploadDirectory + id + ".jpg";

        // 使用IO操作将文件保存到本地
        FileOutputStream outputStream = new FileOutputStream(filePath);
        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = ((InputStream) inputStream).read(buffer)) != -1) {
            outputStream.write(buffer, 0, bytesRead);
        }

        // 关闭输入流和输出流
        inputStream.close();
        outputStream.close();


        //2、修改数据库中的字段
        bodycheckInstitutionsServiceImpl.uploadPicture(id);

        return Result.ok();
    }


    //删除机构和旗下所有分店,前端会传总店的id过来
    @OperationLogAnnotation(module = "体检机构", type = "删除", description = "删除体检机构和分店")
    @GetMapping("/deleteInstitution/{id}")
    public Result deleteInstitution(@PathVariable Long id) {
        bodycheckInstitutionsServiceImpl.deleteInstitution(id);

        return Result.ok();
    }


    @OperationLogAnnotation(module = "体检机构", type = "修改状态", description = "修改上架下架状态")
    //修改机构上下架状态
    @PostMapping("/editInstitutionState")
    public Result editInstitutionState(@RequestBody BodycheckInstitutions bodycheckInstitutions) {
        bodycheckInstitutionsServiceImpl.editInstitutionState(bodycheckInstitutions);

        return Result.ok();
    }


    @OperationLogAnnotation(module = "体检机构", type = "新增", description = "新增体检机构")
    //添加体检机构，有可能添加总店，也有可能添加分店
    @PostMapping("/addInstitution")
    public Result addInstitution(@RequestBody BodycheckInstitutionVO bodycheckInstitutionVO) {
        System.out.println("查看前端传过来的bodycheckInstitutionVO = " + bodycheckInstitutionVO);
        bodycheckInstitutionsServiceImpl.addInstitution(bodycheckInstitutionVO);


        return Result.ok();
    }


    //根据主键id查找对应的体检机构的分店数据
    @GetMapping("/getBranchInstitutionById")
    public Result getBranchInstitutionById(Integer id) {
        List<BodycheckInstitutions> list = bodycheckInstitutionsServiceImpl.list(Wrappers.lambdaQuery(BodycheckInstitutions.class)
                .eq(Objects.nonNull(id), BodycheckInstitutions::getPid, id));

        return Result.ok(list);

    }


    //显示全部的体检机构，如果有条件就显示符合条件的
    @GetMapping("/getALLInstitutions")
    public Result getALLInstitutions(String institutionName,
                                     Integer institutionType,
                                     Integer pageNum,
                                     Integer pageSize) {
        Page page = Page.of(pageNum, pageSize);

        Page<BodycheckInstitutions> pageList =
                bodycheckInstitutionsServiceImpl.getALLInstitutions(institutionName, institutionType, page);
        return Result.ok(pageList);
    }

    /**
     * 补充方法，用于体检套餐中的查询机构调用
     */
    //显示全部的体检机构，如果有条件就显示符合条件的
    @GetMapping("/getALLInst")
    public Result getALLInstitutions() {
        List<BodycheckInstitutions> list = bodycheckInstitutionsServiceImpl.list();
        return Result.ok(list);
    }

}
