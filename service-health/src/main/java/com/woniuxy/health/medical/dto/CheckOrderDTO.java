package com.woniuxy.health.medical.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.List;

/**
 * @Date 2023/9/12 16:46
 * @Author LZH
 * Description:
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CheckOrderDTO {

    @ApiModelProperty("条件名")
    private String name;

    @ApiModelProperty("查询字段")
    private String keyword;

    @ApiModelProperty("时间范围")
    private List<String> dateRange;

}
