package com.woniuxy.health.medical.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniuxy.health.medical.dto.BodycheckMealDTO;
import com.woniuxy.health.medical.dto.DoctorQueryDTO;
import com.woniuxy.health.model.medical.BodycheckMeal;
import com.woniuxy.health.vo.BodycheckMealVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-11
 */
@Mapper
public interface BodycheckMealMapper extends BaseMapper<BodycheckMeal> {

    Page<BodycheckMealVO> selectMealPage(@Param("bodycheckMealVOPage") Page<BodycheckMealVO> bodycheckMealVOPage,
                                         @Param("bodycheckMealDTO") BodycheckMealDTO bodycheckMealDTO);

}
