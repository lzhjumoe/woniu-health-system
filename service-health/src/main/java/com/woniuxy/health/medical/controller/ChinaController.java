package com.woniuxy.health.medical.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.woniuxy.health.medical.service.IChinaService;
import com.woniuxy.health.model.medical.China;
import com.woniuxy.utils.result.Result;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author usercheng
 * @since 2023-09-09
 */
@RestController
@RequestMapping("/china")
public class ChinaController {

    private final IChinaService chinaServiceImpl;
    public ChinaController(IChinaService chinaServiceImpl){
        this.chinaServiceImpl = chinaServiceImpl;
    }


    //获取对应的区
    @GetMapping("/getDistricts")
    public Result getDistricts(Long id){
        List<China> list = chinaServiceImpl.list(Wrappers.lambdaQuery(China.class)
                .eq(China::getPid, id));

        return Result.ok(list);
    }


    //获取对应的市
    @GetMapping("/getCities")
    public Result getCities(Long id){
        List<China> list = chinaServiceImpl.list(Wrappers.lambdaQuery(China.class)
                .eq(China::getPid, id));

        return Result.ok(list);
    }


    @GetMapping("/getProvinces")
    public Result getProvinces(){
        List<China> list = chinaServiceImpl.list(Wrappers.lambdaQuery(China.class)
                .eq(China::getPid, 0));

        return Result.ok(list);
    }

}
