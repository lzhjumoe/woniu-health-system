package com.woniuxy.health.medical.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.woniuxy.health.medical.vo.SetMealCountVO;
import com.woniuxy.health.medical.vo.SetMealVO;
import com.woniuxy.health.medical.vo.SetMealVolumeVO;
import com.woniuxy.health.model.medical.SetMeal;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Mapper
public interface SetMealMapper extends BaseMapper<SetMeal> {

    List<SetMealCountVO> getSetMealCount();

    List<SetMealVolumeVO> getSetMealIncome();

    List<SetMealCountVO> getSetMealCountForMonth(@Param("firstDayOfMonth") Date firstDayOfMonth, @Param("lastDayOfMonth") Date lastDayOfMonth);

    List<SetMealCountVO> getSetMealIncomeForMonth(@Param("firstDayOfMonth") Date firstDayOfMonth, @Param("lastDayOfMonth") Date lastDayOfMonth);

    List<SetMealVO> getTotalIncomeAndSales();

    List<SetMealVO> getIncomeAndSalesForMonth(@Param("firstDayOfMonth") Date firstDayOfMonth, @Param("lastDayOfMonth") Date lastDayOfMonth);

}
