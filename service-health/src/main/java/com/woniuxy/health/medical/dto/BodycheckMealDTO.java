package com.woniuxy.health.medical.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Date 2023/9/11 14:35
 * @Author LZH
 * Description:
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class BodycheckMealDTO {

    @ApiModelProperty("套餐名称")
    private String mealName;

    @ApiModelProperty("体检机构")
    private String institutionsName;

    @ApiModelProperty("套餐上架下架")
    private Integer mealState;

}
