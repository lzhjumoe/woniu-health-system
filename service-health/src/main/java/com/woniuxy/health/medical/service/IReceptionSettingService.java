package com.woniuxy.health.medical.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.woniuxy.health.model.medical.ReceptionSetting;
import com.woniuxy.health.vo.ReceptionVO;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
public interface IReceptionSettingService extends IService<ReceptionSetting> {

    void addReception(ReceptionVO receptionVO);
}
