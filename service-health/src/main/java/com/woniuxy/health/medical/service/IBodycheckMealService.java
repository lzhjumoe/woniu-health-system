package com.woniuxy.health.medical.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.woniuxy.health.medical.dto.BodycheckMealDTO;
import com.woniuxy.health.medical.dto.DoctorQueryDTO;
import com.woniuxy.health.model.medical.BodycheckMeal;
import com.woniuxy.health.vo.BodycheckMealVO;
import com.woniuxy.health.vo.PageVO;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-11
 */
public interface IBodycheckMealService extends IService<BodycheckMeal> {


    PageVO<BodycheckMealVO> selectMealPage(Page<BodycheckMealVO> bodycheckMealVOPage, BodycheckMealDTO bodycheckMealDTO);

    void addMeal(BodycheckMealVO bodycheckMealVO);

    BodycheckMealVO getOneMeal(long id);
}
