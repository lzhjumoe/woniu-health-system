package com.woniuxy.health.medical.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.woniuxy.health.medical.vo.DoctorIncomeVO;
import com.woniuxy.health.model.medical.Doctor;
import com.woniuxy.health.medical.dto.DoctorQueryDTO;
import com.woniuxy.health.vo.DoctorVO;
import com.woniuxy.health.vo.PageVO;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
public interface IDoctorService extends IService<Doctor> {


    void saveDoc(DoctorVO doctorVO);

    PageVO<DoctorVO> selectDocPage(Page<DoctorVO> doctorVOPage, DoctorQueryDTO doctorQueryDTO);

    DoctorVO getOneDoc(Long docId);

    List<DoctorIncomeVO> getDoctorIncomeTop20();
}
