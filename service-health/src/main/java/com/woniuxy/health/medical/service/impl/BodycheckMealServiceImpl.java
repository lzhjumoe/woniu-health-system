package com.woniuxy.health.medical.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.medical.dto.BodycheckMealDTO;
import com.woniuxy.health.medical.mapper.BodycheckInstitutionsMapper;
import com.woniuxy.health.medical.mapper.BodycheckMealMapper;
import com.woniuxy.health.medical.mapper.BodycheckTimeMapper;
import com.woniuxy.health.medical.mapper.SetMealMapper;
import com.woniuxy.health.medical.service.IBodycheckMealService;
import com.woniuxy.health.model.medical.BodycheckInstitutions;
import com.woniuxy.health.model.medical.BodycheckMeal;
import com.woniuxy.health.model.medical.BodycheckTime;
import com.woniuxy.health.model.medical.SetMeal;
import com.woniuxy.health.vo.BodycheckMealVO;
import com.woniuxy.health.vo.PageVO;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Time;
import java.time.LocalTime;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-11
 */
@Service
public class BodycheckMealServiceImpl extends ServiceImpl<BodycheckMealMapper, BodycheckMeal> implements IBodycheckMealService {

    private final BodycheckMealMapper bodycheckMealMapper;
    private final BodycheckTimeMapper bodycheckTimeMapper;

    private final BodycheckInstitutionsMapper bodycheckInstitutionsMapper;

    private final SetMealMapper setMealMapper;

    @Autowired
    public BodycheckMealServiceImpl(BodycheckMealMapper bodycheckMealMapper, BodycheckTimeMapper bodycheckTimeMapper, BodycheckInstitutionsMapper bodycheckInstitutionsMapper, SetMealMapper setMealMapper) {
        this.bodycheckMealMapper = bodycheckMealMapper;
        this.bodycheckTimeMapper = bodycheckTimeMapper;
        this.bodycheckInstitutionsMapper = bodycheckInstitutionsMapper;
        this.setMealMapper = setMealMapper;
    }

    @Override
    public PageVO<BodycheckMealVO> selectMealPage(Page<BodycheckMealVO> bodycheckMealVOPage, BodycheckMealDTO bodycheckMealDTO) {
        Page<BodycheckMealVO> page = bodycheckMealMapper.selectMealPage(bodycheckMealVOPage, bodycheckMealDTO);
        return new PageVO<>(page.getTotal(), page.getRecords());
    }

    @Override
    public void addMeal(BodycheckMealVO bodycheckMealVO) {
        BodycheckMeal bodycheckMeal = new BodycheckMeal();
        BeanUtil.copyProperties(bodycheckMealVO, bodycheckMeal);
        BodycheckInstitutions bodycheckInstitutions = bodycheckInstitutionsMapper.selectOne(
                Wrappers.lambdaQuery(BodycheckInstitutions.class)
                        .eq(BodycheckInstitutions::getInstitutionsName, bodycheckMealVO.getInstitutionsName()));
        bodycheckMeal.setCheckInstId(bodycheckInstitutions.getId());
        bodycheckMeal.setMealState(0);
        SetMeal setMeal = new SetMeal();
        setMeal.setSetMealName(bodycheckMeal.getMealName());
        setMeal.setSetMealPrice(Long.valueOf((String) bodycheckMeal.getMealPrice()));
        setMeal.setHospitalId(bodycheckMeal.getCheckInstId());
        setMealMapper.insert(setMeal);
        bodycheckMealMapper.insert(bodycheckMeal);
        BodycheckTime bodycheckTime = null;
        List<Integer> timeIds = bodycheckMealVO.getTimeIds();
        for (Integer timeId : timeIds) {
            if (timeId.longValue() == 1L) {
                bodycheckTime = new BodycheckTime();
                bodycheckTime.setMealId(bodycheckMeal.getId());
                bodycheckTime.setBeginTime(Time.valueOf(LocalTime.of(8, 30)));
                bodycheckTime.setEndTime(Time.valueOf(LocalTime.of(10, 00)));
                bodycheckTime.setState(1);
                bodycheckTimeMapper.insert(bodycheckTime);
            }
            if (timeId.longValue() == 2L) {
                bodycheckTime = new BodycheckTime();
                bodycheckTime.setMealId(bodycheckMeal.getId());
                bodycheckTime.setBeginTime(Time.valueOf(LocalTime.of(10, 00)));
                bodycheckTime.setEndTime(Time.valueOf(LocalTime.of(11, 30)));
                bodycheckTime.setState(2);
                bodycheckTimeMapper.insert(bodycheckTime);
            }
            if (timeId.longValue() == 3L) {
                bodycheckTime = new BodycheckTime();
                bodycheckTime.setMealId(bodycheckMeal.getId());
                bodycheckTime.setBeginTime(Time.valueOf(LocalTime.of(13, 00)));
                bodycheckTime.setEndTime(Time.valueOf(LocalTime.of(14, 30)));
                bodycheckTime.setState(3);
                bodycheckTimeMapper.insert(bodycheckTime);
            }
            if (timeId.longValue() == 4L) {
                bodycheckTime = new BodycheckTime();
                bodycheckTime.setMealId(bodycheckMeal.getId());
                bodycheckTime.setBeginTime(Time.valueOf(LocalTime.of(14, 30)));
                bodycheckTime.setEndTime(Time.valueOf(LocalTime.of(16, 00)));
                bodycheckTime.setState(4);
                bodycheckTimeMapper.insert(bodycheckTime);
            }
            if (timeId.longValue() == 5L) {
                bodycheckTime = new BodycheckTime();
                bodycheckTime.setMealId(bodycheckMeal.getId());
                bodycheckTime.setBeginTime(Time.valueOf(LocalTime.of(16, 00)));
                bodycheckTime.setEndTime(Time.valueOf(LocalTime.of(17, 30)));
                bodycheckTime.setState(5);
                bodycheckTimeMapper.insert(bodycheckTime);
            }
        }
    }


    @Override
    public BodycheckMealVO getOneMeal(long id) {
        BodycheckMeal bodycheckMeal = bodycheckMealMapper.selectById(id);
        BodycheckMealVO bodycheckMealVO = new BodycheckMealVO();
        BeanUtil.copyProperties(bodycheckMeal, bodycheckMealVO);

        //剩下有机构名称，机构地址，添加时间的集合
        BodycheckInstitutions bodycheckInstitutions = bodycheckInstitutionsMapper
                .selectById(bodycheckMeal.getCheckInstId());
        bodycheckMealVO.setInstitutionsName(bodycheckInstitutions.getInstitutionsName());
        bodycheckMealVO.setInstitutionsAddress(bodycheckInstitutions.getInstitutionsAddress());
        List<BodycheckTime> bodycheckTimes = bodycheckTimeMapper.selectList(
                Wrappers.lambdaQuery(BodycheckTime.class)
                        .eq(BodycheckTime::getMealId, id));
        List<Integer> collect = bodycheckTimes.stream().map(e -> e.getState()).collect(Collectors.toList());
        bodycheckMealVO.setTimeIds(collect);
        System.out.println("bodycheckMealVO = " + bodycheckMealVO);
        return bodycheckMealVO;
    }


}