package com.woniuxy.health.medical.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.medical.mapper.InquiryAppointmentMapper;
import com.woniuxy.health.medical.service.IInquiryAppointmentService;
import com.woniuxy.health.model.medical.InquiryAppointment;
import org.springframework.stereotype.Service;
import java.util.Date;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author usercheng
 * @since 2023-09-07
 */
@Service
public class InquiryAppointmentServiceImpl extends ServiceImpl<InquiryAppointmentMapper, InquiryAppointment> implements IInquiryAppointmentService {

    private final InquiryAppointmentMapper inquiryAppointmentMapper;
    public InquiryAppointmentServiceImpl(InquiryAppointmentMapper inquiryAppointmentMapper){
        this.inquiryAppointmentMapper = inquiryAppointmentMapper;
    }

    @Override
    public void addReceptionTime(Integer orderId) {
        UpdateWrapper<InquiryAppointment> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("order_id",orderId);

        InquiryAppointment inquiryAppointment = new InquiryAppointment();
        inquiryAppointment.setReceptionTime(new Date());

        inquiryAppointmentMapper.update(inquiryAppointment,updateWrapper);
    }
}
