package com.woniuxy.health.medical.controller;

import com.woniuxy.health.medical.service.IInquiryAppointmentService;
import com.woniuxy.health.medical.service.IOrderDetailService;
import com.woniuxy.health.medical.vo.DepartmentIncomeVO;
import com.woniuxy.health.vo.InquiryRecordVO;
import com.woniuxy.utils.result.Result;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author usercheng
 * @since 2023-09-07
 */
@RestController
@RequestMapping("/inquiry-appointment")
public class InquiryAppointmentController {

    private final IInquiryAppointmentService inquiryAppointmentServiceImpl;
    private final IOrderDetailService orderDetailService;
    private final RedisTemplate redisTemplate;

    public InquiryAppointmentController(IInquiryAppointmentService inquiryAppointmentServiceImpl,
                                        IOrderDetailService orderDetailService, RedisTemplate redisTemplate){
        this.inquiryAppointmentServiceImpl = inquiryAppointmentServiceImpl;
        this.orderDetailService = orderDetailService;
        this.redisTemplate = redisTemplate;
    }




    //用户端接口，用户点击进入问诊记录页面时，显示问诊订单的数据，前端需要传个问诊类型过来
    //需要显示医生的信息，病情描述，订单支付状态
    @GetMapping("/getInquiryRecords")
    public Result getInquiryRecords(Integer orderType,Long orderId){
        List<InquiryRecordVO> list = orderDetailService.getInquiryRecords(orderType);

        return Result.ok(list);
    }


    //各科室当年的问诊收入
    @GetMapping("/getDepInqIncomeForYear")
    public Result getDepInqIncomeForYear(){
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_YEAR, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        Date firstDayOfYear = calendar.getTime();

        calendar.set(Calendar.DAY_OF_YEAR, calendar.getActualMaximum(Calendar.DAY_OF_YEAR));
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        Date lastDayOfYear = calendar.getTime();

        System.out.println("The first day of the year is: " + firstDayOfYear);
        System.out.println("The last day of the year is: " + lastDayOfYear);

        List<DepartmentIncomeVO> list =
                orderDetailService.getDepInqIncomeForYear(firstDayOfYear,lastDayOfYear);

        return Result.ok(list);
    }



    //各科室当月的问诊收入
    @GetMapping("/getDepInqIncomeForMonth")
    public Result getDepInqIncomeForMonth(){
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        Date firstDayOfMonth = calendar.getTime();

        calendar.add(Calendar.MONTH, 1);
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        Date lastDayOfMonth = calendar.getTime();

        //获取当月的第一天和最后一天
        System.out.println("The first day of the month is: " + firstDayOfMonth);
        System.out.println("The last day of the month is: " + lastDayOfMonth);

        List<DepartmentIncomeVO> list =
                orderDetailService.getDepInqIncomeForMonth(firstDayOfMonth,lastDayOfMonth);

        return Result.ok(list);
    }


}
