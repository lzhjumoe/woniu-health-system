package com.woniuxy.health.medical.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Date 2023/9/6 12:20
 * @Author LZH
 * Description:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DoctorQueryDTO {

    @ApiModelProperty("医生名称")
    private String name;

    private String deptId;

    private String hospName;

}
