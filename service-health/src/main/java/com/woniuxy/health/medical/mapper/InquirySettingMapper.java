package com.woniuxy.health.medical.mapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniuxy.health.medical.dto.InquirySettingDTO;
import org.apache.ibatis.annotations.Mapper;
import com.woniuxy.health.model.medical.InquirySetting;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Mapper
public interface InquirySettingMapper extends BaseMapper<InquirySetting> {

    Page<InquirySettingDTO> selectInquirySetting(@Param("doctorId") Long doctorId,
                                                 Page page);
}
