package com.woniuxy.health.medical.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InquirySettingDTO {
    private Long id;//问诊设置表的主键id
    private Integer inquiryType;
    private Double  inquiryPrice;
    private String doctorName;
    private Long doctorId;

}
