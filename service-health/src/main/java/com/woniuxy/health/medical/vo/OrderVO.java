package com.woniuxy.health.medical.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderVO {

    private Integer orderId;

    private Long setMealId;

    private String institutionsName;

    private String realname;

    private String userTel;

    private Timestamp createTime;

    private Timestamp uploadingTime;

}
