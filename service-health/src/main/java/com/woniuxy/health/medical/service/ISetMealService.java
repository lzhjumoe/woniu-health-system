package com.woniuxy.health.medical.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.woniuxy.health.medical.vo.SetMealCountVO;
import com.woniuxy.health.medical.vo.SetMealVO;
import com.woniuxy.health.medical.vo.SetMealVolumeVO;
import com.woniuxy.health.model.medical.SetMeal;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
public interface ISetMealService extends IService<SetMeal> {



    List<SetMealCountVO> getSetMealCountForMonth(Date firstDayOfMonth, Date lastDayOfMonth);

    List<SetMealCountVO> getSetMealIncomeForMonth(Date firstDayOfMonth, Date lastDayOfMonth);

    List<SetMealVO> getTotalIncomeAndSales();

    List<SetMealVO> getIncomeAndSalesForMonth(Date firstDayOfMonth, Date lastDayOfMonth);
    List<SetMealCountVO> getSetMealCount();

    List<SetMealVolumeVO> getSetMealIncome();



}
