package com.woniuxy.health.medical.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.medical.mapper.*;
import com.woniuxy.health.medical.service.IDoctorService;
import com.woniuxy.health.medical.vo.DoctorIncomeVO;
import com.woniuxy.health.medical.vo.HospitalIncomeVO;
import com.woniuxy.health.model.medical.*;
import com.woniuxy.health.medical.dto.DoctorQueryDTO;
import com.woniuxy.health.oss.OSSUtil;
import com.woniuxy.health.vo.DoctorVO;
import com.woniuxy.health.vo.PageVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Service
public class DoctorServiceImpl extends ServiceImpl<DoctorMapper, Doctor> implements IDoctorService {

    private final DoctorMapper doctorMapper;
    private final HospitalMapper hospitalMapper;

    private final InquirySettingMapper inquirySettingMapper;

    private final HospitalDepartmentsMapper hospitalDepartmentsMapper;
    private final DoctorTitleMapper doctorTitleMapper;


    @Autowired
    public DoctorServiceImpl(DoctorMapper doctorMapper, HospitalMapper hospitalMapper, InquirySettingMapper inquirySettingMapper, HospitalDepartmentsMapper hospitalDepartmentsMapper, DoctorTitleMapper doctorTitleMapper) {
        this.doctorMapper = doctorMapper;
        this.hospitalMapper = hospitalMapper;
        this.inquirySettingMapper = inquirySettingMapper;
        this.hospitalDepartmentsMapper = hospitalDepartmentsMapper;
        this.doctorTitleMapper = doctorTitleMapper;
    }

    @Resource
    private OSSUtil ossUtil;




    @Override
    public List<DoctorIncomeVO> getDoctorIncomeTop20() {
        List<DoctorIncomeVO> list = doctorMapper.getDoctorIncomeTop20();
        System.out.println("查看list = " + list);
        return list;

    }




    @Transactional
    @Override
    public void saveDoc(DoctorVO doctorVO) {
        Hospital hospital = hospitalMapper.selectOne(
                Wrappers.lambdaQuery(Hospital.class)
                        .eq(Hospital::getHospitalName, doctorVO.getHospitalName()));
        HospitalDepartments hospDept = hospitalDepartmentsMapper.selectOne(
                Wrappers.lambdaQuery(HospitalDepartments.class)
                        .eq(HospitalDepartments::getId, doctorVO.getDepartmentName()));
        DoctorTitle doctorTitle = doctorTitleMapper.selectOne(
                Wrappers.lambdaQuery(DoctorTitle.class)
                        .eq(DoctorTitle::getName, doctorVO.getTitle()));
        //更新医生表
        Doctor doctor = BeanUtil.copyProperties(doctorVO, Doctor.class);
        doctor.setHospitalId(hospital.getId());
        doctor.setDepartmentId(hospDept.getId());
        doctor.setTitleId(doctorTitle.getId());
        doctor.setState(0);
        doctorMapper.insert(doctor);
        //更新问诊表
        //图文问诊
        if (Objects.nonNull(doctorVO.getInquiryPrice0())) {
            InquirySetting inquirySetting = new InquirySetting();
            inquirySetting.setDoctorId(doctor.getId());
            inquirySetting.setInquiryType((byte) 0);
            inquirySetting.setInquiryPrice(doctorVO.getInquiryPrice0());
            inquirySettingMapper.insert(inquirySetting);
        }
        //电话问诊
        if (Objects.nonNull(doctorVO.getInquiryPrice1())) {
            InquirySetting inquirySetting1 = new InquirySetting();
            inquirySetting1.setDoctorId(doctor.getId());
            inquirySetting1.setInquiryType((byte) 1);
            inquirySetting1.setInquiryPrice(doctorVO.getInquiryPrice1());
            inquirySettingMapper.insert(inquirySetting1);
        }
    }

    @Override
    public PageVO<DoctorVO> selectDocPage(Page<DoctorVO> doctorVOPage, DoctorQueryDTO doctorQueryDTO) {
        Page<DoctorVO> pageModel = doctorMapper.selectDocPage(doctorVOPage, doctorQueryDTO);
        return new PageVO<>(pageModel.getTotal(), pageModel.getRecords());
    }

    @Override
    public DoctorVO getOneDoc(Long docId) {
        Doctor doctor = doctorMapper.selectById(docId);
        DoctorVO doctorVO = new DoctorVO();
        BeanUtil.copyProperties(doctor, doctorVO);
        //这时需要把doc中的医院姓名，科室名，医生职称，还有两种问诊价格都要得到，然后还要找到一级科室的名字
        //医院
        Hospital hospital = hospitalMapper.selectOne(
                Wrappers.lambdaQuery(Hospital.class)
                        .eq(Hospital::getId, doctor.getHospitalId()));
        doctorVO.setHospitalName(hospital.getHospitalName());

        //部门
        HospitalDepartments hospitalDepartment = hospitalDepartmentsMapper.selectOne(
                Wrappers.lambdaQuery(HospitalDepartments.class)
                        .eq(HospitalDepartments::getId, doctor.getDepartmentId()));
        doctorVO.setDepartmentName(hospitalDepartment.getDepartmentName());
        //一级部门
        HospitalDepartments firstDept = hospitalDepartmentsMapper.selectOne(
                Wrappers.lambdaQuery(HospitalDepartments.class)
                        .eq(HospitalDepartments::getId, hospitalDepartment.getPid()));
        doctorVO.setFirstDept(firstDept.getDepartmentName());
        //职称
        DoctorTitle doctorTitle = doctorTitleMapper.selectOne(
                Wrappers.lambdaQuery(DoctorTitle.class)
                        .eq(DoctorTitle::getId, doctor.getTitleId()));
        doctorVO.setTitle(doctorTitle.getName());

        InquirySetting inquirySetting0 = inquirySettingMapper.selectOne(
                Wrappers.lambdaQuery(InquirySetting.class)
                        .eq(InquirySetting::getDoctorId, doctor.getId())
                        .eq(InquirySetting::getInquiryType, 0));
        InquirySetting inquirySetting1 = inquirySettingMapper.selectOne(
                Wrappers.lambdaQuery(InquirySetting.class)
                        .eq(InquirySetting::getDoctorId, doctor.getId())
                        .eq(InquirySetting::getInquiryType, 1));
        if (Objects.nonNull(inquirySetting0)) {
            doctorVO.setInquiryPrice0(inquirySetting0.getInquiryPrice());
        }
        if (Objects.nonNull(inquirySetting1)) {
            doctorVO.setInquiryPrice1(inquirySetting1.getInquiryPrice());
        }
        return doctorVO;
    }




}
