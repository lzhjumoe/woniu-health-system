package com.woniuxy.health.medical.controller;

import com.woniuxy.health.medical.service.IBodycheckTimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-11
 */
@RestController
@RequestMapping("/bodycheck-time")
public class BodycheckTimeController {

    private final IBodycheckTimeService bodycheckTimeServiceImpl;

    @Autowired
    public BodycheckTimeController(IBodycheckTimeService bodycheckTimeServiceImpl){
        this.bodycheckTimeServiceImpl = bodycheckTimeServiceImpl;
    }




}
