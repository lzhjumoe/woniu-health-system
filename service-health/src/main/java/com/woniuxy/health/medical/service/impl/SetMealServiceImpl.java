package com.woniuxy.health.medical.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.medical.mapper.SetMealMapper;
import com.woniuxy.health.medical.service.ISetMealService;
import com.woniuxy.health.medical.vo.SetMealCountVO;
import com.woniuxy.health.medical.vo.SetMealVO;
import com.woniuxy.health.medical.vo.SetMealVolumeVO;
import com.woniuxy.health.model.medical.SetMeal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Service
public class SetMealServiceImpl extends ServiceImpl<SetMealMapper, SetMeal> implements ISetMealService {

    private final SetMealMapper setMealMapper;

    @Autowired
    public SetMealServiceImpl(SetMealMapper setMealMapper){
        this.setMealMapper = setMealMapper;
    }





    @Override
    public List<SetMealVO> getIncomeAndSalesForMonth(Date firstDayOfMonth, Date lastDayOfMonth) {
        List<SetMealVO> list = setMealMapper.getIncomeAndSalesForMonth(firstDayOfMonth,lastDayOfMonth);
        return list;
    }


    @Override
    public List<SetMealVO> getTotalIncomeAndSales() {
        List<SetMealVO> list = setMealMapper.getTotalIncomeAndSales();
        return list;
    }



    @Override
    public List<SetMealCountVO> getSetMealIncomeForMonth(Date firstDayOfMonth, Date lastDayOfMonth) {
        List<SetMealCountVO> list = setMealMapper.getSetMealIncomeForMonth(firstDayOfMonth,lastDayOfMonth);
        return list;
    }



    @Override
    public List<SetMealCountVO> getSetMealCountForMonth(Date firstDayOfMonth, Date lastDayOfMonth) {
        List<SetMealCountVO> list = setMealMapper.getSetMealCountForMonth(firstDayOfMonth,lastDayOfMonth);
        return list;
    }




    @Override
    public List<SetMealVolumeVO> getSetMealIncome() {
        List<SetMealVolumeVO> list = setMealMapper.getSetMealIncome();

        System.out.println("查看list = " + list);
        return list;
    }



    @Override
    public List<SetMealCountVO> getSetMealCount() {
        List<SetMealCountVO> list = setMealMapper.getSetMealCount();

        System.out.println("查看list = " + list);
        return list;
    }



}
