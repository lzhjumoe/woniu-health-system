package com.woniuxy.health.medical.mapper;
import com.woniuxy.health.model.medical.BodycheckTime;
import org.apache.ibatis.annotations.Mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-11
 */
@Mapper
public interface BodycheckTimeMapper extends BaseMapper<BodycheckTime> {

}
