package com.woniuxy.health.medical.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniuxy.health.aspect.OperationLogAnnotation;
import com.woniuxy.health.medical.service.IInquirySettingService;
import com.woniuxy.health.medical.vo.SetMealCountVO;
import com.woniuxy.health.vo.ChangeInquiryPriceVO;
import com.woniuxy.health.vo.InquirySettingVo;
import com.woniuxy.utils.result.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@RestController
@RequestMapping("/inquiry-setting")
public class InquirySettingController {

    private final IInquirySettingService inquirySettingServiceImpl;

    @Autowired
    public InquirySettingController(IInquirySettingService inquirySettingServiceImpl){
        this.inquirySettingServiceImpl = inquirySettingServiceImpl;
    }



    //获取各科室的电话问诊数量
    @GetMapping("/getPhoneInquiryCount")
    public Result getPhoneInquiryCount(){
        //我这边返回的数据是科室+问诊数量,也就是一个字符串和一个整形，我就直接借用SetMealCountVO来接数据了，
        //懒得再写一个VO了
        List<SetMealCountVO> list = inquirySettingServiceImpl.getPhoneInquiryCount();

        return Result.ok(list);

    }

    //获取各科室的图文问诊数量
    @GetMapping("/getPictureInquiryCount")
    public Result getPictureInquiryCount(){
        //我这边返回的数据是科室+问诊数量,也就是一个字符串和一个整形，我就直接借用SetMealCountVO来接数据了，
        //懒得再写一个VO了
        List<SetMealCountVO> list = inquirySettingServiceImpl.getPictureInquiryCount();

        return Result.ok(list);

    }




    //修改问诊价格,前端会传问诊设置表中的主键id和修改后的价格过来
    @OperationLogAnnotation(module = "问诊设置", type = "修改", description = "修改问诊价格")
    @PostMapping("/changeInquiryPrice")
    public Result changeInquiryPrice(@RequestBody ChangeInquiryPriceVO changeInquiryPriceVO){

        inquirySettingServiceImpl.updateInquiryPrice(changeInquiryPriceVO);

        return Result.ok();
    }


    //显示全部问诊设置
    //如果有参数，就根据医生id查找，没有就查询全部
    @GetMapping("/getInquirySetting")
    public Result getInquirySetting(Long doctorId,Integer pageNum,Integer pageSize){
        Page page = Page.of(pageNum,pageSize);

        Page<InquirySettingVo> voPage = inquirySettingServiceImpl.getInquirySetting(doctorId,page);

        return Result.ok(voPage);
    }

}
