package com.woniuxy.health.medical.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.woniuxy.health.medical.vo.DepartmentIncomeVO;
import com.woniuxy.health.model.medical.HospitalDepartments;
import com.woniuxy.health.vo.HospitalDepartmentVO;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
public interface IHospitalDepartmentsService extends IService<HospitalDepartments> {

    List<HospitalDepartments> findSecondLevel(Integer id);

    List<HospitalDepartments> findFirstLevel();

    Page<HospitalDepartmentVO> showDepartment(Page page,Integer id);

    List<HospitalDepartmentVO> showDepartmentById(Integer id);

    List<DepartmentIncomeVO> getDepartmentIncomeForMonth(Date firstDayOfMonth, Date lastDayOfMonth);

    List<DepartmentIncomeVO> getDepartmentIncomeForYear(Date firstDayOfYear, Date lastDayOfYear);
}
