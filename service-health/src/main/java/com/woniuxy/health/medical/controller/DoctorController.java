package com.woniuxy.health.medical.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.conditions.update.LambdaUpdateChainWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniuxy.health.aspect.OperationLogAnnotation;
import com.woniuxy.health.medical.service.*;
import com.woniuxy.health.medical.dto.DoctorQueryDTO;
import com.woniuxy.health.medical.vo.DoctorIncomeVO;
import com.woniuxy.health.medical.vo.HospitalIncomeVO;
import com.woniuxy.health.model.medical.Doctor;
import com.woniuxy.health.vo.DoctorVO;
import com.woniuxy.health.vo.PageVO;
import com.woniuxy.health.vo.ReceptionVO;
import com.woniuxy.utils.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@RestController
@RequestMapping("/doctor")
@Api(tags = "医生管理")
public class DoctorController {

    private final IDoctorService doctorServiceImpl;
    private final IHospitalService hospitalServiceImpl;
    private final IInquirySettingService inquirySettingServiceImpl;

    private final IReceptionSettingService receptionSettingServiceImpl;
    private final IHospitalDepartmentsService hospitalDepartmentsServiceImpl;


    @Autowired
    public DoctorController(IDoctorService doctorServiceImpl, IHospitalService hospitalServiceImpl, IInquirySettingService inquirySettingServiceImpl, IReceptionSettingService receptionSettingServiceImpl, IHospitalDepartmentsService hospitalDepartmentsServiceImpl) {
        this.doctorServiceImpl = doctorServiceImpl;
        this.hospitalServiceImpl = hospitalServiceImpl;
        this.inquirySettingServiceImpl = inquirySettingServiceImpl;
        this.receptionSettingServiceImpl = receptionSettingServiceImpl;
        this.hospitalDepartmentsServiceImpl = hospitalDepartmentsServiceImpl;
    }


    //医生总收入top20
    @GetMapping("/getDoctorIncomeTop20")
    public Result getDoctorIncomeTop20(){
        List<DoctorIncomeVO> list = doctorServiceImpl.getDoctorIncomeTop20();

        return Result.ok(list);

    }




    /**
     * 条件分页查询
     *
     * @param pageNum
     * @param pageSize
     * @param doctorQueryDTO
     * @return
     */
    @PostMapping("/selectPage")
    @ApiOperation("条件分页查询")
    public Result selectDocPage(Integer pageNum,
                                Integer pageSize,
                                @RequestBody DoctorQueryDTO doctorQueryDTO) {
        Page<DoctorVO> doctorVOPage = Page.of(pageNum, pageSize);
        // doctorVOPage = doctorServiceImpl.selectDocPage(doctorVOPage, doctorQueryDTO);
        PageVO<DoctorVO> doctorVOPage1 = doctorServiceImpl.selectDocPage(doctorVOPage, doctorQueryDTO);
        return Result.ok(doctorVOPage1);
    }

    /**
     * 根据id查找医生
     */
    @GetMapping("/getOneDoc/{id}")
    public Result getOneDoc(@PathVariable("id") Long docId) {
        DoctorVO doctorVO = doctorServiceImpl.getOneDoc(docId);
        return Result.ok(doctorVO);
    }

    /**
     * 添加医生的方法
     *
     * @param doctorVO
     * @return
     */
    @OperationLogAnnotation(module = "医生列表", type = "添加医生", description = "添加一条医生信息")
    @PostMapping("/add")
    @ApiOperation("添加医生")
    public Result addDoctor(@RequestBody DoctorVO doctorVO) {
        doctorServiceImpl.saveDoc(doctorVO);
        return Result.ok();
    }

    /**
     * 根据id删除医生
     *
     * @param id
     * @return
     */
    @OperationLogAnnotation(module = "医生列表", type = "删除医生", description = "删除一条医生记录")
    @ApiOperation("根据id删除医生")
    @DeleteMapping("/delete/{id}")
    public Result deleteDoc(@PathVariable("id") Long id) {
        if (Objects.isNull(doctorServiceImpl.getById(id))) {
            return Result.build(null, 201, "没有该医生");
        }
        doctorServiceImpl.removeById(id);
        inquirySettingServiceImpl.deleteInquiry(id);
        return Result.ok();
    }

    @OperationLogAnnotation(module = "医生列表", type = "删除医生", description = "批量删除医生")
    @ApiOperation("根据id批量删除医生")
    @DeleteMapping("/deleteBatch/{ids}")
    public Result deleteDocList(@PathVariable("ids") List<Long> ids) {
        doctorServiceImpl.removeByIds(ids);
        inquirySettingServiceImpl.deleteInquiryList(ids);
        return Result.ok();
    }

    /**
     * 添加医生问诊信息
     *
     * @param receptionVO
     * @return
     */
    @OperationLogAnnotation(module = "医生列表", type = "问诊信息", description = "添加问诊信息")
    @PostMapping("/reception")
    @ApiOperation("添加医生问诊信息")
    public Result reception(@RequestBody ReceptionVO receptionVO) {
        receptionSettingServiceImpl.addReception(receptionVO);
        return Result.ok();
    }

    /**
     * 改变上架下架的状态
     *
     * @param doctorVO
     * @return
     */
    @OperationLogAnnotation(module = "医生列表", type = "改变状态", description = "改变状态")
    @PutMapping("/changeState")
    @ApiOperation("改变上架下架的状态")
    public Result changeState(@RequestBody DoctorVO doctorVO) {
        doctorServiceImpl.update(
                Wrappers.lambdaUpdate(Doctor.class)
                        .eq(Doctor::getId, doctorVO.getId())
                        .set(Doctor::getState, doctorVO.getState()));
        return Result.ok();
    }

    /**
     * 批量上架下架的方法
     */
    @OperationLogAnnotation(module = "医生列表", type = "改变状态", description = "批量改变上架状态")
    @PutMapping("/changeOpenByIds")
    @ApiOperation("批量改变上架的状态")
    public Result changeOpenByIds(@RequestBody List<Long> ids) {
        List<Doctor> doctors = doctorServiceImpl.list(
                Wrappers.lambdaQuery(Doctor.class).in(Doctor::getId, ids));
        doctors.forEach(e -> doctorServiceImpl.update(
                Wrappers.lambdaUpdate(Doctor.class)
                        .eq(Doctor::getId, e.getId())
                        .set(Doctor::getState, 1)));
        return Result.ok();
    }

    @OperationLogAnnotation(module = "医生列表", type = "改变状态", description = "批量改变下架状态")
    @PutMapping("/changeCloseByIds")
    @ApiOperation("批量改变下架的状态")
    public Result changeCloseByIds(@RequestBody List<Long> ids) {
        List<Doctor> doctors = doctorServiceImpl.list(
                Wrappers.lambdaQuery(Doctor.class).in(Doctor::getId, ids));
        doctors.forEach(e -> doctorServiceImpl.update(
                Wrappers.lambdaUpdate(Doctor.class)
                        .eq(Doctor::getId, e.getId())
                        .set(Doctor::getState, 0)));
        return Result.ok();
    }


}
