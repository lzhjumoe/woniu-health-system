package com.woniuxy.health.medical.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.medical.mapper.BodycheckTimeMapper;
import com.woniuxy.health.medical.service.IBodycheckTimeService;
import com.woniuxy.health.model.medical.BodycheckTime;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-11
 */
@Service
public class BodycheckTimeServiceImpl extends ServiceImpl<BodycheckTimeMapper, BodycheckTime> implements IBodycheckTimeService {

    private final BodycheckTimeMapper bodycheckTimeMapper;

    @Autowired
    public BodycheckTimeServiceImpl(BodycheckTimeMapper bodycheckTimeMapper){
        this.bodycheckTimeMapper = bodycheckTimeMapper;
    }

}
