package com.woniuxy.health.medical.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.medical.mapper.HospitalDepartmentsMapper;
import com.woniuxy.health.medical.vo.DepartmentIncomeVO;
import com.woniuxy.health.model.medical.HospitalDepartments;
import com.woniuxy.health.medical.service.IHospitalDepartmentsService;
import com.woniuxy.health.vo.HospitalDepartmentVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Service
public class HospitalDepartmentsServiceImpl extends ServiceImpl<HospitalDepartmentsMapper,
        HospitalDepartments> implements IHospitalDepartmentsService {

    private final HospitalDepartmentsMapper hospitalDepartmentsMapper;


    @Autowired
    public HospitalDepartmentsServiceImpl(HospitalDepartmentsMapper hospitalDepartmentsMapper){
        this.hospitalDepartmentsMapper = hospitalDepartmentsMapper;
    }

    //根据科室，查询各科室的年度挂号收入
    @Override
    public List<DepartmentIncomeVO> getDepartmentIncomeForYear(Date firstDayOfYear, Date lastDayOfYear) {
        List<DepartmentIncomeVO> list =
                hospitalDepartmentsMapper.getDepartmentIncomeForYear(firstDayOfYear,lastDayOfYear);

        System.out.println("查看年度数据list = " + list);

        return list;
    }



    //根据科室，查询各科室的月度挂号收入
    @Override
    public List<DepartmentIncomeVO> getDepartmentIncomeForMonth(Date firstDayOfMonth, Date lastDayOfMonth) {
        List<DepartmentIncomeVO> list =
                hospitalDepartmentsMapper.getDepartmentIncomeForMonth(firstDayOfMonth,lastDayOfMonth);

        System.out.println("查看月度数据list = " + list);

        return list;
    }




    @Override
    public List<HospitalDepartments> findFirstLevel() {
        List<HospitalDepartments> list = hospitalDepartmentsMapper.selectList(Wrappers.lambdaQuery(HospitalDepartments.class)
                .eq(HospitalDepartments::getPid, -1)
                .orderByAsc(HospitalDepartments::getId));

        return list;
    }

    //查找全部1级科室对应的2级科室
    @Override
    public Page<HospitalDepartmentVO> showDepartment(Page page,Integer id) {

        Page<HospitalDepartmentVO> voPage = hospitalDepartmentsMapper.showDepartment(page,id);

        return voPage;
    }


    //根据1级科室的id，显示2级科室
    @Override
    public List<HospitalDepartmentVO> showDepartmentById(Integer id) {
        HospitalDepartments firstLevel = hospitalDepartmentsMapper.selectById(id);

        List<HospitalDepartments> secondLevel = hospitalDepartmentsMapper.selectList(Wrappers.lambdaQuery(HospitalDepartments.class)
                .eq(HospitalDepartments::getPid, id));

        List<HospitalDepartmentVO> list = new ArrayList<>();
        secondLevel.forEach(e->{
            HospitalDepartmentVO hospitalDepartmentVO = BeanUtil.copyProperties(e, HospitalDepartmentVO.class);
            hospitalDepartmentVO.setPName(firstLevel.getDepartmentName());
            hospitalDepartmentVO.setPid(firstLevel.getId().intValue());

            list.add(hospitalDepartmentVO);
        });

        System.out.println("查看list = " + list);

        return list;
    }




    //根据1级科室，查询对应的2级科室
    @Override
    public List<HospitalDepartments> findSecondLevel(Integer id) {
        List<HospitalDepartments> list = hospitalDepartmentsMapper.selectList(Wrappers.lambdaQuery(HospitalDepartments.class)
                .eq(HospitalDepartments::getPid, id));

        return list;
    }

}
