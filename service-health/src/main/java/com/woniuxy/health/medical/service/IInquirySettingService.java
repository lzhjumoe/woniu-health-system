package com.woniuxy.health.medical.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.woniuxy.health.medical.vo.SetMealCountVO;
import com.woniuxy.health.model.medical.InquirySetting;
import com.woniuxy.health.vo.ChangeInquiryPriceVO;
import com.woniuxy.health.vo.InquirySettingVo;
import com.woniuxy.health.vo.ChangeInquiryPriceVO;
import com.woniuxy.health.vo.InquirySettingVo;
import com.woniuxy.health.vo.ReceptionVO;

import java.util.List;


/**
 * <p>
 *  服务类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
public interface IInquirySettingService extends IService<InquirySetting> {

    void deleteInquiry(Long id);

    void deleteInquiryList(List<Long> ids);
    Page<InquirySettingVo> getInquirySetting(Long id,Page page);

    void updateInquiryPrice(ChangeInquiryPriceVO priceVO);

    List<SetMealCountVO> getPictureInquiryCount();

    List<SetMealCountVO> getPhoneInquiryCount();
}
