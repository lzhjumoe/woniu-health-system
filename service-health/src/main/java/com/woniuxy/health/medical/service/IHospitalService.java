package com.woniuxy.health.medical.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.woniuxy.health.dto.HospitalDto;
import com.woniuxy.health.medical.vo.DepartmentIncomeVO;
import com.woniuxy.health.medical.vo.HospitalIncomeVO;
import com.woniuxy.health.model.medical.Hospital;

import com.woniuxy.health.vo.HospitalDetailVO;
import com.woniuxy.health.vo.HospitalVO;

import java.util.List;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
public interface IHospitalService extends IService<Hospital> {

    Page<HospitalVO> findByPage(HospitalDto hospitalDto, Page<Hospital> page);


    HospitalDetailVO searchDetail(Integer id);



    List<HospitalIncomeVO> getHospitalIncomeForMonth(Date firstDayOfMonth, Date lastDayOfMonth);

    List<HospitalIncomeVO> getHospitalIncomeForYear(Date firstDayOfYear, Date lastDayOfYear);
}
