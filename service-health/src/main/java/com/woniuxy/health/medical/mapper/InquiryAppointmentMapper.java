package com.woniuxy.health.medical.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.woniuxy.health.model.medical.InquiryAppointment;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author usercheng
 * @since 2023-09-07
 */
@Mapper
public interface InquiryAppointmentMapper extends BaseMapper<InquiryAppointment> {

}
