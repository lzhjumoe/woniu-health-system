package com.woniuxy.health.medical.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.dto.HospitalDto;
import com.woniuxy.health.medical.mapper.HospitalDepartmentsMapper;
import com.woniuxy.health.medical.mapper.HospitalMapper;
import com.woniuxy.health.medical.service.IHospitalService;

import com.woniuxy.health.medical.vo.DepartmentIncomeVO;
import com.woniuxy.health.medical.vo.HospitalIncomeVO;
import com.woniuxy.health.model.medical.Hospital;
import com.woniuxy.health.model.medical.HospitalDepartments;
import com.woniuxy.health.utils.HospitalGradeUtil;
import com.woniuxy.health.utils.HospitalTypeUtil;
import com.woniuxy.health.vo.HospitalDetailVO;
import com.woniuxy.health.vo.HospitalVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;

import java.util.stream.Collectors;


/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Service
public class HospitalServiceImpl extends ServiceImpl<HospitalMapper, Hospital> implements IHospitalService {

    private final HospitalMapper hospitalMapper;
    private final HospitalDepartmentsMapper hospitalDepartmentsMapper;

    @Autowired
    public HospitalServiceImpl(HospitalMapper hospitalMapper, HospitalDepartmentsMapper hospitalDepartmentsMapper){
        this.hospitalMapper = hospitalMapper;
        this.hospitalDepartmentsMapper = hospitalDepartmentsMapper;
    }



    @Override
    public List<HospitalIncomeVO> getHospitalIncomeForYear(Date firstDayOfYear, Date lastDayOfYear) {
        List<HospitalIncomeVO> list =
                hospitalMapper.getHospitalIncomeForYear(firstDayOfYear,lastDayOfYear);
        System.out.println("查看list = " + list);

        return list;
    }


    //本月医院收入top10
    @Override
    public List<HospitalIncomeVO> getHospitalIncomeForMonth(Date firstDayOfMonth, Date lastDayOfMonth) {
        List<HospitalIncomeVO> list =
                hospitalMapper.getHospitalIncomeForMonth(firstDayOfMonth,lastDayOfMonth);
        System.out.println("查看list = " + list);
        return list;
    }




    @Override
    public Page<HospitalVO> findByPage(HospitalDto hospitalDto, Page<Hospital> page) {
        LambdaQueryWrapper<Hospital> lqw = new LambdaQueryWrapper<>();

        //相当于以前写sql语句,加条件

        lqw.like(StringUtils.hasLength(hospitalDto.getHospitalName()),Hospital::getHospitalName,hospitalDto.getHospitalName());
        lqw.like(hospitalDto.getHospitalType() != null,Hospital::getHospitalType,hospitalDto.getHospitalType());
        lqw.like(hospitalDto.getHospitalGrade() != null,Hospital::getHospitalGrade,hospitalDto.getHospitalGrade());


        Page<Hospital> hospitalPage = hospitalMapper.selectPage(page, lqw);


        List<HospitalVO> hospitalVOS = hospitalPage.getRecords().stream().map(e -> {
            HospitalVO hospitalVO = BeanUtil.copyProperties(e, HospitalVO.class);
            hospitalVO.setHospitalType(HospitalTypeUtil.getHospitalType(e.getHospitalType()));
            hospitalVO.setHospitalGrade(HospitalGradeUtil.getHospitalGrade(e.getHospitalGrade()));
            return hospitalVO;
        }).collect(Collectors.toList());

        Page<HospitalVO> HospitalVOPage = new Page<>();
        HospitalVOPage.setRecords(hospitalVOS);
        HospitalVOPage.setTotal(hospitalPage.getTotal());

        return HospitalVOPage;
    }



    @Override
    public HospitalDetailVO searchDetail(Integer id) {
        //医院信息
        Hospital hospital = hospitalMapper.selectById(id);
        //转换成hospitalDetail
        HospitalDetailVO hospitalDetailVO
                = BeanUtil.copyProperties(hospital, HospitalDetailVO.class);

        hospitalDetailVO.setHospitalType(HospitalTypeUtil.getHospitalType(hospital.getHospitalType()));
        hospitalDetailVO.setHospitalGrade(HospitalGradeUtil.getHospitalGrade(hospital.getHospitalGrade()));


        //科室 ,-1 为pid
        List<HospitalDepartments> hospitalDepartments = hospitalDepartmentsMapper.selectList(Wrappers.lambdaQuery(HospitalDepartments.class)
                .eq(HospitalDepartments::getPid, -1));

        String departmentName = "";

        for (HospitalDepartments hospitalDepartment : hospitalDepartments) {

            departmentName = departmentName + hospitalDepartment.getDepartmentName() + " ";
        }

        hospitalDetailVO.setDepartmentName(departmentName);


        return hospitalDetailVO;
    }


}
