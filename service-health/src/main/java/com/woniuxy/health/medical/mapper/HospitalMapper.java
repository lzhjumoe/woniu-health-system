package com.woniuxy.health.medical.mapper;

import com.woniuxy.health.medical.vo.HospitalIncomeVO;
import org.apache.ibatis.annotations.Mapper;
import com.woniuxy.health.model.medical.Hospital;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Mapper
public interface HospitalMapper extends BaseMapper<Hospital> {

    List<HospitalIncomeVO> getHospitalIncomeForMonth(@Param("firstDayOfMonth") Date firstDayOfMonth, @Param("lastDayOfMonth") Date lastDayOfMonth);

    List<HospitalIncomeVO> getHospitalIncomeForYear(@Param("firstDayOfYear") Date firstDayOfYear, @Param("lastDayOfYear") Date lastDayOfYear);
}
