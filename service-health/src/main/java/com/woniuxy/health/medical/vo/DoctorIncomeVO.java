package com.woniuxy.health.medical.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DoctorIncomeVO {
    private String hospitalName;
    private Long doctorIncome;
    private String name;
    private String doctorImgUrl;

}
