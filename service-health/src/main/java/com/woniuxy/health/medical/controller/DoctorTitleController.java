package com.woniuxy.health.medical.controller;

import com.woniuxy.health.medical.service.IDoctorTitleService;
import com.woniuxy.health.model.medical.DoctorTitle;
import com.woniuxy.utils.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Date 2023/9/9 12:17
 * @Author LZH
 * Description:
 */
@RestController
@RequestMapping("/doctorTitle")
@Api(tags = "医生职称管理")
public class DoctorTitleController {

    private final IDoctorTitleService doctorTitleServiceImpl;

    public DoctorTitleController(IDoctorTitleService doctorTitleServiceImpl) {
        this.doctorTitleServiceImpl = doctorTitleServiceImpl;
    }

    /**
     * 查找医生职称
     */
    @GetMapping("/selectDocTitle")
    @ApiOperation("查找医生职称")
    public Result selectDocTitle() {
        List<DoctorTitle> list = doctorTitleServiceImpl.list();
        return Result.ok(list);
    }
}
