package com.woniuxy.health.medical.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniuxy.health.aspect.OperationLogAnnotation;
import com.woniuxy.health.medical.dto.BodycheckMealDTO;
import com.woniuxy.health.medical.service.IBodycheckMealService;
import com.woniuxy.health.medical.service.IBodycheckTimeService;
import com.woniuxy.health.model.medical.BodycheckMeal;
import com.woniuxy.health.model.medical.BodycheckTime;
import com.woniuxy.health.model.medical.Doctor;
import com.woniuxy.health.vo.BodycheckMealVO;
import com.woniuxy.health.vo.DoctorVO;
import com.woniuxy.health.vo.PageVO;
import com.woniuxy.utils.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-11
 */
@RestController
@RequestMapping("/bodycheck-meal")
@Api(tags = "体检套餐管理")
public class BodycheckMealController {

    private final IBodycheckMealService bodycheckMealServiceImpl;
    private final IBodycheckTimeService bodycheckTimeServiceImpl;

    @Autowired
    public BodycheckMealController(IBodycheckMealService bodycheckMealServiceImpl, IBodycheckTimeService bodycheckTimeServiceImpl) {
        this.bodycheckMealServiceImpl = bodycheckMealServiceImpl;
        this.bodycheckTimeServiceImpl = bodycheckTimeServiceImpl;
    }

    @GetMapping("/selectAllMeal")
    @ApiOperation("查询所有的套餐")
    public Result selectAllMeal() {
        List<BodycheckMeal> list = bodycheckMealServiceImpl.list().stream().distinct().collect(Collectors.toList());
        List<BodycheckMeal> collect = bodycheckMealServiceImpl.list().stream().distinct().collect(Collectors.toList());
        return Result.ok(collect);
    }


    @ApiOperation("根据id进行详情页查询")
    @GetMapping("/getOneMeal/{id}")
    public Result getOneMeal(@PathVariable("id") long id) {
        BodycheckMealVO bodycheckMealVO = bodycheckMealServiceImpl.getOneMeal(id);
        return Result.ok(bodycheckMealVO);
    }

    @PostMapping("/selectByPage")
    @ApiOperation("条件分页查询")
    public Result selectByPage(Integer pageNum,
                               Integer pageSize,
                               @RequestBody BodycheckMealDTO bodycheckMealDTO) {
        Page<BodycheckMealVO> bodycheckMealVOPage = Page.of(pageNum, pageSize);
        PageVO<BodycheckMealVO> page = bodycheckMealServiceImpl.selectMealPage(bodycheckMealVOPage, bodycheckMealDTO);
        return Result.ok(page);
    }

    @GetMapping("/selectTime/{id}")
    @ApiOperation("体检套餐详细信息的时间")
    public Result selectTime(@PathVariable("id") Long id) {
        List<BodycheckTime> list = bodycheckTimeServiceImpl.list(
                Wrappers.lambdaQuery(BodycheckTime.class)
                        .eq(BodycheckTime::getMealId, id));
        return Result.ok(list);
    }

    @OperationLogAnnotation(module = "体检套餐", type = "新增", description = "新增体检套餐")
    @PostMapping("/add")
    public Result addMeal(@RequestBody BodycheckMealVO bodycheckMealVO) {
        bodycheckMealServiceImpl.addMeal(bodycheckMealVO);
        return Result.ok();
    }

    @OperationLogAnnotation(module = "体检套餐", type = "改变状态", description = "改变上架下架的状态")
    @PutMapping("/changeState")
    @ApiOperation("改变上架下架的状态")
    public Result changeState(@RequestBody BodycheckMealVO bodycheckMealVO) {
        bodycheckMealServiceImpl.update(
                Wrappers.lambdaUpdate(BodycheckMeal.class)
                        .eq(BodycheckMeal::getId, bodycheckMealVO.getId())
                        .set(BodycheckMeal::getMealState, bodycheckMealVO.getMealState()));
        return Result.ok();
    }

    @OperationLogAnnotation(module = "体检套餐", type = "批量删除", description = "批量删除套餐")
    @ApiOperation("根据id批量删除套餐")
    @DeleteMapping("/deleteBatch/{ids}")
    public Result deleteDocList(@PathVariable("ids") List<Long> ids) {
        bodycheckMealServiceImpl.removeByIds(ids);
        return Result.ok();
    }

    /**
     * 批量上架下架的方法
     */
    @OperationLogAnnotation(module = "体检套餐", type = "批量改变状态", description = "批量改变上架的状态")
    @PutMapping("/changeOpenByIds")
    @ApiOperation("批量改变上架的状态")
    public Result changeOpenByIds(@RequestBody List<Long> ids) {
        List<BodycheckMeal> list = bodycheckMealServiceImpl.list(
                Wrappers.lambdaQuery(BodycheckMeal.class).in(BodycheckMeal::getId, ids));
        list.forEach(e -> bodycheckMealServiceImpl.update(
                Wrappers.lambdaUpdate(BodycheckMeal.class)
                        .eq(BodycheckMeal::getId, e.getId())
                        .set(BodycheckMeal::getMealState, 1)));
        return Result.ok();
    }

    @OperationLogAnnotation(module = "体检套餐", type = "批量改变状态", description = "批量改变下架的状态")
    @PutMapping("/changeCloseByIds")
    @ApiOperation("批量改变下架的状态")
    public Result changeCloseByIds(@RequestBody List<Long> ids) {
        List<BodycheckMeal> list = bodycheckMealServiceImpl.list(
                Wrappers.lambdaQuery(BodycheckMeal.class).in(BodycheckMeal::getId, ids));
        list.forEach(e -> bodycheckMealServiceImpl.update(
                Wrappers.lambdaUpdate(BodycheckMeal.class)
                        .eq(BodycheckMeal::getId, e.getId())
                        .set(BodycheckMeal::getMealState, 0)));
        return Result.ok();
    }

}
