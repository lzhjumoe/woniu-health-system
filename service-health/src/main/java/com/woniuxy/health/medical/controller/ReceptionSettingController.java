package com.woniuxy.health.medical.controller;

import com.woniuxy.health.medical.service.IDoctorService;
import com.woniuxy.health.medical.service.IReceptionSettingService;
import com.woniuxy.health.vo.ReceptionVO;
import com.woniuxy.utils.result.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@RestController
@RequestMapping("/reception-setting")
public class ReceptionSettingController {

    private final IReceptionSettingService receptionSettingServiceImpl;
    //private final IDoctorService doctorServiceImpl;

    @Autowired
    public ReceptionSettingController(IReceptionSettingService receptionSettingServiceImpl) {
        this.receptionSettingServiceImpl = receptionSettingServiceImpl;
    }

//    @PostMapping("/receptionDoc")
//    public Result receptionDoc(@RequestBody ReceptionVO receptionVO) {
//        receptionSettingServiceImpl.addReception(receptionVO);
//        return Result.ok();
//    }

    @PostMapping("/receptionDoc")
    public Result receptionDoc(@RequestBody ReceptionVO receptionVO) {
        receptionSettingServiceImpl.addReception(receptionVO);
        return Result.ok();
    }

}
