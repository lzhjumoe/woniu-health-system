package com.woniuxy.health.medical.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.medical.mapper.AppointmentDetailMapper;
import com.woniuxy.health.medical.service.IAppointmentDetailService;
import com.woniuxy.health.model.medical.AppointmentDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Service
public class AppointmentDetailServiceImpl extends ServiceImpl<AppointmentDetailMapper, AppointmentDetail> implements IAppointmentDetailService {

    private final AppointmentDetailMapper appointmentDetailMapper;

    @Autowired
    public AppointmentDetailServiceImpl(AppointmentDetailMapper appointmentDetailMapper){
        this.appointmentDetailMapper = appointmentDetailMapper;
    }

}
