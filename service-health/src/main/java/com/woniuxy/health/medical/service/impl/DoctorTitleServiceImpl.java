package com.woniuxy.health.medical.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.medical.mapper.DoctorMapper;
import com.woniuxy.health.medical.mapper.DoctorTitleMapper;
import com.woniuxy.health.medical.service.IDoctorTitleService;
import com.woniuxy.health.model.medical.Doctor;
import com.woniuxy.health.model.medical.DoctorTitle;
import org.springframework.stereotype.Service;

/**
 * @Date 2023/9/9 12:18
 * @Author LZH
 * Description:
 */
@Service
public class DoctorTitleServiceImpl extends ServiceImpl<DoctorTitleMapper, DoctorTitle> implements IDoctorTitleService {
}
