package com.woniuxy.health.medical.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.woniuxy.health.model.medical.BodycheckInstitutions;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author usercheng
 * @since 2023-09-09
 */
@Mapper
public interface BodycheckInstitutionsMapper extends BaseMapper<BodycheckInstitutions> {

}
