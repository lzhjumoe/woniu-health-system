package com.woniuxy.health.medical.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.woniuxy.health.dto.OrderDto;
import com.woniuxy.health.medical.vo.DepartmentIncomeVO;
import com.woniuxy.health.model.medical.OrderDetail;
import com.woniuxy.health.vo.InquiryRecordVO;
import com.woniuxy.health.vo.OrderDetailVO;
import com.woniuxy.health.vo.OrderDetailVO2;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
public interface IOrderDetailService extends IService<OrderDetail> {

    List<OrderDetailVO> getOrderList();

    Page<OrderDetailVO> getOrderDetailByCondition(Long id,Integer inquiryType,Integer orderState,
                                                  LocalDateTime star , LocalDateTime end,Page page);



    Page getOrderPage(Page page, OrderDto orderDto);

    OrderDetailVO2 searchOrderDetailById(Long id);


    List<DepartmentIncomeVO> getDepInqIncomeForMonth(Date firstDayOfMonth, Date lastDayOfMonth);

    List<DepartmentIncomeVO> getDepInqIncomeForYear(Date firstDayOfYear, Date lastDayOfYear);

    List<InquiryRecordVO> getInquiryRecords(Integer orderType);
    List<OrderDetailVO2> getAll();
}
