package com.woniuxy.health.medical.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.medical.mapper.BodycheckInstitutionsMapper;
import com.woniuxy.health.medical.mapper.ChinaMapper;
import com.woniuxy.health.medical.service.IBodycheckInstitutionsService;
import com.woniuxy.health.model.medical.BodycheckInstitutions;
import com.woniuxy.health.model.medical.China;
import com.woniuxy.health.vo.BodycheckInstitutionVO;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;
import java.util.Objects;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author usercheng
 * @since 2023-09-09
 */
@Service
public class BodycheckInstitutionsServiceImpl extends ServiceImpl<BodycheckInstitutionsMapper,
        BodycheckInstitutions> implements IBodycheckInstitutionsService {

    private final BodycheckInstitutionsMapper bodycheckInstitutionsMapper;
    private final ChinaMapper chinaMapper;
    public BodycheckInstitutionsServiceImpl(BodycheckInstitutionsMapper bodycheckInstitutionsMapper, ChinaMapper chinaMapper){
        this.bodycheckInstitutionsMapper = bodycheckInstitutionsMapper;
        this.chinaMapper = chinaMapper;
    }

    @Override
    public Page<BodycheckInstitutions> getALLInstitutions(String institutionName, Integer institutionType,Page page) {
        Page pageList = bodycheckInstitutionsMapper.selectPage(page, Wrappers.lambdaQuery(BodycheckInstitutions.class)
                .eq(BodycheckInstitutions::getPid, -1)
                .like(StringUtils.hasLength(institutionName), BodycheckInstitutions::getInstitutionsName, institutionName)
                .eq(Objects.nonNull(institutionType), BodycheckInstitutions::getInstitutionsType, institutionType));
        System.out.println("pageList = " + pageList);
        return pageList;
    }

    //新增一个体检机构总部
    @Override
    public void addInstitution(BodycheckInstitutionVO bodycheckInstitutionVO) {
        //前端传过来的是省市区的id,我们需要根据id找到对应的文字
        //然后将省、市、区、详细地址拼接起来,并放入数据库InstitutionsAddress字段中
        China province = chinaMapper.selectById(bodycheckInstitutionVO.getProvinceId());
        String provinceName = province.getName();

        China city = chinaMapper.selectById(bodycheckInstitutionVO.getCityId());
        String cityName = city.getName();

        String districtName = "";
        China district = chinaMapper.selectById(bodycheckInstitutionVO.getDistrictId());
        //加这个if判断的原因是，数据库的china地区表有点问题，有些市下面没有对应的区，就会导致我们后端这边空指针异常
        if(district != null){
            districtName = district.getName();
        }

        BodycheckInstitutions bodycheckInstitutions = new BodycheckInstitutions();
        bodycheckInstitutions.setInstitutionsName(bodycheckInstitutionVO.getInstitutionsName());
        bodycheckInstitutions.setInstitutionsType(bodycheckInstitutionVO.getInstitutionsType());

        Integer InstitutionsState = Integer.valueOf(bodycheckInstitutionVO.getInstitutionsState());
        bodycheckInstitutions.setInstitutionsState(InstitutionsState);
        bodycheckInstitutions.setPid((long)-1);

        String address = provinceName + cityName + districtName + bodycheckInstitutionVO.getInstitutionsAddress();
        bodycheckInstitutions.setInstitutionsAddress(address);
        bodycheckInstitutions.setInstitutionsImgUrl(bodycheckInstitutionVO.getInstitutionImgUrl());

        bodycheckInstitutionsMapper.insert(bodycheckInstitutions);

    }

    //修改机构上下架状态
    @Override
    public void editInstitutionState(BodycheckInstitutions bodycheckInstitutions) {
        Integer state = bodycheckInstitutions.getInstitutionsState();
        System.out.println("查看是否拿到"+state);
        //我们需要把机构的总店和它旗下的全部分店状态都改成上/下架状态(0、1)
        //根据总店id,找到总店和它的分店,并修改状态
        BodycheckInstitutions mainStore = bodycheckInstitutionsMapper.selectById(bodycheckInstitutions.getId());
        mainStore.setInstitutionsState(bodycheckInstitutions.getInstitutionsState());
        bodycheckInstitutionsMapper.updateById(mainStore);

        //分店
        List<BodycheckInstitutions> branchStores = bodycheckInstitutionsMapper.selectList(Wrappers.lambdaQuery(BodycheckInstitutions.class)
                .eq(Objects.nonNull(bodycheckInstitutions.getId()), BodycheckInstitutions::getPid, bodycheckInstitutions.getId()));

        branchStores.forEach(e->{
            e.setInstitutionsState(bodycheckInstitutions.getInstitutionsState());
            bodycheckInstitutionsMapper.updateById(e);
        });

    }


    //删除总店和它旗下的全部分店
    @Override
    public void deleteInstitution(Long id) {
        bodycheckInstitutionsMapper.deleteById(id);

        //分店
        List<BodycheckInstitutions> branchStores =
                bodycheckInstitutionsMapper.selectList(Wrappers.lambdaQuery(BodycheckInstitutions.class)
                .eq(Objects.nonNull(id), BodycheckInstitutions::getPid, id));

        branchStores.forEach(e->{
            bodycheckInstitutionsMapper.deleteById(e.getId());
        });

    }

    //修改数据库中的图片路径
    @Override
    public void uploadPicture(Long id) {
        BodycheckInstitutions bodycheckInstitutions = new BodycheckInstitutions();
        bodycheckInstitutions.setId(id);

        //拼一个图片路径,例如 http://localhost:7099/imgs/20.jpg
        String url = "http://localhost:7099/imgs/" + id + ".jpg";
        bodycheckInstitutions.setInstitutionsImgUrl(url);

        bodycheckInstitutionsMapper.updateById(bodycheckInstitutions);
    }
}
