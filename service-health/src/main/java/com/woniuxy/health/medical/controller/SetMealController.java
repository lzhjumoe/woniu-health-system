package com.woniuxy.health.medical.controller;


import com.woniuxy.health.medical.service.ISetMealService;
import com.woniuxy.health.medical.vo.SetMealCountVO;
import com.woniuxy.health.medical.vo.SetMealVO;
import com.woniuxy.health.medical.vo.SetMealVolumeVO;
import com.woniuxy.utils.result.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@RestController
@RequestMapping("/set-meal")
public class SetMealController {

    private final ISetMealService setMealServiceImpl;

    @Autowired
    public SetMealController(ISetMealService setMealServiceImpl){
        this.setMealServiceImpl = setMealServiceImpl;
    }

    //在table中显示月度收入和总销量
    @GetMapping("/getIncomeAndSalesForMonth")
    public Result getIncomeAndSalesForMonth(){
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        Date firstDayOfMonth = calendar.getTime();

        calendar.add(Calendar.MONTH, 1);
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        Date lastDayOfMonth = calendar.getTime();

        //获取当月的第一天和最后一天
        System.out.println("The first day of the month is: " + firstDayOfMonth);
        System.out.println("The last day of the month is: " + lastDayOfMonth);

        List<SetMealVO> list = setMealServiceImpl.getIncomeAndSalesForMonth(firstDayOfMonth,lastDayOfMonth);

        return Result.ok(list);
    }


    //在table中显示总收入和总销量
    @GetMapping("/getTotalIncomeAndSales")
    public Result getTotalIncomeAndSales(){
        List<SetMealVO> list = setMealServiceImpl.getTotalIncomeAndSales();

        return Result.ok(list);
    }


    //获取当月的套餐销售总额
    @GetMapping("/getSetMealIncomeForMonth")
    public Result getSetMealIncomeForMonth(){
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        Date firstDayOfMonth = calendar.getTime();

        calendar.add(Calendar.MONTH, 1);
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        Date lastDayOfMonth = calendar.getTime();

        //获取当月的第一天和最后一天
        System.out.println("The first day of the month is: " + firstDayOfMonth);
        System.out.println("The last day of the month is: " + lastDayOfMonth);

        List<SetMealCountVO> list = setMealServiceImpl.getSetMealIncomeForMonth(firstDayOfMonth,lastDayOfMonth);

        return Result.ok(list);

    }


    //获取当月的套餐销售量
    @GetMapping("/getSetMealCountForMonth")
    public Result getSetMealCountForMonth(){
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        Date firstDayOfMonth = calendar.getTime();

        calendar.add(Calendar.MONTH, 1);
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        Date lastDayOfMonth = calendar.getTime();

        //获取当月的第一天和最后一天
        System.out.println("The first day of the month is: " + firstDayOfMonth);
        System.out.println("The last day of the month is: " + lastDayOfMonth);


        List<SetMealCountVO> list = setMealServiceImpl.getSetMealCountForMonth(firstDayOfMonth,lastDayOfMonth);

        return Result.ok(list);
    }


    @GetMapping("/getSetMealIncome")
    public Result getSetMealIncome(){
        List<SetMealVolumeVO> list = setMealServiceImpl.getSetMealIncome();

        return Result.ok(list);
    }

    @GetMapping("/getSetMealCount")
    public Result getSetMealCount(){
        List<SetMealCountVO> list = setMealServiceImpl.getSetMealCount();

        return Result.ok(list);
    }


}
