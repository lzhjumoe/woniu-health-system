package com.woniuxy.health.medical.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniuxy.health.aspect.OperationLogAnnotation;
import com.woniuxy.health.dto.HospitalDto;
import com.woniuxy.health.medical.service.IHospitalDepartmentsService;
import com.woniuxy.health.medical.service.IHospitalService;
import com.woniuxy.health.model.medical.Doctor;
import com.woniuxy.health.medical.vo.DepartmentIncomeVO;
import com.woniuxy.health.medical.vo.HospitalIncomeVO;
import com.woniuxy.health.model.medical.Hospital;
import com.woniuxy.health.utils.HospitalGradeUtil;
import com.woniuxy.health.utils.HospitalTypeUtil;
import com.woniuxy.health.vo.DocHospVO;
import com.woniuxy.health.vo.DoctorVO;
import com.woniuxy.health.vo.HospitalVO;
import com.woniuxy.utils.result.Result;
import com.woniuxy.health.vo.HospitalDetailVO;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import java.util.*;
import java.util.List;
import java.util.stream.Collectors;


/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@RestController
@RequestMapping("/hospital")
public class HospitalController {

    private final IHospitalService hospitalServiceImpl;
    private final IHospitalDepartmentsService hospitalDepartmentsService;

    @Autowired
    public HospitalController(IHospitalService hospitalServiceImpl, IHospitalDepartmentsService hospitalDepartmentsService) {
        this.hospitalServiceImpl = hospitalServiceImpl;
        this.hospitalDepartmentsService = hospitalDepartmentsService;
    }



    @GetMapping("/getHospitalIncomeForYear")
    public Result getHospitalIncomeForYear(){
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_YEAR, 1);
        Date firstDayOfYear = calendar.getTime();

        calendar.set(Calendar.DAY_OF_YEAR, calendar.getActualMaximum(Calendar.DAY_OF_YEAR));
        Date lastDayOfYear = calendar.getTime();

        System.out.println("The first day of the year is: " + firstDayOfYear);
        System.out.println("The last day of the year is: " + lastDayOfYear);


        List<HospitalIncomeVO> VOs =
                hospitalServiceImpl.getHospitalIncomeForYear(firstDayOfYear,lastDayOfYear);

        return Result.ok(VOs);

    }



    //获取本月医院收入top10
    @GetMapping("/getHospitalIncomeForMonth")
    public Result getHospitalIncomeForMonth(){
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        Date firstDayOfMonth = calendar.getTime();

        calendar.add(Calendar.MONTH, 1);
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        Date lastDayOfMonth = calendar.getTime();

        //获取当月的第一天和最后一天
        System.out.println("The first day of the month is: " + firstDayOfMonth);
        System.out.println("The last day of the month is: " + lastDayOfMonth);

        List<HospitalIncomeVO> list =
                hospitalServiceImpl.getHospitalIncomeForMonth(firstDayOfMonth,lastDayOfMonth);

        return Result.ok(list);
    }



    /**
     * 查询医院id和医院名称
     *
     * @return
     */
    @OperationLogAnnotation(module = "医院列表", type = "改变状态", description = "改变一条状态")
    @PutMapping("/changeState")
    @ApiOperation("改变上架下架的状态")
    public Result changeState(@RequestBody HospitalVO hospitalVO) {
        hospitalServiceImpl.update(
                Wrappers.lambdaUpdate(Hospital.class)
                        .eq(Hospital::getId, hospitalVO.getId())
                        .set(Hospital::getHospitalState, hospitalVO.getHospitalState()));
        return Result.ok();
    }

    @GetMapping("/selectDocHosp")
    public Result selectDocHosp(String hospname) {
        List<DocHospVO> docHospVOS = hospitalServiceImpl.list(
                        Wrappers.lambdaQuery(Hospital.class)
                                .likeRight(StringUtils.hasLength(hospname), Hospital::getHospitalName, hospname))
                .stream().map(e ->
                        new DocHospVO(e.getId(), e.getHospitalName())
                ).collect(Collectors.toList());
        return Result.ok(docHospVOS);
    }



    //查看详情 传医院id, 返回详情数据
    //漏了科室还没写
    @GetMapping("/searchDetail/{id}")
    public Result<HospitalDetailVO> searchDetail(@PathVariable Integer id){

        HospitalDetailVO hospitalDetailVO = hospitalServiceImpl.searchDetail(id);


        return Result.ok(hospitalDetailVO);
    }

    //添加
    @OperationLogAnnotation(module = "医院列表", type = "新增", description = "新增一条医院记录")
    @PostMapping("/save")
    public Result<Hospital> save(@RequestBody Hospital hospital){

        hospitalServiceImpl.save(hospital);

        return Result.ok();
    }

    //删除
    @OperationLogAnnotation(module = "医院列表", type = "删除", description = "删除一条医院记录")
    @DeleteMapping("/remove")
    public Result<Hospital> remove(Integer id){

        hospitalServiceImpl.removeById(id);
        return Result.ok();

    }

    //修改
    @OperationLogAnnotation(module = "医院列表", type = "修改", description = "修改一条医院记录")
    @PostMapping("/edit")
    public Result<Hospital> edit(@RequestBody Hospital hospital){
        hospital.setUpdateTime(new Date());
        hospitalServiceImpl.updateById(hospital);
        return Result.ok();
    }

    //分页查询
    @PostMapping ("/list/{pageNum}/{pageSize}")
    public Result list(@RequestBody HospitalDto hospitalDto,
                       @PathVariable("pageNum") Integer pageNum,
                       @PathVariable("pageSize") Integer pageSize){

        Page<Hospital> page = Page.of(pageNum,pageSize);
        Page<HospitalVO> hospitalVOPage = hospitalServiceImpl.findByPage(hospitalDto, page);
        return Result.ok(hospitalVOPage);

    }

    //下拉框带条件查询,从枚举获取全部的医院类型
    @GetMapping("/getAllHospitalType")
    public Result getAllHospitalType(){

        List hospitalType = HospitalTypeUtil.getHospitalType();

        return Result.ok(hospitalType);

    }

    @GetMapping("/getAllHospitalGrade")
    public Result getAllHospitalGrade(){

        List hospitalGrade = HospitalGradeUtil.getHospitalGrade();

        return Result.ok(hospitalGrade);
    }

    //批量删除
    @OperationLogAnnotation(module = "医院列表", type = "删除", description = "批量删除")
    @PostMapping("/removeBatch")
    public Result removeBatch(@RequestBody Integer[] ids){

        List<Integer> list = Arrays.asList(ids);

        hospitalServiceImpl.removeBatchByIds(list);

        return Result.ok();
    }

    //批量上架
    @OperationLogAnnotation(module = "医院列表", type = "上架", description = "批量上架")
    @PostMapping("/editBatch/{val}")
    public Result editBatch(@PathVariable Integer val, @RequestBody Integer[] ids){

        List<Integer> list = Arrays.asList(ids);
        if(val == 1){
            List<Hospital> hospitals = hospitalServiceImpl.listByIds(list);
            hospitals.forEach(e ->{
                e.setHospitalState(1);
            });
            hospitalServiceImpl.updateBatchById(hospitals);
        }else{
            List<Hospital> hospitals = hospitalServiceImpl.listByIds(list);
            hospitals.forEach(e ->{
                e.setHospitalState(0);
            });
            hospitalServiceImpl.updateBatchById(hospitals);
        }

        return Result.ok();
    }

}
