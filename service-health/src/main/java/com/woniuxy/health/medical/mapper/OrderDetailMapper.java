package com.woniuxy.health.medical.mapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniuxy.health.dto.OrderDetailDTO;
import com.woniuxy.health.dto.OrderDto;
import com.woniuxy.health.medical.vo.DepartmentIncomeVO;
import com.woniuxy.health.medical.vo.SetMealCountVO;
import com.woniuxy.health.vo.InquiryRecordVO;
import com.woniuxy.health.vo.OrderDetailVO;
import com.woniuxy.health.vo.OrderDetailVO2;
import org.apache.ibatis.annotations.Mapper;
import com.woniuxy.health.model.medical.OrderDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Mapper
public interface OrderDetailMapper extends BaseMapper<OrderDetail> {


    List<OrderDetailDTO> selectOrderDetail();

    Page<OrderDetailDTO> selectOrderDetailByCondition(@Param("id") Long id,
                                                      @Param("orderType")Integer orderType,
                                                      @Param("orderState")Integer orderState,
                                                      @Param("star") LocalDateTime star ,
                                                      @Param("end") LocalDateTime end,
                                                      Page page);


    Page selectPage2(Page page, @Param("a") OrderDto orderDto);

    OrderDetailVO2 searchOrderDetailById(Long id);


    List<DepartmentIncomeVO> getDepInqIncomeForMonth(@Param("firstDayOfMonth") Date firstDayOfMonth, @Param("lastDayOfMonth") Date lastDayOfMonth);

    List<DepartmentIncomeVO> getDepInqIncomeForYear(@Param("firstDayOfYear") Date firstDayOfYear, @Param("lastDayOfYear") Date lastDayOfYear);

    List<InquiryRecordVO> getInquiryRecords(Integer orderType);

    List<SetMealCountVO> getPictureInquiryCount();

    List<SetMealCountVO> getPhoneInquiryCount();
    List<OrderDetailVO2> getAll();
}
