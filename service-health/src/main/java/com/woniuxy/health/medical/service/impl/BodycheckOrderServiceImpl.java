package com.woniuxy.health.medical.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.medical.dto.CheckOrderDTO;
import com.woniuxy.health.medical.mapper.BodycheckOrderMapper;
import com.woniuxy.health.medical.service.IBodycheckOrderService;
import com.woniuxy.health.model.medical.BodycheckOrder;
import com.woniuxy.health.vo.BodycheckMealVO;
import com.woniuxy.health.vo.CheckOrderVO;
import com.woniuxy.health.vo.PageVO;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-11
 */
@Service
public class BodycheckOrderServiceImpl extends ServiceImpl<BodycheckOrderMapper, BodycheckOrder> implements IBodycheckOrderService {

    private final BodycheckOrderMapper bodycheckOrderMapper;

    @Autowired
    public BodycheckOrderServiceImpl(BodycheckOrderMapper bodycheckOrderMapper){
        this.bodycheckOrderMapper = bodycheckOrderMapper;
    }

    @Override
    public PageVO<CheckOrderVO> selectOrderPage(Page<CheckOrderVO> bodycheckMealVOPage, CheckOrderDTO checkOrderDTO) {

        Page<CheckOrderVO> page = bodycheckOrderMapper.selectMealPage(bodycheckMealVOPage, checkOrderDTO);
        return new PageVO<>(page.getTotal(), page.getRecords());
    }
}
