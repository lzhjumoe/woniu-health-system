package com.woniuxy.health.medical.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.dto.OrderDetailDTO;
import com.woniuxy.health.dto.OrderDto;
import com.woniuxy.health.medical.mapper.OrderDetailMapper;
import com.woniuxy.health.medical.vo.DepartmentIncomeVO;
import com.woniuxy.health.model.medical.OrderDetail;
import com.woniuxy.health.medical.service.IOrderDetailService;
import com.woniuxy.health.utils.OrderStatusUtil;
import com.woniuxy.health.vo.InquiryRecordVO;
import com.woniuxy.health.vo.OrderDetailVO;
import com.woniuxy.health.vo.OrderDetailVO2;
import org.elasticsearch.common.recycler.Recycler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Service
public class OrderDetailServiceImpl extends ServiceImpl<OrderDetailMapper, OrderDetail> implements IOrderDetailService {

    private final OrderDetailMapper orderDetailMapper;

    @Override
    public List<OrderDetailVO2> getAll() {
        List<OrderDetailVO2> all = orderDetailMapper.getAll();
        return all;
    }

    @Autowired
    public OrderDetailServiceImpl(OrderDetailMapper orderDetailMapper){
        this.orderDetailMapper = orderDetailMapper;
    }


    //用户端问诊记录接口
    @Override
    public List<InquiryRecordVO> getInquiryRecords(Integer orderType) {
        List<InquiryRecordVO> list = orderDetailMapper.getInquiryRecords(orderType);
        return list;
    }


    //各科室当年的问诊收入
    @Override
    public List<DepartmentIncomeVO> getDepInqIncomeForYear(Date firstDayOfYear, Date lastDayOfYear) {
        List<DepartmentIncomeVO> list =
                orderDetailMapper.getDepInqIncomeForYear(firstDayOfYear,lastDayOfYear);
        return list;
    }




    //各科室当月的问诊收入
    @Override
    public List<DepartmentIncomeVO> getDepInqIncomeForMonth(Date firstDayOfMonth, Date lastDayOfMonth) {
        List<DepartmentIncomeVO>list =
                orderDetailMapper.getDepInqIncomeForMonth(firstDayOfMonth,lastDayOfMonth);
        System.out.println("查看当月list = " + list);
        return list;
    }




    //tjy 查询体检订单
    @Override
    public Page getOrderPage(Page page, OrderDto orderDto) {
        Page orderVO = orderDetailMapper.selectPage2(page, orderDto);
        return orderVO;
    }

    @Override
    public OrderDetailVO2 searchOrderDetailById(Long id) {
        OrderDetailVO2 orderDetailVO2 = orderDetailMapper.searchOrderDetailById(id);
        return orderDetailVO2;
    }




    //获取全部问诊订单的数据
    @Override
    public List<OrderDetailVO> getOrderList() {
        List<OrderDetailDTO> list = orderDetailMapper.selectOrderDetail();
        System.out.println("=== 打印list === " + list);

        List<OrderDetailVO> orderDetailVOList = list.stream().map(e -> {
            OrderDetailVO orderDetailVO = BeanUtil.copyProperties(e, OrderDetailVO.class);

            //将订单状态和问诊类型的1、0换成文字
            String orderStatus = OrderStatusUtil.getOrderStatus(e.getOrderState());
            String inquiryType = OrderStatusUtil.getOrderType(e.getOrderType());
            orderDetailVO.setOrderState(orderStatus);
            orderDetailVO.setOrderType(inquiryType);
            return orderDetailVO;
        }).collect(Collectors.toList());

        System.out.println("=== 打印看看collect === " + orderDetailVOList);
        return orderDetailVOList;
    }



    @Override
    public Page<OrderDetailVO> getOrderDetailByCondition(Long id, Integer orderType, Integer orderState,
                                                         LocalDateTime star , LocalDateTime end, Page page) {

        Page<OrderDetailDTO> dtoPage =
                 orderDetailMapper.selectOrderDetailByCondition(id, orderType, orderState, star, end, page);

        dtoPage.getTotal();
        List<OrderDetailDTO> records = dtoPage.getRecords();
        System.out.println("打印查看records = " + records);

        //Page对象有总数total和records数据,我们需要加工的是records数据的部分,把状态码变成文字状态
        List<OrderDetailDTO> dtoList = records;
        List<OrderDetailVO> voList = dtoList.stream().map(e -> {
            OrderDetailVO VO = BeanUtil.copyProperties(e, OrderDetailVO.class);

            String strStatus = OrderStatusUtil.getOrderStatus(e.getOrderState());
            VO.setOrderState(strStatus);

            String strType = OrderStatusUtil.getOrderType(e.getOrderType());
            VO.setOrderType(strType);

            return VO;
        }).collect(Collectors.toList());

        //将状态字段加工好的数据从新塞回records中
        Page<OrderDetailVO> voPage = new Page<>();
        voPage.setTotal(dtoPage.getTotal());
        voPage.setRecords(voList);

        System.out.println("查看分页voList = " + voPage);

        return voPage;
    }


}
