package com.woniuxy.health.medical.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniuxy.health.aspect.OperationLogAnnotation;
import com.woniuxy.health.medical.service.IHospitalDepartmentsService;
import com.woniuxy.health.medical.vo.DepartmentIncomeVO;
import com.woniuxy.health.model.medical.HospitalDepartments;
import com.woniuxy.health.vo.HospitalDepartmentVO;
import com.woniuxy.utils.result.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@RestController
@RequestMapping("/hospital-departments")
public class HospitalDepartmentsController {

    private final IHospitalDepartmentsService hospitalDepartmentsServiceImpl;

    @Autowired
    public HospitalDepartmentsController(IHospitalDepartmentsService hospitalDepartmentsServiceImpl){
        this.hospitalDepartmentsServiceImpl = hospitalDepartmentsServiceImpl;
    }

    @OperationLogAnnotation(module = "医院科室", type = "删除", description = "删除一条二级科室")




    //统计各科室本年的收入
    @GetMapping("/getDepartmentIncomeForYear")
    public Result getDepartmentIncomeForYear(){
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_YEAR, 1);
        Date firstDayOfYear = calendar.getTime();

        calendar.set(Calendar.DAY_OF_YEAR, calendar.getActualMaximum(Calendar.DAY_OF_YEAR));
        Date lastDayOfYear = calendar.getTime();

        System.out.println("The first day of the year is: " + firstDayOfYear);
        System.out.println("The last day of the year is: " + lastDayOfYear);


        List<DepartmentIncomeVO> VOs =
                hospitalDepartmentsServiceImpl.getDepartmentIncomeForYear(firstDayOfYear,lastDayOfYear);

        return Result.ok(VOs);

    }



    //统计各科室本月的收入
    @GetMapping("/getDepartmentIncomeForMonth")
    public Result getDepartmentIncomeForMonth(){
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        Date firstDayOfMonth = calendar.getTime();

        calendar.add(Calendar.MONTH, 1);
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        Date lastDayOfMonth = calendar.getTime();

        //获取当月的第一天和最后一天
        System.out.println("The first day of the month is: " + firstDayOfMonth);
        System.out.println("The last day of the month is: " + lastDayOfMonth);


        List<DepartmentIncomeVO> VOs =
                hospitalDepartmentsServiceImpl.getDepartmentIncomeForMonth(firstDayOfMonth,lastDayOfMonth);

        return Result.ok(VOs);

    }




    //根据2级科室的主键id，删除这个2级科室
    @PostMapping("/deleteSecondLevel/{id}")
    public Result deleteSecondLevel(@PathVariable("id") Integer id){
        hospitalDepartmentsServiceImpl.removeById(id);
        return Result.ok();
    }

    //根据1级科室的id，添加2级科室
    //我需要前端传1级科室的id，和需要被添加的2级科室的信息
    @OperationLogAnnotation(module = "医院科室", type = "添加", description = "添加一条二级科室")
    @GetMapping("/addSecondLevel")
    public Result addSecondLevel(Long id ,String departmentName){
        HospitalDepartments hospitalDepartments = new HospitalDepartments();
        hospitalDepartments.setPid(id);
        hospitalDepartments.setDepartmentName(departmentName);

        hospitalDepartmentsServiceImpl.save(hospitalDepartments);

        return Result.ok();
    }


    //封装一个VO对象，返回1级科室和对应的2级科室
    @GetMapping("/showDepartment")
    public Result showDepartment(Integer pageNum,Integer pageSize,Integer sDepartId){
        Page page = Page.of(pageNum,pageSize);

        Page<HospitalDepartmentVO> voPage = hospitalDepartmentsServiceImpl.showDepartment(page,sDepartId);
        return Result.ok(voPage);
    }


    //根据1级科室id，找到对应的2级科室
    @GetMapping("/showDepartmentById/{id}")
    public Result showDepartmentById(@PathVariable("id") Integer id){
        List<HospitalDepartmentVO> list
                = hospitalDepartmentsServiceImpl.showDepartmentById(id);

        return Result.ok(list);
    }


    //根据1级科室id，查询对应的2级科室
    @GetMapping("/findSecondLevel")
    public Result findSecondLevel(Integer id){
        List<HospitalDepartments> list = hospitalDepartmentsServiceImpl.findSecondLevel(id);
        return Result.ok(list);
    }



    //查询全部1级科室,显示在下拉框中
    @GetMapping("/findFirstLevel")
    public Result findFirstLevel(){
        List<HospitalDepartments> list = hospitalDepartmentsServiceImpl.findFirstLevel();

        return Result.ok(list);
    }

}
