package com.woniuxy.health.medical.dto;

import lombok.Data;

@Data
public class HospitalDTO {
    private Long id;
    private String hospitalName;
    private Byte hospitalType;
    private Byte hospitalGrade;

}
