package com.woniuxy.health.medical.controller;

import com.woniuxy.health.medical.service.IAppointmentDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@RestController
@RequestMapping("/appointment-detail")
public class AppointmentDetailController {

    private final IAppointmentDetailService appointmentDetailServiceImpl;

    @Autowired
    public AppointmentDetailController(IAppointmentDetailService appointmentDetailServiceImpl){
        this.appointmentDetailServiceImpl = appointmentDetailServiceImpl;
    }

}
