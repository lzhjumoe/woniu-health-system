package com.woniuxy.health.medical.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.woniuxy.health.model.medical.InquiryAppointment;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author usercheng
 * @since 2023-09-07
 */
public interface IInquiryAppointmentService extends IService<InquiryAppointment> {

    void addReceptionTime(Integer orderId);
}
