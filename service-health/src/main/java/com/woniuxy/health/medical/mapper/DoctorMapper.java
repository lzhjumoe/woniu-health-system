package com.woniuxy.health.medical.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniuxy.health.medical.dto.DoctorQueryDTO;
import com.woniuxy.health.medical.vo.DoctorIncomeVO;
import com.woniuxy.health.vo.DoctorVO;
import com.woniuxy.health.vo.PageVO;
import org.apache.ibatis.annotations.Mapper;
import com.woniuxy.health.model.medical.Doctor;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Mapper
public interface DoctorMapper extends BaseMapper<Doctor> {
    Page<DoctorVO> selectDocPage(@Param("doctorVOPage") Page<DoctorVO> doctorVOPage,
                                 @Param("doctorQueryDTO") DoctorQueryDTO doctorQueryDTO);

    List<DoctorIncomeVO> getDoctorIncomeTop20();
}
