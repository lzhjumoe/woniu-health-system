package com.woniuxy.health.medical.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HospitalIncomeVO {
    private String hospitalName;
    private Long hospitalIncome;
    private String hospitalAddress;

}
