package com.woniuxy.health.medical.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.woniuxy.health.model.medical.China;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author usercheng
 * @since 2023-09-09
 */
@Mapper
public interface ChinaMapper extends BaseMapper<China> {

}
