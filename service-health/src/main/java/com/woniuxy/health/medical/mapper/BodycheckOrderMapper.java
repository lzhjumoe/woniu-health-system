package com.woniuxy.health.medical.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniuxy.health.medical.dto.CheckOrderDTO;
import com.woniuxy.health.model.medical.BodycheckOrder;
import com.woniuxy.health.vo.CheckOrderVO;
import org.apache.ibatis.annotations.Mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-11
 */
@Mapper
public interface BodycheckOrderMapper extends BaseMapper<BodycheckOrder> {

    Page<CheckOrderVO> selectMealPage(@Param("bodycheckMealVOPage") Page<CheckOrderVO> bodycheckMealVOPage,
                                      @Param("checkOrderDTO") CheckOrderDTO checkOrderDTO);
}
