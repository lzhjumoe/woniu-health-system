package com.woniuxy.health.medical.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.medical.dto.InquirySettingDTO;
import com.woniuxy.health.medical.mapper.InquirySettingMapper;
import com.woniuxy.health.medical.mapper.OrderDetailMapper;
import com.woniuxy.health.medical.service.IInquirySettingService;
import com.woniuxy.health.medical.vo.SetMealCountVO;
import com.woniuxy.health.model.medical.InquirySetting;
import com.woniuxy.health.utils.OrderStatusUtil;
import com.woniuxy.health.vo.ChangeInquiryPriceVO;
import com.woniuxy.health.vo.InquirySettingVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Service
public class InquirySettingServiceImpl extends ServiceImpl<InquirySettingMapper, InquirySetting> implements IInquirySettingService {

    private final InquirySettingMapper inquirySettingMapper;
    private final OrderDetailMapper orderDetailMapper;

    @Autowired
    public InquirySettingServiceImpl(InquirySettingMapper inquirySettingMapper, OrderDetailMapper orderDetailMapper) {
        this.inquirySettingMapper = inquirySettingMapper;
        this.orderDetailMapper = orderDetailMapper;
    }

    //获取各科室的电话问诊数量
    @Override
    public List<SetMealCountVO> getPhoneInquiryCount() {
        List<SetMealCountVO> list = orderDetailMapper.getPhoneInquiryCount();

        return list;
    }


    //获取各科室的图文问诊数量
    @Override
    public List<SetMealCountVO> getPictureInquiryCount() {
        List<SetMealCountVO> list = orderDetailMapper.getPictureInquiryCount();

        return list;
    }




    @Override
    public void deleteInquiry(Long id) {
        List<InquirySetting> inquiries = inquirySettingMapper.selectList(
                Wrappers.lambdaQuery(InquirySetting.class)
                        .eq(!Objects.isNull(id), InquirySetting::getDoctorId, id));
        for (InquirySetting inquiry : inquiries) {
            inquirySettingMapper.deleteById(inquiry);
        }
    }

    @Override
    public void deleteInquiryList(List<Long> ids) {
        List<InquirySetting> inquirySettings = inquirySettingMapper.selectList(Wrappers.lambdaQuery(InquirySetting.class)
                .in(!Objects.isNull(ids), InquirySetting::getDoctorId, ids));
        List<Long> collect = inquirySettings.stream().map(e -> e.getId()).collect(Collectors.toList());
        inquirySettingMapper.deleteBatchIds(collect);
    }

    @Override
    public Page<InquirySettingVo> getInquirySetting(Long doctorId,Page page) {
        //1、从数据库中查找出我想要的数据(但是这个数据的状态字段是数字状态码)
        Page<InquirySettingDTO> dtoPage = inquirySettingMapper.selectInquirySetting(doctorId,page);

        List<InquirySettingDTO> dtoList = dtoPage.getRecords();

        //2、把数据封装成VO对象,因为问诊类型需要从数字变成文字返回给前端
        List<InquirySettingVo> voList = dtoList.stream().map(e -> {
            InquirySettingVo VO = BeanUtil.copyProperties(e, InquirySettingVo.class);
            String inquiryType = OrderStatusUtil.getOrderType(e.getInquiryType());
            VO.setInquiryType(inquiryType);
            return VO;
        }).collect(Collectors.toList());

        Page voPage = new Page<>();
        voPage.setTotal(dtoPage.getTotal());
        voPage.setRecords(voList);

        return voPage;

    }


    //修改问诊设置表中的问诊价格
    @Override
    public void updateInquiryPrice(ChangeInquiryPriceVO priceVO) {
        InquirySetting inquirySetting = BeanUtil.copyProperties(priceVO, InquirySetting.class);

        Double price = priceVO.getInquiryPrice();
        inquirySetting.setInquiryPrice(price);

        inquirySettingMapper.updateById(inquirySetting);

    }




}
