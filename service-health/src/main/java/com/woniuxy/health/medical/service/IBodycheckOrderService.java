package com.woniuxy.health.medical.service;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.woniuxy.health.medical.dto.CheckOrderDTO;
import com.woniuxy.health.model.medical.BodycheckOrder;
import com.woniuxy.health.vo.CheckOrderVO;
import com.woniuxy.health.vo.PageVO;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-11
 */
public interface IBodycheckOrderService extends IService<BodycheckOrder> {

    PageVO<CheckOrderVO> selectOrderPage(Page<CheckOrderVO> bodycheckMealVOPage, CheckOrderDTO checkOrderDTO);
}
