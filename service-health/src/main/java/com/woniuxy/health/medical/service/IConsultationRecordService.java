package com.woniuxy.health.medical.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.woniuxy.health.model.medical.ConsultationRecord;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
public interface IConsultationRecordService extends IService<ConsultationRecord> {

}
