package com.woniuxy.health.medical.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniuxy.health.aspect.OperationLogAnnotation;
import com.woniuxy.health.medical.dto.BodycheckMealDTO;
import com.woniuxy.health.medical.dto.CheckOrderDTO;
import com.woniuxy.health.medical.service.IBodycheckOrderService;
import com.woniuxy.health.model.medical.BodycheckOrder;
import com.woniuxy.health.oss.OSSUtil;
import com.woniuxy.health.vo.BodycheckMealVO;
import com.woniuxy.health.vo.CheckOrderVO;
import com.woniuxy.health.vo.PageVO;
import com.woniuxy.utils.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-11
 */
@Api("体检订单")
@RestController
@RequestMapping("/bodycheck-order")
public class BodycheckOrderController {

    private final IBodycheckOrderService bodycheckOrderServiceImpl;

    @Autowired
    public BodycheckOrderController(IBodycheckOrderService bodycheckOrderServiceImpl) {
        this.bodycheckOrderServiceImpl = bodycheckOrderServiceImpl;
    }

    @Resource
    private OSSUtil ossUtil;

    @PostMapping("/selectByPage")
    @ApiOperation("条件分页查询")
    public Result selectByPage(Integer pageNum,
                               Integer pageSize,
                               @RequestBody CheckOrderDTO checkOrderDTO) {
        Page<CheckOrderVO> bodycheckMealVOPage = Page.of(pageNum, pageSize);
        PageVO<CheckOrderVO> page = bodycheckOrderServiceImpl.selectOrderPage(bodycheckMealVOPage, checkOrderDTO);
        return Result.ok(page);
    }

    @OperationLogAnnotation(module = "体检订单", type = "上传报告", description = "上传一条报告")
    @GetMapping("/uploadReport")
    @ApiOperation("上传报告")
    public Result uploadReport(Long userId, String reportUrl) {
        System.out.println("userId = " + userId);
        System.out.println("reportUrl = " + reportUrl);
        BodycheckOrder bodycheckOrder = bodycheckOrderServiceImpl.getById(userId);
        bodycheckOrder.setReportUrl(reportUrl);
        bodycheckOrder.setState(1);
        bodycheckOrder.setUpdateTime(Timestamp.valueOf(LocalDateTime.now()));
        bodycheckOrderServiceImpl.updateById(bodycheckOrder);
        return Result.ok();
    }

    @OperationLogAnnotation(module = "体检套餐", type = "下载报告", description = "下载一条报告")
    @GetMapping("/downloadReport/{id}")
    @ApiOperation("下载报告")
    public Result downloadReport(@PathVariable("id") Long id) {
        BodycheckOrder bodycheckOrder = bodycheckOrderServiceImpl.getById(id);
        //String filePath = "D:\\file";
        String keyFromUrl = bodycheckOrder.getReportUrl();
        //ossUtil.downloadFile(keyFromUrl, filePath);
        return Result.ok(keyFromUrl);
    }

    @OperationLogAnnotation(module = "体检套餐", type = "删除报告", description = "删除一条报告")
    @GetMapping("/deleteReport/{id}")
    @ApiOperation("删除报告")
    public Result deleteReport(@PathVariable("id") Long id) {
        BodycheckOrder bodycheckOrder = bodycheckOrderServiceImpl.getById(id);
        bodycheckOrder.setReportUrl("");
        bodycheckOrder.setState(0);
        bodycheckOrder.setUpdateTime(null);
        bodycheckOrderServiceImpl.updateById(bodycheckOrder);
        return Result.ok();
    }
}
