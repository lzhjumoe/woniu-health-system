package com.woniuxy.health.medical.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.woniuxy.health.model.medical.BodycheckInstitutions;
import com.woniuxy.health.vo.BodycheckInstitutionVO;

import java.util.List;


/**
 * <p>
 *  服务类
 * </p>
 *
 * @author usercheng
 * @since 2023-09-09
 */
public interface IBodycheckInstitutionsService extends IService<BodycheckInstitutions> {

    Page<BodycheckInstitutions> getALLInstitutions(String institutionName, Integer institutionType,Page page);

    void addInstitution(BodycheckInstitutionVO bodycheckInstitutionVO);

    void editInstitutionState(BodycheckInstitutions bodycheckInstitutions);

    void deleteInstitution(Long id);

    void uploadPicture(Long id);
}
