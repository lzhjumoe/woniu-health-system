package com.woniuxy.health.medical.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SetMealVO {
    private String setMealName;
    private Long salesVolume;//销售额
    private Long salesCount;//销售量
}
