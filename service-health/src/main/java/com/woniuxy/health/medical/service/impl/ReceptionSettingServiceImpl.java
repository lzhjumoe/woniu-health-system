package com.woniuxy.health.medical.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.medical.mapper.DoctorMapper;
import com.woniuxy.health.medical.mapper.ReceptionSettingMapper;
import com.woniuxy.health.medical.service.IReceptionSettingService;
import com.woniuxy.health.model.medical.Doctor;
import com.woniuxy.health.model.medical.ReceptionSetting;
import com.woniuxy.health.vo.ReceptionVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Time;
import java.time.LocalTime;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Service
public class ReceptionSettingServiceImpl extends ServiceImpl<ReceptionSettingMapper, ReceptionSetting> implements IReceptionSettingService {

    private final ReceptionSettingMapper receptionSettingMapper;
    private final DoctorMapper doctorMapper;

    @Autowired
    public ReceptionSettingServiceImpl(ReceptionSettingMapper receptionSettingMapper, DoctorMapper doctorMapper) {
        this.receptionSettingMapper = receptionSettingMapper;
        this.doctorMapper = doctorMapper;
    }

    @Override
    public void addReception(ReceptionVO receptionVO) {
        Doctor doctor = doctorMapper.selectById(receptionVO.getDoctorId());
        List<ReceptionSetting> receptionSettings = receptionSettingMapper.selectList(Wrappers.lambdaQuery(ReceptionSetting.class)
                .eq(ReceptionSetting::getDoctorId, receptionVO.getDoctorId()));
        List<Long> ids = receptionSettings.stream().map(e -> e.getId()).collect(Collectors.toList());
        if (!ids.isEmpty()) {
            receptionSettingMapper.deleteBatchIds(ids);
        }
        ReceptionSetting receptionSetting = new ReceptionSetting();
        if (Objects.nonNull(receptionVO.getReceptionCount0())) {
            receptionSetting.setReceptionBegin(Time.valueOf(LocalTime.of(9, 0)));
            receptionSetting.setReceptionEnd(Time.valueOf(LocalTime.of(11, 30)));
            receptionSetting.setReceptionCount(receptionVO.getReceptionCount0());
            receptionSetting.setDoctorId(receptionVO.getDoctorId());
            receptionSetting.setReceptionFee(doctor.getReceptionFee());
            receptionSettingMapper.insert(receptionSetting);
        }
        ReceptionSetting receptionSetting1 = new ReceptionSetting();
        if (Objects.nonNull(receptionVO.getReceptionCount1())) {
            receptionSetting1.setReceptionBegin(Time.valueOf(LocalTime.of(13, 30)));
            receptionSetting1.setReceptionEnd(Time.valueOf(LocalTime.of(17, 00)));
            receptionSetting1.setReceptionCount(receptionVO.getReceptionCount1());
            receptionSetting1.setDoctorId(receptionVO.getDoctorId());
            receptionSetting1.setReceptionFee(doctor.getReceptionFee());
            receptionSettingMapper.insert(receptionSetting1);
        }
    }
}
