package com.woniuxy.health.medical.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
//挂号统计页面的科室挂号收入
public class DepartmentIncomeVO {
    private String departmentName;
    private Long totalPrice;

}
