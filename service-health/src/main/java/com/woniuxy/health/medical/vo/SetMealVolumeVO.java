package com.woniuxy.health.medical.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SetMealVolumeVO {
    //我们这里返回的数据是塞饼图里的，饼图规定了关键字就是叫value和name,
    //所以我们这边接收数据也需要改一样的属性名才能把数据映射过去
    private String name;
    private Long value;//销售额

}
