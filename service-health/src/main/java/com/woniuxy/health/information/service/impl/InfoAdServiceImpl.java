package com.woniuxy.health.information.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.information.mapper.InfoAdMapper;
import com.woniuxy.health.information.vo.PageVo;
import com.woniuxy.health.model.information.Info;
import com.woniuxy.health.model.information.InfoAd;
import com.woniuxy.health.information.service.IInfoAdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Objects;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Service
public class InfoAdServiceImpl extends ServiceImpl<InfoAdMapper, InfoAd> implements IInfoAdService {

    private final InfoAdMapper infoAdMapper;

    @Autowired
    public InfoAdServiceImpl(InfoAdMapper infoAdMapper){
        this.infoAdMapper = infoAdMapper;
    }


    @Override
    public PageVo<InfoAd> getPage(Page<InfoAd> page, String adName) {
        Page<InfoAd> infoAdPage = infoAdMapper.selectPage(page, Wrappers.lambdaQuery(InfoAd.class)
                .likeRight(StringUtils.hasLength(adName), InfoAd::getAdName, adName));

        return new PageVo<>(infoAdPage.getTotal(),infoAdPage.getRecords());
    }

    @Override
    public void add(InfoAd infoAd) {
        infoAdMapper.insert(infoAd);

    }
}
