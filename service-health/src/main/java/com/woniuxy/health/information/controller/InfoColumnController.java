package com.woniuxy.health.information.controller;

import com.woniuxy.health.aspect.OperationLogAnnotation;
import com.woniuxy.health.information.service.IInfoColumnService;
import com.woniuxy.health.information.vo.InfoColumnVo;
import com.woniuxy.health.model.information.InfoColumn;
import com.woniuxy.utils.result.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@RestController
@RequestMapping("/info-column")
@CrossOrigin
public class InfoColumnController {

    private final IInfoColumnService infoColumnServiceImpl;

    @Autowired
    public InfoColumnController(IInfoColumnService infoColumnServiceImpl) {
        this.infoColumnServiceImpl = infoColumnServiceImpl;
    }

    @GetMapping("/findAll")
    public Result findAll() {
        List<InfoColumn> columns = infoColumnServiceImpl.list();

        return Result.ok(columns);
    }
    @PostMapping("/add")
    public Result add(@RequestBody InfoColumn infoColumn){
        infoColumnServiceImpl.add(infoColumn);

        return Result.ok();
    }

    @OperationLogAnnotation(module = "删除栏目", type = "删除栏目", description = "删除一条栏目")
    @DeleteMapping("/deleteById/{id}")
    public Result deleteById (@PathVariable("id") Long id){
        infoColumnServiceImpl.removeById(id);

        return Result.ok();
    }

    @OperationLogAnnotation(module = "编辑栏目", type = "编辑栏目", description = "编辑栏目")
    @PostMapping("/edit")
    public Result edit(@RequestBody InfoColumn infoColumn){

        infoColumnServiceImpl.updateById(infoColumn);

        return Result.ok();
    }
    @GetMapping("/findByInfoAd")
    public Result findByInfoAd(){
        List<InfoColumnVo> list = infoColumnServiceImpl.listInfoColumnName();

        return Result.ok(list);
    }

    @OperationLogAnnotation(module = "栏目修改上架下架状态", type = "修改状态", description = "修改一条栏目状态")
    @PostMapping("/updateState/{Id}/{State}")
    public Result updateState(@PathVariable("Id") Long id,@PathVariable("State") Byte state){

        infoColumnServiceImpl.updateByState(id,state);

        return Result.ok();
    }
    @OperationLogAnnotation(module = "批量删除栏目", type = "批量删除栏目", description = "批量删除栏目")
    @DeleteMapping("/deleteBatch/{ids}")
    public Result deleteBatch(@PathVariable List<Long> ids){

        infoColumnServiceImpl.removeBatchByIds(ids);

        return Result.ok();
    }

}
