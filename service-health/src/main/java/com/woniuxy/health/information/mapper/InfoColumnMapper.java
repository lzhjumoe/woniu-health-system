package com.woniuxy.health.information.mapper;
import org.apache.ibatis.annotations.Mapper;
import com.woniuxy.health.model.information.InfoColumn;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Mapper
public interface InfoColumnMapper extends BaseMapper<InfoColumn> {

    void updateByState(@Param("id") Long id,@Param("state") Byte state);
}
