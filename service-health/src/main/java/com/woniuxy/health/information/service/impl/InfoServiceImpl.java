package com.woniuxy.health.information.service.impl;

import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.information.mapper.InfoMapper;
import com.woniuxy.health.information.vo.AuditVO;
import com.woniuxy.health.information.vo.PageVo;
import com.woniuxy.health.model.information.Info;
import com.woniuxy.health.information.service.IInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Objects;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Service
public class InfoServiceImpl extends ServiceImpl<InfoMapper, Info> implements IInfoService {

    private final InfoMapper infoMapper;

    @Autowired
    public InfoServiceImpl(InfoMapper infoMapper) {
        this.infoMapper = infoMapper;
    }

    @Override
    public PageVo<Info> getPage(Page<Info> page, String authorName, Byte auditType, String infoName) {

        Page<Info> page1 = infoMapper.selectPage(page, Wrappers.lambdaQuery(Info.class)
                .eq(ObjectUtils.isNotNull(auditType), Info::getAuditType, auditType)
                .likeRight(StringUtils.hasLength(authorName), Info::getAuthorName, authorName)
                .likeRight(StringUtils.hasLength(infoName), Info::getInfoName, infoName)
                .orderByDesc(Info::getInfoTop));

        return new PageVo<>(page1.getTotal(), page1.getRecords());
    }

    @Override
    public void add(Info info) {
        infoMapper.insert(info);
    }

    @Override
    public void listTop(Long id) {
        byte top = 0;
        Info info1 = infoMapper.selectByTop();
        info1.setInfoTop(top);
        /*infoMapper.update(info1,null);*/
        infoMapper.updateById(info1);

        Info info = infoMapper.selectById(id);
        info.setInfoTop((byte) 1);
        infoMapper.updateById(info);


    }

    @Override
    public PageVo<AuditVO> getPage1(Page<AuditVO> page, String authorName, Byte auditType, String infoName) {
        Page<AuditVO> auditVOPageVo = infoMapper.selectPageAll(page, auditType, authorName, infoName);

        System.out.println("auditVOPageVo = " + auditVOPageVo);

        return new PageVo<>(auditVOPageVo.getTotal(), auditVOPageVo.getRecords());

    }

    @Override
    public Info findAll(Long id) {
        Info info = infoMapper.selectById(id);

        return info;
    }

    @Override
    public void pass(Info info) {
        Info info1 = infoMapper.selectById(info);
        info1.setState((byte) 1);
        info1.setAuditType((byte) 2);
        infoMapper.updateById(info1);
    }

    @Override
    public void overrule(Info info) {
        Info info1 = infoMapper.selectById(info);
        info1.setState((byte) 0);
        info1.setAuditType((byte) 2);
        infoMapper.updateById(info1);
    }
}
