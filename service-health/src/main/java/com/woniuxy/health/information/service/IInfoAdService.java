package com.woniuxy.health.information.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.woniuxy.health.information.vo.PageVo;
import com.woniuxy.health.model.information.InfoAd;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
public interface IInfoAdService extends IService<InfoAd> {

    PageVo<InfoAd> getPage(Page<InfoAd> page, String adName);

    void add(InfoAd infoAd);

}
