package com.woniuxy.health.information.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniuxy.health.aspect.OperationLogAnnotation;
import com.woniuxy.health.information.vo.PageVo;
import com.woniuxy.health.model.information.InfoAd;
import com.woniuxy.utils.result.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;
import com.woniuxy.health.information.service.IInfoAdService;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@RestController
@RequestMapping("/info-ad")
@CrossOrigin
public class InfoAdController {


    private final RedisTemplate redisTemplate;
    private final IInfoAdService infoAdServiceImpl;

    @Autowired
    public InfoAdController(RedisTemplate redisTemplate, IInfoAdService infoAdServiceImpl){
        this.redisTemplate = redisTemplate;
        this.infoAdServiceImpl = infoAdServiceImpl;
    }



    @GetMapping("/getPage")
    public Result getPage(Integer pageNum, Integer pageSize, String adName) {
        Page<InfoAd> page = Page.of(pageNum,pageSize);

        PageVo<InfoAd> pageVo = infoAdServiceImpl.getPage(page,adName);

        return Result.ok(pageVo);
    }

    @OperationLogAnnotation(module = "广告列表", type = "广告添加", description = "广告添加一条记录")
    @PostMapping("/add")
    public Result add(@RequestBody InfoAd infoAd){

        infoAdServiceImpl.add(infoAd);
        return Result.ok();
    }

    @OperationLogAnnotation(module = "广告列表", type = "广告删除", description = "删除一条广告")
    @DeleteMapping("/deleteById/{id}")
    public Result deleteById (@PathVariable("id") Long id){
        infoAdServiceImpl.removeById(id);

        return Result.ok();
    }

    @OperationLogAnnotation(module = "广告审核", type = "广告修改", description = "修改一条广告")
    @PostMapping("/edit")
    public Result edit(@RequestBody InfoAd infoAd){
        infoAdServiceImpl.updateById(infoAd);

        return Result.ok();
    }

    @OperationLogAnnotation(module = "广告审核", type = "广告批量删除", description = "批量删除广告")
    @PostMapping("/removeByIds")
    public Result removeByIds(@RequestBody List<Long> ids){
        infoAdServiceImpl.removeByIds(ids);

        return Result.ok();
    }

}
