package com.woniuxy.health.information.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.woniuxy.health.information.vo.AuditVO;
import com.woniuxy.health.information.vo.PageVo;
import com.woniuxy.health.model.information.Info;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
public interface IInfoService extends IService<Info> {

    PageVo<Info> getPage(Page<Info> page, String authorName, Byte auditType,String infoName);

    void add(Info info);

    void listTop(Long id);

    PageVo<AuditVO> getPage1(Page<AuditVO> page, String authorName, Byte auditType, String infoName);

    Info findAll(Long id);

    void pass(Info info);

    void overrule(Info info);

}
