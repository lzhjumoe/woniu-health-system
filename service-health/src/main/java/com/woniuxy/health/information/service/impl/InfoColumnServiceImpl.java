package com.woniuxy.health.information.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.information.mapper.InfoColumnMapper;
import com.woniuxy.health.information.vo.InfoColumnVo;
import com.woniuxy.health.model.information.InfoColumn;
import com.woniuxy.health.information.service.IInfoColumnService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Service
public class InfoColumnServiceImpl extends ServiceImpl<InfoColumnMapper, InfoColumn> implements IInfoColumnService {

    private final InfoColumnMapper infoColumnMapper;

    @Autowired
    public InfoColumnServiceImpl(InfoColumnMapper infoColumnMapper){
        this.infoColumnMapper = infoColumnMapper;
    }

    @Override
    public void add(InfoColumn infoColumn) {

        infoColumn.setState((byte) 0);
        infoColumnMapper.insert(infoColumn);
    }

    @Override
    public List<InfoColumnVo> listInfoColumnName() {
        List<InfoColumn> columns = infoColumnMapper.selectList(null);
        List<InfoColumnVo> collect = columns.stream()
                .map(c -> new InfoColumnVo(c.getId(), c.getColumnName()))
                .collect(Collectors.toList());

        return collect;
    }

    @Override
    public void updateByState(Long id, Byte state) {
        infoColumnMapper.updateByState(id,state);
    }
}
