package com.woniuxy.health.information.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniuxy.health.aspect.OperationLogAnnotation;
import com.woniuxy.health.information.service.IInfoService;
import com.woniuxy.health.information.vo.AuditVO;
import com.woniuxy.health.information.vo.PageVo;
import com.woniuxy.health.model.information.Info;
import com.woniuxy.utils.result.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/audit")
@CrossOrigin
public class AuditController {

    private final IInfoService infoServiceImpl;

    @Autowired
    public AuditController(IInfoService infoServiceImpl) {
        this.infoServiceImpl = infoServiceImpl;
    }


    @GetMapping("/findPageAll")
    public Result findPageAll(Integer pageNum, Integer pageSize, String infoName, String authorName, Byte auditType) {
        Page<AuditVO> page = Page.of(pageNum, pageSize);

        //List<AuditVO> page1 = infoServiceImpl.getPage1(page, authorName, auditType, infoName);
        PageVo page1 = infoServiceImpl.getPage1(page, authorName, auditType, infoName);

        return Result.ok(page1);
    }
    @GetMapping("/findAll/{id}")
    public Result findAll(@PathVariable("id") Long id) {
        Info info = infoServiceImpl.findAll(id);
        return Result.ok(info);
    }

    @OperationLogAnnotation(module = "广告审核", type = "广告审核通过", description = "广告审核通过一条")
    @PostMapping("/pass")
    public Result pass(@RequestBody Info info){

        infoServiceImpl.pass(info);

        return Result.ok();
    }
    @OperationLogAnnotation(module = "广告审核", type = "广告审核驳回", description = "广告审核驳回一条")
    @PostMapping("/overrule")
    public Result overrule(@RequestBody Info info){

        infoServiceImpl.overrule(info);

        return Result.ok();
    }

}
