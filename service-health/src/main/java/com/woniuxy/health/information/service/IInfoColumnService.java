package com.woniuxy.health.information.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.woniuxy.health.information.vo.InfoColumnVo;
import com.woniuxy.health.model.information.InfoColumn;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
public interface IInfoColumnService extends IService<InfoColumn> {

    void add(InfoColumn infoColumn);

    List<InfoColumnVo> listInfoColumnName();

    void updateByState(Long id, Byte state);
}
