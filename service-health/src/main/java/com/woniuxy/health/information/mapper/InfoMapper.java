package com.woniuxy.health.information.mapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniuxy.health.information.vo.AuditVO;
import com.woniuxy.health.information.vo.PageVo;
import org.apache.ibatis.annotations.Mapper;
import com.woniuxy.health.model.information.Info;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Mapper
public interface InfoMapper extends BaseMapper<Info> {

    Info selectByTop();

    Page<AuditVO> selectPageAll(@Param("page") Page<AuditVO> page ,
                                @Param("auditType") Byte auditType,
                                  @Param("authorName") String authorName,
                                  @Param("infoName") String infoName
                                );


}
