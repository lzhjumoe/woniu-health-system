package com.woniuxy.health.information.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniuxy.health.aspect.OperationLogAnnotation;
import com.woniuxy.health.information.vo.PageVo;
import com.woniuxy.health.model.information.Info;
import com.woniuxy.utils.result.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.woniuxy.health.information.service.IInfoService;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@RestController
@RequestMapping("/info")
@CrossOrigin
public class InfoController {

    private final IInfoService infoServiceImpl;

    @Autowired
    public InfoController(IInfoService infoServiceImpl){
        this.infoServiceImpl = infoServiceImpl;
    }

    @GetMapping("/getPage")
    public Result getPage(Integer pageNum, Integer pageSize, String authorName,Byte auditType,String infoName) {
        Page<Info> page = Page.of(pageNum,pageSize);
        page.setOptimizeCountSql(false);
        PageVo<Info> pageVo = infoServiceImpl.getPage(page,authorName,auditType,infoName);

        return Result.ok(pageVo);
    }

    @OperationLogAnnotation(module = "资讯列表", type = "添加资讯", description = "添加资讯")
    @PostMapping("/add")
    public Result add(@RequestBody Info info){
        System.out.println("打印info = " + info);
        info.setInfoColumnId(1L);
        infoServiceImpl.add(info);
        return Result.ok();
    }

    @OperationLogAnnotation(module = "资讯列表", type = "删除资讯", description = "删除一条资讯")
    @DeleteMapping("/deleteById/{id}")
    public Result deleteById (@PathVariable("id") Long id){
        infoServiceImpl.removeById(id);
        return Result.ok();
    }
    @OperationLogAnnotation(module = "资讯列表", type = "编辑资讯", description = "编辑一条资讯")
    @PostMapping("/edit")
    public Result edit(@RequestBody Info info){
        infoServiceImpl.updateById(info);

        return Result.ok();
    }

    @PostMapping("/listTop/{id}")
    public Result listTop(@PathVariable("id") Long id){
        infoServiceImpl.listTop(id);

        return Result.ok();
    }



}
