package com.woniuxy.health.information.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuditVO {

    private Long id;

    private String infoName;

    private String infoPhoto;

    private String infoDetail;

    private Byte state;

    private String authorName;

    private Byte auditType;

    private Timestamp createTime;

    private String columnName;
}
