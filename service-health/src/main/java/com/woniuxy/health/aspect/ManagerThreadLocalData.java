package com.woniuxy.health.aspect;

import java.util.HashMap;
import java.util.Map;

public class ManagerThreadLocalData {
    private static ThreadLocal<Map<String, Object>> threadLocal = new ThreadLocal<>();

    public static void set(String key, Object value) {
        Map<String, Object> map = threadLocal.get();
        if (map == null) {
            map = new HashMap<>();
            threadLocal.set(map);
        }
        map.put(key, value);
    }

    public static Object get(String key) {
        Map<String, Object> map = threadLocal.get();
        if (map != null) {
            return map.get(key);
        }
        return null;
    }

    public static void remove(String key) {
        Map<String, Object> map = threadLocal.get();
        if (map != null) {
            map.remove(key);
        }
    }

    public static void clear() {
        threadLocal.remove();
    }


}
