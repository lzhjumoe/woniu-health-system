package com.woniuxy.portal.manager.controller;
import com.woniuxy.portal.manager.service.IRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@RestController
@RequestMapping("/role")
public class RoleController {

    private final IRoleService roleServiceImpl;


    @Autowired
    public RoleController(IRoleService roleServiceImpl){
        this.roleServiceImpl = roleServiceImpl;

    }

}
