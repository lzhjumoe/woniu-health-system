package com.woniuxy.portal.manager.controller;


import com.woniuxy.portal.manager.service.IManagerRoleService;
import com.woniuxy.utils.result.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@RestController
@RequestMapping("/managerRole")
public class ManagerRoleController {

    private final IManagerRoleService managerRoleServiceImpl;

    @Autowired
    public ManagerRoleController(IManagerRoleService managerRoleServiceImpl){
        this.managerRoleServiceImpl = managerRoleServiceImpl;
    }

}
