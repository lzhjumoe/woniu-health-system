package com.woniuxy.portal.manager.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.woniuxy.health.model.manager.Role;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
public interface IRoleService extends IService<Role> {

}
