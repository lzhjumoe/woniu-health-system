package com.woniuxy.portal.manager.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.model.manager.Perssion;
import com.woniuxy.portal.manager.mapper.PerssionMapper;
import com.woniuxy.portal.manager.service.IPerssionService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Service
public class PerssionServiceImpl extends ServiceImpl<PerssionMapper, Perssion> implements IPerssionService {

    private final PerssionMapper perssionMapper;


    public PerssionServiceImpl(PerssionMapper perssionMapper) {
        this.perssionMapper = perssionMapper;
    }
}
