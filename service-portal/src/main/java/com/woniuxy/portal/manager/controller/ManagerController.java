package com.woniuxy.portal.manager.controller;


import com.woniuxy.portal.manager.service.IManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Objects;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@RestController
@RequestMapping("/manager")
@CrossOrigin
public class ManagerController {

    private final IManagerService managerServiceImpl;

    @Autowired
    public ManagerController(IManagerService managerServiceImpl){
        this.managerServiceImpl = managerServiceImpl;
    }

}
