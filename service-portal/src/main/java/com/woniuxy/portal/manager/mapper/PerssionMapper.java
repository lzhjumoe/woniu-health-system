package com.woniuxy.portal.manager.mapper;
import org.apache.ibatis.annotations.Mapper;
import com.woniuxy.health.model.manager.Perssion;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Mapper
public interface PerssionMapper extends BaseMapper<Perssion> {

}
