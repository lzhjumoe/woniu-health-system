package com.woniuxy.portal.manager.controller;


import com.woniuxy.portal.medical.service.IDepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@RestController
@RequestMapping("/department")
public class DepartmentController {

    private final IDepartmentService departmentServiceImpl;

    @Autowired
    public DepartmentController(IDepartmentService departmentServiceImpl){
        this.departmentServiceImpl = departmentServiceImpl;
    }

}
