package com.woniuxy.portal.manager.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.model.manager.ManagerRole;
import com.woniuxy.portal.manager.mapper.ManagerRoleMapper;
import com.woniuxy.portal.manager.service.IManagerRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Service
public class ManagerRoleServiceImpl extends ServiceImpl<ManagerRoleMapper, ManagerRole> implements IManagerRoleService {

    private final ManagerRoleMapper managerRoleMapper;

    @Autowired
    public ManagerRoleServiceImpl(ManagerRoleMapper managerRoleMapper) {
        this.managerRoleMapper = managerRoleMapper;
    }

}
