package com.woniuxy.portal.manager.controller;


import com.woniuxy.portal.manager.service.IRolePermissionService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@RestController
@RequestMapping("/rolePermission")
public class RolePermissionController {

    private final IRolePermissionService rolePermissionServiceImpl;


    public RolePermissionController(IRolePermissionService rolePermissionServiceImpl) {
        this.rolePermissionServiceImpl = rolePermissionServiceImpl;
    }
}
