package com.woniuxy.portal.manager.controller;

import com.woniuxy.portal.manager.service.IPerssionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@RestController
@CrossOrigin
@RequestMapping("/permission")
public class PerssionController {

    private final IPerssionService perssionServiceImpl;

    @Autowired
    public PerssionController(IPerssionService perssionServiceImpl){
        this.perssionServiceImpl = perssionServiceImpl;
    }

}
