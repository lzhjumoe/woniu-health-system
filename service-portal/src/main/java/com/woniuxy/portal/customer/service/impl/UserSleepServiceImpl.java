package com.woniuxy.portal.customer.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.model.customer.UserSleep;
import com.woniuxy.portal.customer.mapper.UserSleepMapper;
import com.woniuxy.portal.customer.service.IUserSleepService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 睡眠表 服务实现类
 * </p>
 *
 * @author godric
 * @since 2023-09-08
 */
@Service
public class UserSleepServiceImpl extends ServiceImpl<UserSleepMapper, UserSleep> implements IUserSleepService {

    private final UserSleepMapper userSleepMapper;
    public UserSleepServiceImpl(UserSleepMapper userSleepMapper){
        this.userSleepMapper = userSleepMapper;
    }

}
