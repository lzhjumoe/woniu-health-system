package com.woniuxy.portal.customer.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.model.customer.UserBloodPressure;
import com.woniuxy.portal.customer.mapper.UserBloodPressureMapper;
import com.woniuxy.portal.customer.service.IUserBloodPressureService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 血压表 服务实现类
 * </p>
 *
 * @author godric
 * @since 2023-09-08
 */
@Service
public class UserBloodPressureServiceImpl extends ServiceImpl<UserBloodPressureMapper, UserBloodPressure> implements IUserBloodPressureService {

    private final UserBloodPressureMapper userBloodPressureMapper;
    public UserBloodPressureServiceImpl(UserBloodPressureMapper userBloodPressureMapper){
        this.userBloodPressureMapper = userBloodPressureMapper;
    }

}
