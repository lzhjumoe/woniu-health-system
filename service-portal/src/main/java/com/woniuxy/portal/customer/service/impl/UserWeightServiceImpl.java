package com.woniuxy.portal.customer.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.model.customer.UserWeight;
import com.woniuxy.portal.customer.mapper.UserWeightMapper;
import com.woniuxy.portal.customer.service.IUserWeightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 体重表 服务实现类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Service
public class UserWeightServiceImpl extends ServiceImpl<UserWeightMapper, UserWeight> implements IUserWeightService {

    private final UserWeightMapper userWeightMapper;

    @Autowired
    public UserWeightServiceImpl(UserWeightMapper userWeightMapper) {
        this.userWeightMapper = userWeightMapper;
    }

}
