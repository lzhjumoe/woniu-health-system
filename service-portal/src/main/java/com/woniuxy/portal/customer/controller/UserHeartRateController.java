package com.woniuxy.portal.customer.controller;


import com.woniuxy.portal.customer.service.IUserHeartRateService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 心率表 前端控制器
 * </p>
 *
 * @author godric
 * @since 2023-09-08
 */
@RestController
@CrossOrigin
@RequestMapping("/userHeartRate")
public class UserHeartRateController {

    private final IUserHeartRateService userHeartRateServiceImpl;
    public UserHeartRateController(IUserHeartRateService userHeartRateServiceImpl){
        this.userHeartRateServiceImpl = userHeartRateServiceImpl;
    }

}
