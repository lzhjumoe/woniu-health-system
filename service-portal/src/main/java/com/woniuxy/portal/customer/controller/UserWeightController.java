package com.woniuxy.portal.customer.controller;


import com.woniuxy.portal.customer.service.IUserWeightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 体重表 前端控制器
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@RestController
@CrossOrigin
@RequestMapping("/userWeight")
public class UserWeightController {

    private final IUserWeightService userWeightServiceImpl;

    @Autowired
    public UserWeightController(IUserWeightService userWeightServiceImpl){
        this.userWeightServiceImpl = userWeightServiceImpl;
    }


}
