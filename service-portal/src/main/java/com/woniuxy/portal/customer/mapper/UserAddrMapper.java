package com.woniuxy.portal.customer.mapper;
import org.apache.ibatis.annotations.Mapper;
import com.woniuxy.health.model.customer.UserAddr;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Mapper
public interface UserAddrMapper extends BaseMapper<UserAddr> {

}
