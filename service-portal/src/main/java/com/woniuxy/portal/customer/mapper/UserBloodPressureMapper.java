package com.woniuxy.portal.customer.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.woniuxy.health.model.customer.UserBloodPressure;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 血压表 Mapper 接口
 * </p>
 *
 * @author godric
 * @since 2023-09-08
 */
@Mapper
public interface UserBloodPressureMapper extends BaseMapper<UserBloodPressure> {

}
