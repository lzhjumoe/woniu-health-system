package com.woniuxy.portal.customer.controller;


import com.woniuxy.portal.customer.service.IUserSleepService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 睡眠表 前端控制器
 * </p>
 *
 * @author godric
 * @since 2023-09-08
 */
@RestController
@CrossOrigin
@RequestMapping("/userSleep")
public class UserSleepController {

    private final IUserSleepService userSleepServiceImpl;
    public UserSleepController(IUserSleepService userSleepServiceImpl){
        this.userSleepServiceImpl = userSleepServiceImpl;
    }

}
