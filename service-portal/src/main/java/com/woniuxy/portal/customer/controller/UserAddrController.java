package com.woniuxy.portal.customer.controller;


import com.woniuxy.portal.customer.service.IUserAddrService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@RestController
@RequestMapping("/userAddr")
public class UserAddrController {

    private final IUserAddrService userAddrServiceImpl;

    @Autowired
    public UserAddrController(IUserAddrService userAddrServiceImpl){
        this.userAddrServiceImpl = userAddrServiceImpl;
    }

}
