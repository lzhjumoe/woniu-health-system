package com.woniuxy.portal.customer.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.woniuxy.health.model.customer.UserWalk;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 走步表 Mapper 接口
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Mapper
public interface UserWalkMapper extends BaseMapper<UserWalk> {

}
