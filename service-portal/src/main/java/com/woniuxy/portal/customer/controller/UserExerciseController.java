package com.woniuxy.portal.customer.controller;


import com.woniuxy.portal.customer.service.IUserExerciseService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 运动表 前端控制器
 * </p>
 *
 * @author godric
 * @since 2023-09-08
 */
@RestController
@CrossOrigin
@RequestMapping("/userExercise")
public class UserExerciseController {

    private final IUserExerciseService userExerciseServiceImpl;
    public UserExerciseController(IUserExerciseService userExerciseServiceImpl){
        this.userExerciseServiceImpl = userExerciseServiceImpl;
    }

}
