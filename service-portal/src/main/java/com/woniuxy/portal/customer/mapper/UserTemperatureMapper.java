package com.woniuxy.portal.customer.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.woniuxy.health.model.customer.UserTemperature;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 体温表 Mapper 接口
 * </p>
 *
 * @author godric
 * @since 2023-09-08
 */
@Mapper
public interface UserTemperatureMapper extends BaseMapper<UserTemperature> {

}
