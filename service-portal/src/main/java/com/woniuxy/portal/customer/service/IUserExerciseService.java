package com.woniuxy.portal.customer.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.woniuxy.health.model.customer.UserExercise;

/**
 * <p>
 * 运动表 服务类
 * </p>
 *
 * @author godric
 * @since 2023-09-08
 */
public interface IUserExerciseService extends IService<UserExercise> {

}
