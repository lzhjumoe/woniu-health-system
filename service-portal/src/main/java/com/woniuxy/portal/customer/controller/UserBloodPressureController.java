package com.woniuxy.portal.customer.controller;


import com.woniuxy.portal.customer.service.IUserBloodPressureService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 血压表 前端控制器
 * </p>
 *
 * @author godric
 * @since 2023-09-08
 */
@RestController
@CrossOrigin
@RequestMapping("/userBloodPressure")
public class UserBloodPressureController {

    private final IUserBloodPressureService userBloodPressureServiceImpl;
    public UserBloodPressureController(IUserBloodPressureService userBloodPressureServiceImpl){
        this.userBloodPressureServiceImpl = userBloodPressureServiceImpl;
    }

}
