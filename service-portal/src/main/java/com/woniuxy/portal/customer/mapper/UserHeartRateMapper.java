package com.woniuxy.portal.customer.mapper;


import org.apache.ibatis.annotations.Mapper;
import com.woniuxy.health.model.customer.UserHeartRate;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 心率表 Mapper 接口
 * </p>
 *
 * @author godric
 * @since 2023-09-08
 */
@Mapper
public interface UserHeartRateMapper extends BaseMapper<UserHeartRate> {

}
