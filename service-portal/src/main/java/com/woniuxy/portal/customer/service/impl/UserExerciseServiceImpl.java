package com.woniuxy.portal.customer.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.model.customer.UserExercise;
import com.woniuxy.portal.customer.mapper.UserExerciseMapper;
import com.woniuxy.portal.customer.service.IUserExerciseService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 运动表 服务实现类
 * </p>
 *
 * @author godric
 * @since 2023-09-08
 */
@Service
public class UserExerciseServiceImpl extends ServiceImpl<UserExerciseMapper, UserExercise> implements IUserExerciseService {

    private final UserExerciseMapper userExerciseMapper;
    public UserExerciseServiceImpl(UserExerciseMapper userExerciseMapper){
        this.userExerciseMapper = userExerciseMapper;
    }


}
