package com.woniuxy.portal.customer.controller;

import com.woniuxy.portal.customer.service.IUserTemperatureService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 体温表 前端控制器
 * </p>
 *
 * @author godric
 * @since 2023-09-08
 */
@RestController
@CrossOrigin
@RequestMapping("/userTemperature")
public class UserTemperatureController {

    private final IUserTemperatureService userTemperatureServiceImpl;
    public UserTemperatureController(IUserTemperatureService userTemperatureServiceImpl){
        this.userTemperatureServiceImpl = userTemperatureServiceImpl;
    }

}
