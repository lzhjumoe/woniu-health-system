package com.woniuxy.portal.customer.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.model.customer.UserTemperature;
import com.woniuxy.portal.customer.mapper.UserTemperatureMapper;
import com.woniuxy.portal.customer.service.IUserTemperatureService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 体温表 服务实现类
 * </p>
 *
 * @author godric
 * @since 2023-09-08
 */
@Service
public class UserTemperatureServiceImpl extends ServiceImpl<UserTemperatureMapper, UserTemperature> implements IUserTemperatureService {

    private final UserTemperatureMapper userTemperatureMapper;
    public UserTemperatureServiceImpl(UserTemperatureMapper userTemperatureMapper){
        this.userTemperatureMapper = userTemperatureMapper;
    }

}
