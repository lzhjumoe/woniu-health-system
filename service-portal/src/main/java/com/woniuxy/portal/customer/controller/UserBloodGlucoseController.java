package com.woniuxy.portal.customer.controller;

import com.woniuxy.portal.customer.service.IUserBloodGlucoseService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 血糖表 前端控制器
 * </p>
 *
 * @author godric
 * @since 2023-09-08
 */
@RestController
@CrossOrigin
@RequestMapping("/userBloodGlucose")
public class UserBloodGlucoseController {

    private final IUserBloodGlucoseService userBloodGlucoseServiceImpl;
    public UserBloodGlucoseController(IUserBloodGlucoseService userBloodGlucoseServiceImpl){
        this.userBloodGlucoseServiceImpl = userBloodGlucoseServiceImpl;
    }

}
