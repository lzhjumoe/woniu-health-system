package com.woniuxy.portal.customer.controller;

import com.woniuxy.portal.customer.service.IUserWalkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 走步表 前端控制器
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@RestController
@CrossOrigin
@RequestMapping("/userWalk")
public class UserWalkController {

    private final IUserWalkService userWalkServiceImpl;

    @Autowired
    public UserWalkController(IUserWalkService userWalkServiceImpl){
        this.userWalkServiceImpl = userWalkServiceImpl;

    }


}
