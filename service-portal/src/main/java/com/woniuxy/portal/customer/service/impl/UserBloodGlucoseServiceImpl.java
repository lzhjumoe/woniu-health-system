package com.woniuxy.portal.customer.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.model.customer.UserBloodGlucose;
import com.woniuxy.portal.customer.mapper.UserBloodGlucoseMapper;
import com.woniuxy.portal.customer.service.IUserBloodGlucoseService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 血糖表 服务实现类
 * </p>
 *
 * @author godric
 * @since 2023-09-08
 */
@Service
public class UserBloodGlucoseServiceImpl extends ServiceImpl<UserBloodGlucoseMapper, UserBloodGlucose> implements IUserBloodGlucoseService {

    private final UserBloodGlucoseMapper userBloodGlucoseMapper;
    public UserBloodGlucoseServiceImpl(UserBloodGlucoseMapper userBloodGlucoseMapper){
        this.userBloodGlucoseMapper = userBloodGlucoseMapper;
    }

}
