package com.woniuxy.portal.customer.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.woniuxy.health.model.customer.UserWeight;

/**
 * <p>
 * 体重表 服务类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
public interface IUserWeightService extends IService<UserWeight> {

}
