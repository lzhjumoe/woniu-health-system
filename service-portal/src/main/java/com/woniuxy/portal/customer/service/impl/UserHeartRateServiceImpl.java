package com.woniuxy.portal.customer.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.model.customer.UserHeartRate;
import com.woniuxy.portal.customer.mapper.UserHeartRateMapper;
import com.woniuxy.portal.customer.service.IUserHeartRateService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 心率表 服务实现类
 * </p>
 *
 * @author godric
 * @since 2023-09-08
 */
@Service
public class UserHeartRateServiceImpl extends ServiceImpl<UserHeartRateMapper, UserHeartRate> implements IUserHeartRateService {

    private final UserHeartRateMapper userHeartRateMapper;
    public UserHeartRateServiceImpl(UserHeartRateMapper userHeartRateMapper){
        this.userHeartRateMapper = userHeartRateMapper;
    }

}
