package com.woniuxy.portal.customer.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.model.customer.UserWalk;
import com.woniuxy.portal.customer.mapper.UserWalkMapper;
import com.woniuxy.portal.customer.service.IUserWalkService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 走步表 服务实现类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Service
public class UserWalkServiceImpl extends ServiceImpl<UserWalkMapper, UserWalk> implements IUserWalkService {

    private final UserWalkMapper userWalkMapper;


    public UserWalkServiceImpl(UserWalkMapper userWalkMapper) {
        this.userWalkMapper = userWalkMapper;
    }
}
