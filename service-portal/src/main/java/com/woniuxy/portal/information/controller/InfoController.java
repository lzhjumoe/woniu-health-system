package com.woniuxy.portal.information.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniuxy.health.model.information.Info;
import com.woniuxy.portal.information.service.IInfoService;
import com.woniuxy.portal.information.vo.PageVo;
import com.woniuxy.utils.result.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@RestController
@RequestMapping("/info")
@CrossOrigin
public class InfoController {

    private final IInfoService infoServiceImpl;

    @Autowired
    public InfoController(IInfoService infoServiceImpl){
        this.infoServiceImpl = infoServiceImpl;
    }

    @GetMapping("/showFindByInfoColumnId")
    public Result showFindByInfoColumnId(Integer pageNum, Integer pageSize, Long id) {
        //Page<Info>
        //infoCoLumnServiceImpl.showFindByInfoAd(id):
        Page<Info> page = Page.of(pageNum, pageSize);
        PageVo<Info> pageVo = infoServiceImpl.getByInfoColumnId(page, id);
        return Result.ok(pageVo);
    }

}
