package com.woniuxy.portal.information.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.model.information.InfoColumn;
import com.woniuxy.portal.information.mapper.InfoColumnMapper;
import com.woniuxy.portal.information.service.IInfoColumnService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Service
public class InfoColumnServiceImpl extends ServiceImpl<InfoColumnMapper, InfoColumn> implements IInfoColumnService {

    private final InfoColumnMapper infoColumnMapper;

    @Autowired
    public InfoColumnServiceImpl(InfoColumnMapper infoColumnMapper){
        this.infoColumnMapper = infoColumnMapper;
    }

}
