package com.woniuxy.portal.information.mapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import com.woniuxy.health.model.information.Info;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Mapper
public interface InfoMapper extends BaseMapper<Info> {

    Page<Info> selectPageByColumnId(Page<Info> page, Long id);
}
