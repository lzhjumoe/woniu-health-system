package com.woniuxy.portal.information.mapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import com.woniuxy.health.model.information.InfoAd;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Mapper
public interface InfoAdMapper extends BaseMapper<InfoAd> {

    Page<InfoAd> showFindAll(Page<InfoAd> page);
}
