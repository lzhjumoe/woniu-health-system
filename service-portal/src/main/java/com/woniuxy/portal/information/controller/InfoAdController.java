package com.woniuxy.portal.information.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.woniuxy.health.model.information.InfoAd;
import com.woniuxy.portal.information.service.IInfoAdService;
import com.woniuxy.utils.result.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@RestController
@RequestMapping("/info-ad")
public class InfoAdController {

    private final IInfoAdService infoAdServiceImpl;

    @Autowired
    public InfoAdController(IInfoAdService infoAdServiceImpl){
        this.infoAdServiceImpl = infoAdServiceImpl;
    }



    @GetMapping("/showFindAll")
    public Result showFindAll(Integer pageNum, Integer pageSize) {
        Page<InfoAd> page = Page.of(pageNum, pageSize);
        Page<InfoAd> adPage = infoAdServiceImpl.showFindAll(page);

        return Result.ok(adPage);
    }
}
