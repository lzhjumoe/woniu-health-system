package com.woniuxy.portal.information.service.impl;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.model.information.InfoAd;
import com.woniuxy.portal.information.mapper.InfoAdMapper;
import com.woniuxy.portal.information.service.IInfoAdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Service
public class InfoAdServiceImpl extends ServiceImpl<InfoAdMapper, InfoAd> implements IInfoAdService {

    private final InfoAdMapper infoAdMapper;

    @Autowired
    public InfoAdServiceImpl(InfoAdMapper infoAdMapper){
        this.infoAdMapper = infoAdMapper;
    }

    @Override
    public Page<InfoAd> showFindAll(Page<InfoAd> page) {
        Page<InfoAd> adPage = infoAdMapper.showFindAll(page);
        return adPage;
    }
}
