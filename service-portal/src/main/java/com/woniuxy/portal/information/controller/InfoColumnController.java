package com.woniuxy.portal.information.controller;


import com.woniuxy.portal.information.service.IInfoColumnService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@RestController
@RequestMapping("/info-column")
public class InfoColumnController {

    private final IInfoColumnService infoColumnServiceImpl;

    @Autowired
    public InfoColumnController(IInfoColumnService infoColumnServiceImpl) {
        this.infoColumnServiceImpl = infoColumnServiceImpl;
    }

}
