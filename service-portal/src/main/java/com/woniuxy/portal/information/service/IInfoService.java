package com.woniuxy.portal.information.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import com.woniuxy.health.model.information.Info;
import com.woniuxy.portal.information.vo.PageVo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
public interface IInfoService extends IService<Info> {


    PageVo<Info> getByInfoColumnId(Page<Info> page, Long id);
}
