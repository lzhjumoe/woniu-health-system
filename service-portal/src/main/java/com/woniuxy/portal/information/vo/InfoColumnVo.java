package com.woniuxy.portal.information.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InfoColumnVo {
    private Long id;
    private String columnName;
}
