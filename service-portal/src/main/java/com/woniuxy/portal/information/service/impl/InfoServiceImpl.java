package com.woniuxy.portal.information.service.impl;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.model.information.Info;
import com.woniuxy.portal.information.mapper.InfoMapper;
import com.woniuxy.portal.information.service.IInfoService;
import com.woniuxy.portal.information.vo.PageVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Service
public class InfoServiceImpl extends ServiceImpl<InfoMapper, Info> implements IInfoService {

    private final InfoMapper infoMapper;

    @Autowired
    public InfoServiceImpl(InfoMapper infoMapper) {
        this.infoMapper = infoMapper;
    }

    @Override
    public PageVo<Info> getByInfoColumnId(Page<Info> page, Long id) {
        Page<Info> infoVoPage = infoMapper.selectPageByColumnId(page,id);
        return new PageVo<>(infoVoPage.getTotal(),infoVoPage.getRecords());
    }
}
