package com.woniuxy.portal;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Date 2023/9/13 15:14
 * @Author LZH
 * Description:
 */
@SpringBootApplication
@MapperScan(basePackages = {"com.woniuxy.portal.commodity.mapper", "com.woniuxy.portal.customer.mapper",
        "com.woniuxy.portal.information.mapper", "com.woniuxy.portal.manager.mapper", "com.woniuxy.portal.medical.mapper"})
public class PortalApplication {

    public static void main(String[] args) {
        SpringApplication.run(PortalApplication.class, args);
    }
}
