package com.woniuxy.portal.medical.controller;

import com.woniuxy.portal.medical.service.IHospitalService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@RestController
@RequestMapping("/hospital")
public class HospitalController {

    private final IHospitalService hospitalServiceImpl;

    public HospitalController(IHospitalService hospitalServiceImpl) {
        this.hospitalServiceImpl = hospitalServiceImpl;
    }
}
