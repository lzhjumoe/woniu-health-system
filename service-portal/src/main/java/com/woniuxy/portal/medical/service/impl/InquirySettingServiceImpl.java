package com.woniuxy.portal.medical.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.model.medical.InquirySetting;
import com.woniuxy.portal.medical.mapper.InquirySettingMapper;
import com.woniuxy.portal.medical.service.IInquirySettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Service
public class InquirySettingServiceImpl extends ServiceImpl<InquirySettingMapper, InquirySetting> implements IInquirySettingService {

    private final InquirySettingMapper inquirySettingMapper;

    @Autowired
    public InquirySettingServiceImpl(InquirySettingMapper inquirySettingMapper) {
        this.inquirySettingMapper = inquirySettingMapper;
    }

}
