package com.woniuxy.portal.medical.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.woniuxy.health.model.medical.BodycheckInstitutions;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author usercheng
 * @since 2023-09-09
 */
public interface IBodycheckInstitutionsService extends IService<BodycheckInstitutions> {

}
