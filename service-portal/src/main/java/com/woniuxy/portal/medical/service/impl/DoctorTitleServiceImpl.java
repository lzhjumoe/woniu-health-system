package com.woniuxy.portal.medical.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.model.medical.DoctorTitle;
import com.woniuxy.portal.medical.mapper.DoctorTitleMapper;
import com.woniuxy.portal.medical.service.IDoctorTitleService;
import org.springframework.stereotype.Service;

/**
 * @Date 2023/9/9 12:18
 * @Author LZH
 * Description:
 */
@Service
public class DoctorTitleServiceImpl extends ServiceImpl<DoctorTitleMapper, DoctorTitle> implements IDoctorTitleService {
}
