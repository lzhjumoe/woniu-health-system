package com.woniuxy.portal.medical.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.woniuxy.health.model.medical.Doctor;
import com.woniuxy.health.model.medical.DoctorTitle;

/**
 * @Date 2023/9/9 12:18
 * @Author LZH
 * Description:
 */
public interface IDoctorTitleService extends IService<DoctorTitle> {
}
