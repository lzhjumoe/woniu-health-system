package com.woniuxy.portal.medical.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.model.medical.ReceptionSetting;
import com.woniuxy.portal.medical.mapper.ReceptionSettingMapper;
import com.woniuxy.portal.medical.service.IReceptionSettingService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Service
public class ReceptionSettingServiceImpl extends ServiceImpl<ReceptionSettingMapper, ReceptionSetting> implements IReceptionSettingService {

    private final ReceptionSettingMapper receptionSettingMapper;


    public ReceptionSettingServiceImpl(ReceptionSettingMapper receptionSettingMapper) {
        this.receptionSettingMapper = receptionSettingMapper;
    }
}
