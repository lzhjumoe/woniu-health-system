package com.woniuxy.portal.medical.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.model.medical.ConsultationRecord;
import com.woniuxy.portal.medical.mapper.ConsultationRecordMapper;
import com.woniuxy.portal.medical.service.IConsultationRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Service
public class ConsultationRecordServiceImpl extends ServiceImpl<ConsultationRecordMapper, ConsultationRecord> implements IConsultationRecordService {

    private final ConsultationRecordMapper consultationRecordMapper;

    @Autowired
    public ConsultationRecordServiceImpl(ConsultationRecordMapper consultationRecordMapper){
        this.consultationRecordMapper = consultationRecordMapper;
    }

}
