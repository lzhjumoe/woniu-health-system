package com.woniuxy.portal.medical.controller;


import com.woniuxy.portal.medical.service.IConsultationRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@RestController
@RequestMapping("/consultation-record")
public class ConsultationRecordController {

    private final IConsultationRecordService consultationRecordServiceImpl;

    @Autowired
    public ConsultationRecordController(IConsultationRecordService consultationRecordServiceImpl){
        this.consultationRecordServiceImpl = consultationRecordServiceImpl;
    }

}
