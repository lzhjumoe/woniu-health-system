package com.woniuxy.portal.medical.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.model.medical.BodycheckOrder;
import com.woniuxy.portal.medical.mapper.BodycheckOrderMapper;
import com.woniuxy.portal.medical.service.IBodycheckOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-11
 */
@Service
public class BodycheckOrderServiceImpl extends ServiceImpl<BodycheckOrderMapper, BodycheckOrder> implements IBodycheckOrderService {

    private final BodycheckOrderMapper bodycheckOrderMapper;

    @Autowired
    public BodycheckOrderServiceImpl(BodycheckOrderMapper bodycheckOrderMapper){
        this.bodycheckOrderMapper = bodycheckOrderMapper;
    }
}
