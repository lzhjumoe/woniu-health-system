package com.woniuxy.portal.medical.controller;


import com.woniuxy.portal.medical.service.IBodycheckOrderService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-11
 */
@Api("体检订单")
@RestController
@RequestMapping("/bodycheck-order")
public class BodycheckOrderController {

    private final IBodycheckOrderService bodycheckOrderServiceImpl;

    @Autowired
    public BodycheckOrderController(IBodycheckOrderService bodycheckOrderServiceImpl) {
        this.bodycheckOrderServiceImpl = bodycheckOrderServiceImpl;
    }


}
