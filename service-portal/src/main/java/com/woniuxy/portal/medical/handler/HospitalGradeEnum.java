package com.woniuxy.portal.medical.handler;

import lombok.Getter;

@Getter
public enum HospitalGradeEnum {

    Grade_A(1,"甲等"),
    Grade_B(2,"乙等");

    private Integer code;
    private String describe;
    private HospitalGradeEnum(Integer code, String describe) {
        this.code = code;
        this.describe = describe;
    }


}
