package com.woniuxy.portal.medical.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.model.medical.BodycheckMeal;
import com.woniuxy.portal.medical.mapper.BodycheckMealMapper;
import com.woniuxy.portal.medical.service.IBodycheckMealService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-11
 */
@Service
public class BodycheckMealServiceImpl extends ServiceImpl<BodycheckMealMapper, BodycheckMeal> implements IBodycheckMealService {

    private final BodycheckMealMapper bodycheckMealMapper;

    public BodycheckMealServiceImpl(BodycheckMealMapper bodycheckMealMapper) {
        this.bodycheckMealMapper = bodycheckMealMapper;
    }
}