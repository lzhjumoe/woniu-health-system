package com.woniuxy.portal.medical.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.model.medical.BodycheckInstitutions;
import com.woniuxy.portal.medical.mapper.BodycheckInstitutionsMapper;
import com.woniuxy.portal.medical.service.IBodycheckInstitutionsService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author usercheng
 * @since 2023-09-09
 */
@Service
public class BodycheckInstitutionsServiceImpl extends ServiceImpl<BodycheckInstitutionsMapper,
        BodycheckInstitutions> implements IBodycheckInstitutionsService {

    private final BodycheckInstitutionsMapper bodycheckInstitutionsMapper;

    public BodycheckInstitutionsServiceImpl(BodycheckInstitutionsMapper bodycheckInstitutionsMapper) {
        this.bodycheckInstitutionsMapper = bodycheckInstitutionsMapper;
    }
}
