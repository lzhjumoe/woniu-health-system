package com.woniuxy.portal.medical.controller;

import com.woniuxy.portal.medical.service.IInquirySettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@RestController
@RequestMapping("/inquiry-setting")
public class InquirySettingController {

    private final IInquirySettingService inquirySettingServiceImpl;

    @Autowired
    public InquirySettingController(IInquirySettingService inquirySettingServiceImpl){
        this.inquirySettingServiceImpl = inquirySettingServiceImpl;
    }
}
