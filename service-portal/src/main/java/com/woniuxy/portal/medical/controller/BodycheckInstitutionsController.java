package com.woniuxy.portal.medical.controller;


import com.woniuxy.portal.medical.service.IBodycheckInstitutionsService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@RestController
@RequestMapping("/bodycheck-institutions")
public class BodycheckInstitutionsController {

    private final IBodycheckInstitutionsService bodycheckInstitutionsServiceImpl;


    public BodycheckInstitutionsController(IBodycheckInstitutionsService bodycheckInstitutionsServiceImpl) {
        this.bodycheckInstitutionsServiceImpl = bodycheckInstitutionsServiceImpl;
    }
}
