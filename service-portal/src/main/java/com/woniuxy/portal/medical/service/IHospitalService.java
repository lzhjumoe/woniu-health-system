package com.woniuxy.portal.medical.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.woniuxy.health.model.medical.Hospital;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
public interface IHospitalService extends IService<Hospital> {

}
