package com.woniuxy.portal.medical.controller;

import com.woniuxy.portal.medical.service.IInquiryAppointmentService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author usercheng
 * @since 2023-09-07
 */
@RestController
@RequestMapping("/inquiry-appointment")
public class InquiryAppointmentController {

    private final IInquiryAppointmentService inquiryAppointmentServiceImpl;
    public InquiryAppointmentController(IInquiryAppointmentService inquiryAppointmentServiceImpl){
        this.inquiryAppointmentServiceImpl = inquiryAppointmentServiceImpl;
    }

}
