package com.woniuxy.portal.medical.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.model.medical.China;
import com.woniuxy.portal.medical.mapper.ChinaMapper;
import com.woniuxy.portal.medical.service.IChinaService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author usercheng
 * @since 2023-09-09
 */
@Service
public class ChinaServiceImpl extends ServiceImpl<ChinaMapper, China> implements IChinaService {

    private final ChinaMapper chinaMapper;
    public ChinaServiceImpl(ChinaMapper chinaMapper){
        this.chinaMapper = chinaMapper;
    }

}
