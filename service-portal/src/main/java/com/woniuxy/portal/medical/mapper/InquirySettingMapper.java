package com.woniuxy.portal.medical.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.woniuxy.health.model.medical.InquirySetting;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Mapper
public interface InquirySettingMapper extends BaseMapper<InquirySetting> {

}
