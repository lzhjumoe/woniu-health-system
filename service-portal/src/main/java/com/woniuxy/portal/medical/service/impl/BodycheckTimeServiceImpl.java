package com.woniuxy.portal.medical.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.model.medical.BodycheckTime;
import com.woniuxy.portal.medical.mapper.BodycheckTimeMapper;
import com.woniuxy.portal.medical.service.IBodycheckTimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-11
 */
@Service
public class BodycheckTimeServiceImpl extends ServiceImpl<BodycheckTimeMapper, BodycheckTime> implements IBodycheckTimeService {

    private final BodycheckTimeMapper bodycheckTimeMapper;

    @Autowired
    public BodycheckTimeServiceImpl(BodycheckTimeMapper bodycheckTimeMapper){
        this.bodycheckTimeMapper = bodycheckTimeMapper;
    }

}
