package com.woniuxy.portal.medical.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.woniuxy.health.model.medical.BodycheckTime;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-11
 */
public interface IBodycheckTimeService extends IService<BodycheckTime> {

}
