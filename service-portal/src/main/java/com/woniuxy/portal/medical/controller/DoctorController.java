package com.woniuxy.portal.medical.controller;


import com.woniuxy.portal.medical.service.*;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@RestController
@RequestMapping("/doctor")
@Api(tags = "医生管理")
public class DoctorController {

    private final IDoctorService doctorServiceImpl;

    public DoctorController(IDoctorService doctorServiceImpl) {
        this.doctorServiceImpl = doctorServiceImpl;
    }
}
