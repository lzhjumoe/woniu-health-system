package com.woniuxy.portal.medical.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.woniuxy.health.model.medical.BodycheckOrder;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-11
 */
@Mapper
public interface BodycheckOrderMapper extends BaseMapper<BodycheckOrder> {

}
