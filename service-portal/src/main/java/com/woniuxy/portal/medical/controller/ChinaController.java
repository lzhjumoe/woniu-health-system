package com.woniuxy.portal.medical.controller;


import com.woniuxy.health.model.medical.China;
import com.woniuxy.portal.medical.service.IChinaService;
import com.woniuxy.utils.result.Result;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author usercheng
 * @since 2023-09-09
 */
@RestController
@RequestMapping("/china")
public class ChinaController {

    private final IChinaService chinaServiceImpl;
    public ChinaController(IChinaService chinaServiceImpl){
        this.chinaServiceImpl = chinaServiceImpl;
    }

}
