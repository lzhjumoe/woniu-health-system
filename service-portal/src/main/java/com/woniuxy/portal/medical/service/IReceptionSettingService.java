package com.woniuxy.portal.medical.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.woniuxy.health.model.medical.ReceptionSetting;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
public interface IReceptionSettingService extends IService<ReceptionSetting> {

}
