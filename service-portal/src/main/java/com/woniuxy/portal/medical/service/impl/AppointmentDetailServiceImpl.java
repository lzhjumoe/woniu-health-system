package com.woniuxy.portal.medical.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.model.medical.AppointmentDetail;
import com.woniuxy.portal.medical.mapper.AppointmentDetailMapper;
import com.woniuxy.portal.medical.service.IAppointmentDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Service
public class AppointmentDetailServiceImpl extends ServiceImpl<AppointmentDetailMapper, AppointmentDetail> implements IAppointmentDetailService {

    private final AppointmentDetailMapper appointmentDetailMapper;

    @Autowired
    public AppointmentDetailServiceImpl(AppointmentDetailMapper appointmentDetailMapper){
        this.appointmentDetailMapper = appointmentDetailMapper;
    }

}
