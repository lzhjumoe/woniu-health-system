package com.woniuxy.portal.medical.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.model.medical.InquiryAppointment;
import com.woniuxy.portal.medical.mapper.InquiryAppointmentMapper;
import com.woniuxy.portal.medical.service.IInquiryAppointmentService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author usercheng
 * @since 2023-09-07
 */
@Service
public class InquiryAppointmentServiceImpl extends ServiceImpl<InquiryAppointmentMapper, InquiryAppointment> implements IInquiryAppointmentService {

    private final InquiryAppointmentMapper inquiryAppointmentMapper;
    public InquiryAppointmentServiceImpl(InquiryAppointmentMapper inquiryAppointmentMapper){
        this.inquiryAppointmentMapper = inquiryAppointmentMapper;
    }

}
