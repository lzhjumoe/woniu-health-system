package com.woniuxy.portal.medical.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.model.medical.HospitalDepartments;
import com.woniuxy.portal.medical.mapper.HospitalDepartmentsMapper;
import com.woniuxy.portal.medical.service.IHospitalDepartmentsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Service
public class HospitalDepartmentsServiceImpl extends ServiceImpl<HospitalDepartmentsMapper,
        HospitalDepartments> implements IHospitalDepartmentsService {

    private final HospitalDepartmentsMapper hospitalDepartmentsMapper;

    @Autowired
    public HospitalDepartmentsServiceImpl(HospitalDepartmentsMapper hospitalDepartmentsMapper){
        this.hospitalDepartmentsMapper = hospitalDepartmentsMapper;
    }

}
