package com.woniuxy.portal.medical.controller;


import com.woniuxy.portal.medical.service.IDoctorTitleService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Date 2023/9/9 12:17
 * @Author LZH
 * Description:
 */
@RestController
@RequestMapping("/doctorTitle")
@Api(tags = "医生职称管理")
public class DoctorTitleController {

    private final IDoctorTitleService doctorTitleServiceImpl;

    public DoctorTitleController(IDoctorTitleService doctorTitleServiceImpl) {
        this.doctorTitleServiceImpl = doctorTitleServiceImpl;
    }

}
