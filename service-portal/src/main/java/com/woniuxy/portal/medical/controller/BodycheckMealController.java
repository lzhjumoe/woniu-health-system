package com.woniuxy.portal.medical.controller;


import com.woniuxy.portal.medical.service.IBodycheckMealService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-11
 */
@RestController
@RequestMapping("/bodycheck-meal")
@Api(tags = "体检套餐管理")
public class BodycheckMealController {

    private final IBodycheckMealService bodycheckMealServiceImpl;


    public BodycheckMealController(IBodycheckMealService bodycheckMealServiceImpl) {
        this.bodycheckMealServiceImpl = bodycheckMealServiceImpl;
    }
}
