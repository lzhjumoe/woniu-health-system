package com.woniuxy.portal.medical.handler;

import lombok.Getter;

@Getter
public enum HospitalTypeEnum {

    TYPE_A(1,"公立医院"),
    TYPE_B(2,"私立医院");

    private Integer code;
    private String describe;
    private HospitalTypeEnum(Integer code, String describe) {
        this.code = code;
        this.describe = describe;
    }


}
