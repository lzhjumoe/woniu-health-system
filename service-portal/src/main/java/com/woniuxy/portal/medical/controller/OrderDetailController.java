package com.woniuxy.portal.medical.controller;


import com.woniuxy.portal.medical.service.IOrderDetailService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@RestController
@RequestMapping("/order-detail")
public class OrderDetailController {

    private final IOrderDetailService orderDetailServiceImpl;

    public OrderDetailController(IOrderDetailService orderDetailServiceImpl) {
        this.orderDetailServiceImpl = orderDetailServiceImpl;
    }
}
