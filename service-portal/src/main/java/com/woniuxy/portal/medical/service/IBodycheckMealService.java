package com.woniuxy.portal.medical.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.woniuxy.health.model.medical.BodycheckMeal;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-11
 */
public interface IBodycheckMealService extends IService<BodycheckMeal> {

}
