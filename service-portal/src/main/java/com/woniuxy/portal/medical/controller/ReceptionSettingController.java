package com.woniuxy.portal.medical.controller;


import com.woniuxy.portal.medical.service.IReceptionSettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@RestController
@RequestMapping("/reception-setting")
public class ReceptionSettingController {

    private final IReceptionSettingService receptionSettingServiceImpl;


    @Autowired
    public ReceptionSettingController(IReceptionSettingService receptionSettingServiceImpl) {
        this.receptionSettingServiceImpl = receptionSettingServiceImpl;
    }

}
