package com.woniuxy.portal.medical.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.woniuxy.health.model.medical.Doctor;
import com.woniuxy.health.model.medical.DoctorTitle;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Date 2023/9/9 12:19
 * @Author LZH
 * Description:
 */
@Mapper
public interface DoctorTitleMapper extends BaseMapper<DoctorTitle> {
}
