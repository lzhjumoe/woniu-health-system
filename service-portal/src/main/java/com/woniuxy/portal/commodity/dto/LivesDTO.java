package com.woniuxy.portal.commodity.dto;

import lombok.Data;

/**
 * @author huang
 * @date 2023/9/14 15:06
 */
@Data
public class LivesDTO {

    private String province;
    private String city;
    private String adcode;
    private String weather;
    private String temperature;
    private String winddirection;
    private String windpower;
    private String humidity;
    private String reporttime;
    private String temperatureFloat;
    private String humidityFloat;
}
