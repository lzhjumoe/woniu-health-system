package com.woniuxy.portal.commodity.controller;

import com.woniuxy.health.model.commodity.GoodsLogistics;
import com.woniuxy.portal.commodity.service.IGoodsLogisticsService;
import com.woniuxy.utils.result.Result;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@RestController
@RequestMapping("/goodsLogistics")
public class GoodsLogisticsController {

    private final IGoodsLogisticsService goodsLogisticsServiceImpl;

    public GoodsLogisticsController(IGoodsLogisticsService goodsLogisticsServiceImpl){
        this.goodsLogisticsServiceImpl = goodsLogisticsServiceImpl;
    }

}
