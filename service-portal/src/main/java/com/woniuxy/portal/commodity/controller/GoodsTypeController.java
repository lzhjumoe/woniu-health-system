package com.woniuxy.portal.commodity.controller;

import com.woniuxy.health.model.commodity.GoodsType;
import com.woniuxy.portal.commodity.service.IGoodsTypeService;
import com.woniuxy.utils.result.Result;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@RestController
@RequestMapping("/goodsType")
public class GoodsTypeController {

    private final IGoodsTypeService goodsTypeServiceImpl;

    public GoodsTypeController(IGoodsTypeService goodsTypeServiceImpl) {
        this.goodsTypeServiceImpl = goodsTypeServiceImpl;
    }

    @GetMapping("/getAllLevelOne")
    public Result getAllLevelOne(){
        List<GoodsType> goodsTypes = goodsTypeServiceImpl.getLevelOne();
        return Result.ok(goodsTypes);
    }
}
