package com.woniuxy.portal.commodity.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.woniuxy.health.model.commodity.GoodsOrder;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Mapper
public interface GoodsOrderMapper extends BaseMapper<GoodsOrder> {

}
