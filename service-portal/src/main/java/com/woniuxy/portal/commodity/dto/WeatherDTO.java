package com.woniuxy.portal.commodity.dto;

import lombok.Data;

import java.util.List;

/**
 * @author huang
 * @date 2023/9/14 15:09
 */
@Data
public class WeatherDTO {

    private String status;
    private String count;
    private String info;
    private String infocode;
    private List<LivesDTO> lives;
}
