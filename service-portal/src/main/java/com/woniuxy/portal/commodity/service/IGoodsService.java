package com.woniuxy.portal.commodity.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import com.woniuxy.health.model.commodity.Goods;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
public interface IGoodsService extends IService<Goods> {

    List<Goods> getByWeather(String weather);
}
