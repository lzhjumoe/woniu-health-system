package com.woniuxy.portal.commodity.service.impl;


import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.model.commodity.Goods;
import com.woniuxy.portal.commodity.mapper.GoodsMapper;
import com.woniuxy.portal.commodity.service.IGoodsService;
import org.apache.poi.ss.formula.functions.Na;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Service
public class GoodsServiceImpl extends ServiceImpl<GoodsMapper, Goods> implements IGoodsService {

    private final GoodsMapper goodsMapper;
    private final ElasticsearchRestTemplate es;

    public GoodsServiceImpl(GoodsMapper goodsMapper, ElasticsearchRestTemplate es) {
        this.goodsMapper = goodsMapper;
        this.es = es;
    }

    @Override
    public List<Goods> getByWeather(String weather) {
//
//        NativeSearchQueryBuilder builder
//                = new NativeSearchQueryBuilder();
//
//            builder.withQuery(QueryBuilders.multiMatchQuery(weather,
//                    "goodsName", "goodsHead",
//                    "goodsSubHead", "goodsDetail", "goodsTypeName"));
//
//        SearchHits<Goods> hits = es.search(builder.build(), Goods.class);
//        List<Goods> collect
//                = hits.stream().map(e -> e.getContent()).collect(Collectors.toList());

        List<Goods> goods =
                goodsMapper.selectList(Wrappers
                        .lambdaQuery(Goods.class)
                        .likeRight(Goods::getGoodsName, weather));
        return goods;
    }
}
