package com.woniuxy.portal.commodity.controller;

import com.woniuxy.portal.commodity.service.IGoodsOrderService;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@RestController
@RequestMapping("/goodsOrder")
public class GoodsOrderController {

    private final IGoodsOrderService goodsOrderServiceImpl;

    public GoodsOrderController(IGoodsOrderService goodsOrderServiceImpl) {
        this.goodsOrderServiceImpl = goodsOrderServiceImpl;
    }
}
