package com.woniuxy.portal.commodity.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.woniuxy.health.model.commodity.GoodsLogistics;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
public interface IGoodsLogisticsService extends IService<GoodsLogistics> {

}
