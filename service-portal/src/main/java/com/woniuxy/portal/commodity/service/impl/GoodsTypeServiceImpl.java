package com.woniuxy.portal.commodity.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.model.commodity.GoodsType;
import com.woniuxy.portal.commodity.mapper.GoodsTypeMapper;
import com.woniuxy.portal.commodity.service.IGoodsTypeService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Service
public class GoodsTypeServiceImpl extends ServiceImpl<GoodsTypeMapper, GoodsType> implements IGoodsTypeService {

    private final GoodsTypeMapper goodsTypeMapper;

    public GoodsTypeServiceImpl(GoodsTypeMapper goodsTypeMapper) {
        this.goodsTypeMapper = goodsTypeMapper;
    }

    @Override
    public List<GoodsType> getLevelOne() {
        List<GoodsType> goodsTypes =
                goodsTypeMapper.selectList(Wrappers
                        .lambdaQuery(GoodsType.class)
                        .eq(GoodsType::getParentId, 0));

        return goodsTypes;
    }
}
