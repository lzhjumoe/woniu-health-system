package com.woniuxy.portal.commodity.dto;

import lombok.Data;

/**
 * @author huang
 * @date 2023/9/14 14:58
 */
@Data
public class IpAddrDTO {

    private String status;
    private String info;
    private String infocode;
    private String province;
    private String city;
    private String adcode;
    private String rectangle;
}
