package com.woniuxy.portal.commodity.service;



import com.baomidou.mybatisplus.extension.service.IService;
import com.woniuxy.health.model.commodity.GoodsType;

import java.util.HashMap;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
public interface IGoodsTypeService extends IService<GoodsType> {

    List<GoodsType> getLevelOne();
}
