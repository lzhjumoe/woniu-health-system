package com.woniuxy.portal.commodity.service.impl;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniuxy.health.model.commodity.GoodsOrder;
import com.woniuxy.portal.commodity.mapper.GoodsOrderMapper;
import com.woniuxy.portal.commodity.service.IGoodsOrderService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author woniuxy
 * @since 2023-09-04
 */
@Service
public class GoodsOrderServiceImpl extends ServiceImpl<GoodsOrderMapper, GoodsOrder> implements IGoodsOrderService {

    private final GoodsOrderMapper goodsOrderMapper;


    public GoodsOrderServiceImpl(GoodsOrderMapper goodsOrderMapper) {
        this.goodsOrderMapper = goodsOrderMapper;
    }
}
