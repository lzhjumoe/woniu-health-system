package com.woniuxy.utils.handle;


import com.woniuxy.utils.result.Result;
import com.woniuxy.utils.result.handle.WoniuHealthException;
import org.springframework.core.annotation.Order;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @Date 2023/8/7 19:04
 * @Author LZH
 * Description: 全局异常处理器
 */

//可以认为该类，就是异常切面
//@RestControllerAdvice 监听所有的Controller，看是否抛出异常，一旦抛出异常，就被通知到切面上
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(WoniuHealthException.class)
    @Order
    @ResponseBody
    public Result error(WoniuHealthException e) {
        e.printStackTrace();
        return Result.fail().code(e.getCode()).message(e.getMessage());
    }

}
