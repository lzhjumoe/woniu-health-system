package com.woniuxy.utils.config.code;



import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.config.rules.DateType;

import java.util.Collections;

/**
 * @Date 2023/8/5 10:00
 * @Author LZH
 * Description:
 */
public class CodeGenerator {

    public static void main(String[] args) {
        //修改url username password
        FastAutoGenerator.create("jdbc:mysql://localhost:3306/woniubook", "root", "root")
                .globalConfig(builder -> {
                    builder.author("woniuxy") // 设置作者
                            .enableSwagger() // 开启 swagger 模式
                            .fileOverride() // 覆盖已生成文件
                            .dateType(DateType.ONLY_DATE).disableOpenDir().outputDir(
                                    "D:\\snailCode\\code\\java"); // 指定输出目录
                }).packageConfig(builder -> {
                    builder.parent("com.woniuxy.operator") // 设置父包名
                            .moduleName(null) // 设置父包模块名
                            .pathInfo(Collections.singletonMap(
                                    OutputFile.xml, "D:\\snailCode\\code\\resources\\mapper")); // 设置mapperXml生成路径
                }).strategyConfig(builder -> {
                    builder.entityBuilder().enableLombok();
                    builder.controllerBuilder().enableHyphenStyle().enableRestStyle();
                    builder.addInclude("t_book", "t_booktype",
                                    "t_manager", "t_manager_role", "t_role",
                                    "t_role_url_permission", "t_url_permission", "t_user") // 设置需要生成的表名
                            .addTablePrefix("t_"); // 设置过滤表前缀
                })
                .execute();
    }

}
