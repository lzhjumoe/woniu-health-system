package com.woniuxy.utils.result.handle;


import com.woniuxy.utils.result.ResultCodeEnum;
import lombok.Data;

/**
 * @Date 2023/8/7 19:13
 * @Author LZH
 * Description:
 */
@Data
public class WoniuHealthException extends RuntimeException{


    private Integer code;//状态码

    private String message;//描述信息

    /**
     * 通过状态码和错误消息创建异常对象
     *
     * @param code
     * @param message
     */
    public WoniuHealthException(Integer code, String message) {
        super(message);
        this.code = code;
        this.message = message;
    }

    /**
     * 接收枚举类型对象
     *
     * @param resultCodeEnum
     */
    public WoniuHealthException(ResultCodeEnum resultCodeEnum) {
        super(resultCodeEnum.getMessage());
        this.code = resultCodeEnum.getCode();
        this.message = resultCodeEnum.getMessage();
    }


    @Override
    public String toString() {
        return "WoniuBookException{" +
                "code=" + code +
                ", message='" + message + '\'' +
                '}';
    }
}
