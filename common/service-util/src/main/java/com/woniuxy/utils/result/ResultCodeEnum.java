package com.woniuxy.utils.result;

import lombok.Getter;

/**
 * @Date 2023/7/13 11:49
 * @Author LZH
 * Description:
 */
@Getter
public enum ResultCodeEnum {

    SUCCESS(200, "成功"),
    FAIL(201, "失败"),
    SERVICE_ERROR(2012, "服务异常"),
    DATA_ERROR(204, "数据异常"),
    LOGIN_MOBLE_ERROR(401, "登陆失败"),
    LOGIN_AUTH(208, "未登陆"),
    PERMISSION(209, "没有权限"),
    SYSTEM_BUSY(1000, "系统繁忙，请稍后重试！"),
    OPTIMISTIC_LOCK_ERROR(1001, "网络波动异常，请稍后重试！"),
    VALICODE_CODE_ISNULL(2001, "验证码不能为空"),
    VALICODE_ERROR(2002, "验证码输入错误，请重新输入"),
    VALICODE_CODE_ISLOSE(2003, "验证码已失效，请重新获取验证码"),
    ACCOUNT_NOT_EXIST(2010, "用户名错误，请重新输入"),
    ACCOUNT_ALREADY_EXIST(2011, "用户名已存在"),
    ACCOUNT_PWD_ERROR(2012, "账号或密码错误"),
    PWD_ERROR(2013, "密码错误，请重试"),
    ACCOUNT_DISABLED(2015, "账号被禁用"),
    EMAIL_ALREADY_EXIST(2014, "邮箱已存在"),
    INVENTORY_NOT_ENOUGH(3000, "商品已售罄"),
    HTTP_ALREADY_HANDLE(4000,"请求已处理，请勿重复提交"),
    TYPE_ALREADY_EXISTS(3001, "该品类已存在，请勿重复添加"),
    NO_DATA(3002, "未提交必填数据！"),
    ACCOUNT_PASSWORD_ERROR(  4000,"用户密码不正确" );


    private Integer code;
    private String message;

    private ResultCodeEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
