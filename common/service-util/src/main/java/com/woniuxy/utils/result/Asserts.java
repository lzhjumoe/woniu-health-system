package com.woniuxy.utils.result;


import com.woniuxy.utils.result.handle.WoniuHealthException;

/**
 * @Date 2023/8/15 18:18
 * @Author LZH
 * Description:
 */
public class Asserts {

    /**
     * 抛出异常
     * @param condition
     * @param resultCodeEnum
     */
    public static void fail(boolean condition, ResultCodeEnum resultCodeEnum) {
        if (condition == true) {
            throw new WoniuHealthException(resultCodeEnum);
        }
    }

}
