import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.config.rules.DateType;

import java.util.Collections;

/**
 * @Date 2023/8/5 10:00
 * @Author LZH
 * Description:
 */
public class CodeGenerator {

    public static void main(String[] args) {
        //修改url username password
        FastAutoGenerator.create("jdbc:mysql://192.168.200.130:3306/woniuxy_health_sys", "root", "root")
                .globalConfig(builder -> {
                    builder.author("woniuxy") // 设置作者
                            .enableSwagger() // 开启 swagger 模式
                            .fileOverride() // 覆盖已生成文件
                            .dateType(DateType.ONLY_DATE).disableOpenDir().outputDir(
                                    "D:\\snailCode\\code\\java"); // 指定输出目录
                }).packageConfig(builder -> {
                    builder.parent("com.woniuxy.health") // 设置父包名
                            .moduleName(null) // 设置父包模块名
                            .pathInfo(Collections.singletonMap(
                                    OutputFile.xml, "D:\\snailCode\\code\\resources\\mapper")); // 设置mapperXml生成路径
                }).strategyConfig(builder -> {
                    builder.entityBuilder().enableLombok();
                    builder.controllerBuilder().enableHyphenStyle().enableRestStyle();
//                    builder.addInclude("sys_department", "sys_manager",
//                                    "sys_manager_role", "sys_operation_log", "sys_perssion",
//                                    "sys_role", "sys_role_permission", "t_appointment_detail",
//                                    "t_bodycheck_institutions","t_consultation_record","t_doctor",
//                                    "t_goods","t_goods_logistics","t_goods_order","t_goods_type","t_hospital",
//                                    "t_hospital_departments", "t_info","t_info_ad","t_info_column",
//                                    "t_inquiry_setting","t_order_detail","t_reception_setting","t_set_meal","t_set_meal_setting",
//                                    "t_timetable","t_user","t_user_addr","t_user_walk","t_user_weight") // 设置需要生成的表名
                    builder.addInclude("t_bodycheck_meal", "t_bodycheck_order", "t_bodycheck_time")
                            .addTablePrefix("t_"); // 设置过滤表前缀
                })
                .execute();
    }

}
